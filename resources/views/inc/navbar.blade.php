<!-- ========== HEADER ========== -->
<header id="header" class="u-header u-header--abs-top-xl u-header--white-nav-links-xl u-header--bg-transparent-xl u-header--show-hide-xl" data-header-fix-moment="500" data-header-fix-effect="slide">
    <div class="u-header__section u-header__shadow-on-show-hide py-4 py-xl-0">
        @if(isset($announcement))
        @if($announcement->status == 1)
        <!-- Topbar -->
        <div class="container-fluid u-header__hide-content u-header__topbar u-header__topbar-lg border-bottom border-color-white bg-topbar">
            <div class="align-items-center topbar-announcement">
                <div class="align-items-center">
                    <i class="fas fa-bullhorn icon-topbar"></i>{{ $announcement->name }}. <a class="link-topbar" href="{{ $announcement->url }}">Lihat selengkapnya</a>
                </div>
            </div>
        </div>
        @endif
        @endif
        <!-- End Topbar -->
        <!-- Topbar -->
        <div class="container-fluid u-header__hide-content u-header__topbar u-header__topbar-lg border-bottom border-color-white">
            <div class="d-flex align-items-center">
                <ul class="list-inline u-header__topbar-nav-divider mb-0">
                    <li class="list-inline-item mr-0"><a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang" class="u-header__navbar-link" target="_blank">0812-1663-2836</a></li>
                    <li class="list-inline-item mr-0"><a href="mailto:giliketapangtrip1@gmail.com" class="u-header__navbar-link" target="_blank">giliketapangtrip1@gmail.com</a></li>
                </ul>
                <div class="ml-auto d-flex align-items-center">
                    <ul class="list-inline mb-0 mr-2 pr-1">
                        <li class="list-inline-item">
                            <a class="btn btn-sm btn-icon btn-pill btn-soft-white btn-bg-transparent transition-3d-hover" href="https://www.facebook.com/giliketapangtrip.co.id" target="_blank">
                                <span class="fab fa-facebook-f btn-icon__inner"></span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="btn btn-sm btn-icon btn-pill btn-soft-white btn-bg-transparent transition-3d-hover" href="https://www.instagram.com/giliketapang.trip" target="_blank">
                                <span class="fab fa-instagram btn-icon__inner"></span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="btn btn-sm btn-icon btn-pill btn-soft-white btn-bg-transparent transition-3d-hover" href="https://www.youtube.com/" target="_blank">
                                <span class="fab fa-youtube btn-icon__inner"></span>
                            </a>
                        </li>
                    </ul>
                    <div class="position-relative px-3 u-header__topbar-divider">
                        <div class="d-flex align-items-center text-white">
                            <a href="{{ route('check_order') }}" class="text-white u-header__navbar-link"><i class="flaticon-shopping-basket mr-2 ml-1 font-size-18"></i> <span class="d-inline-block font-size-14 mr-1">Cek status order</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Topbar -->
        <div id="logoAndNav" class="container-fluid py-xl-4">
            <!-- Nav -->
            <nav class="js-mega-menu navbar navbar-expand-xl u-header__navbar u-header__navbar--no-space my-1">
                <!-- Logo -->
                <a class="navbar-brand u-header__navbar-brand-default u-header__navbar-brand-center u-header__navbar-brand-text-white" href="{{url ('/')}}" aria-label="MyYacht">
                    <img src="{{asset ('/img/logo/logo-white.png')}}" alt="Logo" height="53px" width="55px" style="margin-bottom: 0;">
                    <span class="u-header__navbar-brand-text">Gili Ketapang Trip</span>
                </a>
                <!-- End Logo -->

                <!-- Handheld Logo -->
                <a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center u-header__navbar-brand-collapsed" href="{{url ('/')}}" aria-label="MyYacht">
                    <img src="{{asset ('/img/logo/logo.png')}}" alt="Logo" height="53px" width="55px" style="margin-bottom: 0;">
                    <span class="u-header__navbar-brand-text">Gili Ketapang Trip</span>
                </a>
                <!-- End Handheld Logo -->

                <!-- Scroll Logo -->
                <a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center u-header__navbar-brand-on-scroll" href="{{url ('/')}}" aria-label="MyYacht">
                    <img src="{{asset ('/img/logo/logo.png')}}" alt="Logo" height="53px" width="55px" style="margin-bottom: 0;">
                    <span class="u-header__navbar-brand-text">Gili Ketapang Trip</span>
                </a>
                <!-- End Scroll Logo -->

                <!-- Responsive Toggle Button -->
                <button type="button" class="navbar-toggler btn u-hamburger u-hamburger--primary order-2 ml-3" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
                    <span id="hamburgerTrigger" class="u-hamburger__box">
                        <span class="u-hamburger__inner"></span>
                    </span>
                </button>
                <!-- End Responsive Toggle Button -->

                <!-- Navigation -->
                <div id="navBar" class="navbar-collapse u-header__navbar-collapse collapse order-2 order-xl-0 pt-4 p-xl-0 position-relative">
                    <ul class="navbar-nav u-header__navbar-nav">
                        <!-- Home -->
                        <li class="nav-item u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut">
                            <a id="homeMenu" class="nav-link u-header__nav-link" href="{{url('/')}}" aria-haspopup="true" aria-expanded="false" aria-labelledby="homeSubMenu">Home</a>
                        </li>
                        <!-- End Home -->

                        <!-- Layanan -->
                        <li class="nav-item hs-has-sub-menu u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut">
                            <a id="hotelMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle u-header__nav-link-border" href="#" aria-haspopup="true" aria-expanded="false" aria-labelledby="hotelSubMenu">Paket Wisata</a>
                            <!-- Hotel Submenu -->
                            <ul id="hotelSubMenu" class="hs-sub-menu u-header__sub-menu u-header__sub-menu-rounded u-header__sub-menu-bordered hs-sub-menu-right u-header__sub-menu--spacer" aria-labelledby="hotelMenu" style="min-width: 230px;">
                                <li><a class="nav-link u-header__sub-menu-nav-link" href="{{url ('/activities/snorkeling')}}">Snorkeling</a></li>
                                <li><a class="nav-link u-header__sub-menu-nav-link" href="{{url ('/activities/pemancingan')}}">Wisata Pemancingan</a></li>
                                <li><a class="nav-link u-header__sub-menu-nav-link" href="{{url ('/activities/banana-boat')}}">Banana Boat</a></li>
                                <li><a class="nav-link u-header__sub-menu-nav-link" href="{{url ('/activities/drone')}}">Drone Gili Ketapang</a></li>
                                <li><a class="nav-link u-header__sub-menu-nav-link" href="{{url ('/activities/gua-kucing')}}">Gua Kucing</a></li>
                            </ul>
                            <!-- End Layanan Submenu -->
                        </li>
                        <!-- End Hotel -->

                        <!-- Abou Us -->
                        <li class="nav-item u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut" data-max-width="722px" data-position="right">
                            <a id="tourMegaMenu" class="nav-link u-header__nav-link" href="{{url('about')}}" aria-haspopup="true" aria-expanded="false">Tentang Kami</a>
                        </li>
                        <!-- End Abou Us -->

                        <!-- Galery -->
                        <li class="nav-item u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut">
                            <a id="ActivityMenu" class="nav-link u-header__nav-link" href="{{url('galery')}}" aria-haspopup="true" aria-expanded="false" aria-labelledby="ActivitySubMenu">Galeri</a>
                        </li>
                        <!-- End Galeri -->

                        <!-- Blog -->
                        <li class="nav-item u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut">
                            <a id="rentalMenu" class="nav-link u-header__nav-link" href="{{url('blog')}}" aria-haspopup="true" aria-expanded="false" aria-labelledby="rentalSubMenu">Blog</a>
                        </li>

                        <li class="nav-item u-header__nav-item menucek" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut">
                            <a id="rentalMenu" class="nav-link u-header__nav-link" href="{{url('check_order')}}" aria-haspopup="true" aria-expanded="false" aria-labelledby="rentalSubMenu">Cek Status Order</a>
                        </li>
                        <!-- End Rental -->                       
                    </ul>
                </div>
                <!-- End Navigation -->

                <!-- Button -->
                @if(strpos(request()->fullUrl(), 'activities') === false )
                @if(strpos(request()->fullUrl(), 'booking') === false )
                <div class="pl-4 ml-1 u-header__last-item-btn u-header__last-item-btn-lg">
                    <a class="btn btn-wide rounded-sm btn-outline-white border-width-2 transition-3d-hover" href="/booking" target="_blank">
                        <i class="flaticon-right-arrow font-size-16 mr-2"></i>
                        <span class="d-inline-block">Pesan Sekarang</span>
                    </a>
                </div>
                @endif
                @endif

                <!-- End Button -->
            </nav>
            <!-- End Nav -->
        </div>
    </div>
</header>
<!-- ========== End HEADER ========== -->