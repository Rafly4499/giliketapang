<aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar" data-sidebarbg="skin6">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/dashboard') }}" aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                <li class="list-divider"></li>
                <li class="nav-small-cap"><span class="hide-menu">Components</span></li>
                <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/announcement') }}"
                    aria-expanded="false"><i data-feather="volume-2" class="feather-icon"></i><span
                        class="hide-menu">Pengumuman
                    </span></a>
            </li>
                <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/activities') }}"
                    aria-expanded="false"><i class="icon-badge"></i><span
                        class="hide-menu">Aktivitas
                    </span></a>
            </li>

                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/booking') }}"
                            aria-expanded="false"><i data-feather="bar-chart" class="feather-icon"></i><span
                                class="hide-menu">Booking
                            </span></a>
                    </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/customer') }}"
                            aria-expanded="false"><i data-feather="user" class="feather-icon"></i><span
                                class="hide-menu">Customer
                            </span></a>
                    </li>
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                        aria-expanded="false"><i data-feather="grid" class="feather-icon"></i><span
                            class="hide-menu">Blogs </span></a>
                    <ul aria-expanded="false" class="collapse  first-level base-level-line">
                        <li class="sidebar-item"><a href="{{ url('/panel/blog') }}" class="sidebar-link"><span
                                    class="hide-menu"> List Blog
                                </span></a>
                        </li>
                        <li class="sidebar-item"><a href="{{ url('/panel/categoryblog') }}" class="sidebar-link"><span
                                    class="hide-menu"> Kategori Blog
                                </span></a>
                        </li>
                        
                    </ul>
                </li>
                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/contact') }}"
                        aria-expanded="false"><i data-feather="sidebar" class="feather-icon"></i><span
                            class="hide-menu">Pesan
                        </span></a>
                </li>
                <li class="nav-small-cap"><span class="hide-menu">Utilities</span></li>

                <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" aria-expanded="false"><i data-feather="log-out" class="feather-icon"></i><span
                                    class="hide-menu">{{ __('Logout') }}</span></a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
