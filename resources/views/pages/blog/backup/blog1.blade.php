@extends('layouts.landing')

@section('content')

<!-- Hero Section -->
<div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url(../img/1920x400/img3.jpg);">
    <div class="container space-top-xl-3 py-6 py-xl-0">
        <div class="row justify-content-center py-xl-4">
            <!-- Info -->
            <div class="py-xl-10 py-5">
                <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">{{$label}}</h1>
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb breadcrumb-no-gutter justify-content-center mb-0">
                  <li class="breadcrumb-item font-size-14"> <a class="text-white" href="{{url ('/')}}">Home</a> </li>
                    <li class="breadcrumb-item custom-breadcrumb-item font-size-14 text-white active" aria-current="page">Blog</li>
                  </ol>
                </nav>
            </div>
            <!-- End Info -->
        </div>
    </div>
</div>
<!-- End Hero Section -->

<br>
<br>

<div class="mb-6 mb-lg-8 pb-lg-1 pt-1">
    <div class="container mb-5 mb-lg-9">
        <div class="row">
            <div class="col-lg-8 col-xl-9">
                <div class="js-slick-carousel u-slick mb-4" data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle" data-arrow-left-classes="flaticon-back u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-3 ml-xl-5" data-arrow-right-classes="flaticon-next u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-3 mr-xl-5">
                    <div class="js-slide bg-img-hero-center min-height-450 rounded-xs" style="background-image: url(../img/955x450/img1.jpg);"></div>

                    <div class="js-slide bg-img-hero-center min-height-450 rounded-xs" style="background-image: url(../img/955x450/img2.jpg);"></div>

                    <div class="js-slide bg-img-hero-center min-height-450 rounded-xs" style="background-image: url(../img/955x450/img3.jpg);"></div>

                    <div class="js-slide bg-img-hero-center min-height-450 rounded-xs" style="background-image: url(../img/955x450/img4.jpg);"></div>
                </div>

                <h5 class="font-weight-bold font-size-21 text-gray-3">
                    <a href="blog-single.html">{{$label}}</a>
                </h5>
                <div class="mb-3">
                    <a class="mr-3 pr-1" href="#">
                        <span class="font-weight-normal text-gray-3">Maret 21, 2020</span>
                    </a>
                    <a href="#">
                        <span class="text-primary font-weight-normal">Tips</span>
                    </a>
                </div>
                <p class="text-lh-lg text-gray-1 mb-5">Sesi berlibur merupakan hal yang paling ditunggu-tunggu, terlebih lagi dengan berbagai destinasi wisata menarik yang mampu merileksasikan pikiran sejenak. Namun, terkadang kita seringkali menyia-nyiakan liburan dan tidak memanfaatkan nya dengan baik dengan tidak merencanakan segala sesuatunya secara matang. Padahal, destinasi wisata merupakan hal yang paling ditunggu-tunggu, oleh sebab itu kita juga harus memaksimalkan hal tersebut dengan baik dan sesuai.</p>
                <div class="border border-color-8 mb-4"></div>
            </div>

            <div class="col-lg-4 col-xl-3">
                <form class="input-group input-group-borderless mb-5">
                <!-- Input -->
                <div class="js-focus-state w-100">
                    <div class="input-group border border-color-8 border-width-2 rounded d-flex align-items-center">
                        <input type="text" class="form-control font-size-14 placeholder-1 ml-1" placeholder="Cari Topik" aria-label="Company or title">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="flaticon-magnifying-glass-1 font-size-20 text-gray-8 mr-1"></i>
                             </span>
                        </div>
                    </div>
                </div>
                <!-- End Input -->
                </form>
                <!-- List -->
                <ul id="sidebarNav" class="custom-dropdown list-unstyled border border-color-7 rounded pt-4 pb-1 mb-5">
                    <h5 class="font-weight-bold font-size-17 text-gray-6 mb-2 pb-1 px-4">Kategori</h5>
                    <li class="list-item">
                        <a class="d-block dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav1Collapse" data-target="#sidebarNav1Collapse">
                            <span class="font-weight-normal text-gray-1">Semua</span>
                        </a>

                        <div id="sidebarNav1Collapse" class="collapse" data-parent="#sidebarNav">
                            <ul id="sidebarNav1" class="list-unstyled">
                                <!-- Menu List -->
                                <li><a class="dropdown-item" href="#">Info</a></li>
                                <li><a class="dropdown-item" href="#">Tips</a></li>
                                <li><a class="dropdown-item" href="#">Wisatawan</a></li>
                                <li><a class="dropdown-item" href="#">Snorkeling</a></li>
                                <li><a class="dropdown-item" href="#">Drone Gili consoles</a></li>
                                <li><a class="dropdown-item" href="#">Gua Kucing cameras</a></li>
                                <li><a class="dropdown-item" href="#">Banana Boat</a></li>
                                <!-- End Menu List -->
                            </ul>
                        </div>
                    </li>
                </ul>
                <!-- End List -->

                <div class="border border-color-7 rounded p-4 mb-5">
                                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-star font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Bisa Booking Kapanpun</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Pembayaran Mudah</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-favorites-button font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Tour Guide </a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-airplane font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Basecamp</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-airplane font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Admin Online 24 Jam</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-airplane font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                                            </h6>
                                        </div>
                                    </div>

                <div class="border border-color-7 rounded p-4">
                    <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Tags</h6>
                    <div class="d-flex flex-wrap">
                        <a class="btn d-flex align-items-center justify-content-center btn-gray-1 rounded-xs transition-3d-hover font-size-14 text-gray-1 height-35 width-70 mr-2 mb-2" href="https://madrasthemes.github.io/mytravel-html/html/tour/tour-list.html">
                            Tour
                        </a>
                        <a class="btn d-flex align-items-center justify-content-center btn-gray-1 rounded-xs transition-3d-hover font-size-14 text-gray-1 height-35 width-90 mr-2 mb-2" href="https://madrasthemes.github.io/mytravel-html/html/rental/rental-list.html">
                            Snorkeling
                        </a>
                        <a class="btn d-flex align-items-center justify-content-center btn-gray-1 rounded-xs transition-3d-hover font-size-14 text-gray-1 height-35 width-70 mr-2 mb-2" href="https://madrasthemes.github.io/mytravel-html/html/cars/cars-list.html">
                            Tips
                        </a>
                        <a class="btn d-flex align-items-center justify-content-center btn-gray-1 rounded-xs transition-3d-hover font-size-14 text-gray-1 height-35 width-80 mr-2 mb-2" href="https://madrasthemes.github.io/mytravel-html/html/yacht/yacht-list.html">
                            Galery
                        </a>
                        <a class="btn d-flex align-items-center justify-content-center btn-gray-1 rounded-xs transition-3d-hover font-size-14 text-gray-1 height-35 width-90 mr-2 mb-2" href="https://madrasthemes.github.io/mytravel-html/html/activities/activities-list.html">
                            Info
                        </a>
                        <a class="btn d-flex align-items-center justify-content-center btn-gray-1 rounded-xs transition-3d-hover font-size-14 text-gray-1 height-35 width-90 mb-2" href="https://madrasthemes.github.io/mytravel-html/html/tour/tour-list.html">
                            Promo
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="border border-color-8"></div>
</div>

@endsection