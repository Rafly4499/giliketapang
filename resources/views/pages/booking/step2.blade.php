@extends('layouts.landing')

@section('content')
<!-- ========== MAIN CONTENT ========== -->
<main id="content" class="bg-gray">
    <!-- Hero Section -->
    <div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url({{ asset ('img/1920x600/img88.jpg')}});">
        <div class="container space-top-xl-3 py-6 py-xl-0">
            <div class="row justify-content-center py-xl-4">
                <!-- Info -->
                <div class="py-xl-10 py-5">
                    <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">Booking</h1>
                </div>
                <!-- End Info -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-9">
                <div class="mb-5 mt-5 shadow-soft bg-white rounded-sm">
                    <div class="py-3 px-4 px-xl-12 border-bottom">
                        <ul class="list-group flex-nowrap overflow-auto overflow-md-visble list-group-horizontal list-group-borderless flex-center-between pt-1">
                            <li class="list-group-item text-center flex-shrink-0 flex-xl-shrink-1">
                                <div class="flex-content-center mb-3 width-40 height-40 border  border-width-2 border-gray mx-auto rounded-circle">
                                    1
                                </div>
                                <div class="text-gray-1">Customer information</div>
                            </li>
                            <li class="list-group-item text-center flex-shrink-0 flex-xl-shrink-1">
                                <div class="flex-content-center mb-3 width-40 height-40 bg-primary border-width-2 border border-primary text-white mx-auto rounded-circle">
                                    2
                                </div>
                                <div class="text-primary">Payment information</div>
                            </li>
                            <li class="list-group-item text-center flex-shrink-0 flex-md-shrink-1">
                                <div class="flex-content-center mb-3 width-40 height-40 border  border-width-2 border-gray mx-auto rounded-circle">
                                    3
                                </div>
                                <div class="text-gray-1">Review Booking!</div>
                            </li>
                        </ul>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark mb-4">
                            Your Payment Information
                        </h5>
                        <!-- Nav Classic -->
                        <ul class="nav nav-classic nav-choose border-0 nav-justified border mx-n3" role="tablist">
                            <li class="nav-item mx-3 mb-4 mb-md-0">
                                <a class="rounded py-5 border-width-2 border nav-link font-weight-medium active" id="pills-one-example2-tab" data-toggle="pill" href="#pills-one-example2" role="tab" aria-controls="pills-one-example2" aria-selected="true">
                                    <div class="height-25 width-25 flex-content-center bg-primary rounded-circle position-absolute left-0 top-0 ml-2 mt-2">
                                        <i class="flaticon-tick text-white font-size-15"></i>
                                    </div>
                                    <div class="d-md-flex justify-content-md-center align-items-md-center flex-wrap">
                                        <img class="img-fluid mb-3" style="width:200px;" src="{{asset ('img/logo/bri.png')}}" alt="Image-Description">
                                        <div class="w-100 text-dark">
                                        <p>An.ABDUL MUIS <br> 007301026338537</p>
                                        Pembayaran melalui bank
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item mx-3">
                                <a class="rounded py-5 border-width-2 border nav-link font-weight-medium" id="pills-two-example2-tab" data-toggle="pill" href="#pills-two-example2" role="tab" aria-controls="pills-two-example2" aria-selected="false">
                                    <div class="height-25 width-25 flex-content-center bg-primary rounded-circle position-absolute left-0 top-0 ml-2 mt-2">
                                        <i class="flaticon-tick text-white font-size-15"></i>
                                    </div>
                                    <div class="d-md-flex justify-content-md-center align-items-md-center flex-wrap">
                                        <img class="img-fluid mb-3" style="width:180px;" src="{{asset ('img/pay.png')}}" alt="Image-Description">
                                        <div class="w-100 text-dark">COD (Cash On Delivery)</div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- End Nav Classic -->

                        <!-- Tab Content -->
                        <div class="tab-content">
                            <div class="tab-pane fade pt-8 show active" id="pills-one-example2" role="tabpanel" aria-labelledby="pills-one-example2-tab">
                                <!-- Payment Form -->
                                <form action="{{ route('booking.step.two.post') }}" method="POST" class="js-validate">
                                        @csrf
                                    <div class="row">
                                        <!-- Input -->
                                        <input type="hidden" class="form-control" name="method_payment" value="Bank Transfer"/>
                                        <div class="col-sm-12 mb-4">
                                            <div class="js-form-message">
                                                <div class="form-group">
                                                <label class="form-label">
                                                    Tipe Pembayaran Bank   :
                                                </label>
    
                                                
                                                <label class="radio-inline labelbooking"><input type="radio" class="radiobooking" name="type_payment_bank" value="Full Pembayaran" {{{ (isset($booking->type_payment_bank) && $booking->type_payment_bank == 'Full Pembayaran') ? "checked" : "" }}} checked
                                                data-msg="Masukkan tipe pembayaran bank."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success">Full Pembayaran</label>
                                                <label class="radio-inline labelbooking"><input type="radio" class="radiobooking" name="type_payment_bank" value="DP30%" {{{ (isset($booking->type_payment_bank) && $booking->type_payment_bank == 'DP30%') ? "checked" : "" }}}> DP 30%</label>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- End Input -->
                                        <div class="col-sm-12">
                                            <div class="row">
                                            <div class="col-sm-6 mb-4">
                                                <a class="btn btn-default border w-100 rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3" href="{{ route('booking.step.one') }}">BACK STEP</a>
                                            </div>
                                            <div class="col-sm-6 mb-4">
                                                <button type="submit" class="btn btn-primary w-100 rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3">CONFIRM PAYMENT</button>
                                            </div>
                                            </div>
                                            <!-- End Checkbox -->
                                        </div>
                                        
                                    </div>
                                </form>
                                <!-- End Payment Form -->
                            </div>

                            <div class="tab-pane fade pt-8" id="pills-two-example2" role="tabpanel" aria-labelledby="pills-two-example2-tab">
                                <form action="{{ route('booking.step.two.post') }}" method="POST" class="js-validate">
                                    @csrf
                                    <!-- Login -->
                                    <div id="login" data-target-group="idForm">
                                        <input type="hidden" class="form-control" name="method_payment" value="COD"/>
                                        <div class="pt-4 border-bottom">
                                        <!-- End Input -->
                                        <div class="col-sm-12">
                                            <div class="row">
                                            <div class="col-sm-6 mb-4">
                                                <a class="btn btn-default border w-100 rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3" href="{{ route('booking.step.one') }}">BACK STEP</a>
                                            </div>
                                            <div class="col-sm-6 mb-4">
                                                <button type="submit" class="btn btn-primary w-100 rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3">CONFIRM PAYMENT</button>
                                            </div>
                                            </div>
                                            <!-- End Checkbox -->
                                        </div>
                                        
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Tab Content -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3">
                <div class="mb-4">
                
                    <div class="border border-color-7 shadow-soft bg-white rounded p-4 mb-5">
                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Bisa Booking Kapanpun</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Pembayaran Mudah</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Tersedia Tour Guide </a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Tersedia Basecamp</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Admin Online 24 Jam</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                            </h6>
                        </div>
                    </>
                </div>
            </div>
            <div class="mb-4">
                <div class="border border-color-7 shadow-soft bg-white rounded p-4 mb-5">
                <div>
                    <small class="d-block font-size-18 font-weight-normal text-primary">Booking / Info: <span class="text-primary font-weight-semi-bold"><br>0812 1663 2836</span></small>
                    <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang
    " target="_blank"><img src="https://gosocial.co.id/assets/landing/image/wa.png" width="200px" height="47px" alt="Whatsapp"></a>
                </div>
            </div>
            </div>
        </div>
    </div>
</main>
@endsection
<!-- ========== END MAIN CONTENT ========== -->