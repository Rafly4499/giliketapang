@extends('layouts.landing')

@section('content')

<!-- ========== MAIN CONTENT ========== -->
<main id="content">
    <!-- Hero Section -->
    <div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url({{ asset ('img/1920x600/imagesvn.jpg')}});">
        <div class="container space-top-xl-3 py-6 py-xl-0">
            <div class="row justify-content-center py-xl-4">
                <!-- Info -->
                <div class="py-xl-10 py-5">
                    <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">{{$title}}</h1>
                </div>
                <!-- End Info -->
            </div>
        </div>
    </div>
    <!-- End Hero Section -->
    <!-- Breadcrumb -->
    <div class="container">
        <nav class="py-3" aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-no-gutter mb-0 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{url ('/')}}">Home</a></li>
                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">{{$label}}</li>
            </ol>
        </nav>
    </div>
    <!-- End Breadcrumb -->
    <div class="container">
        <div class="row">
            @if (session()->has('success'))
            <script>
            Swal.fire({
                title: 'Success!',
                text:  'Anda berhasil mengonfirmasi pembayaran. Silahkan cek status order anda',
                icon: 'success',
                confirmButtonText: 'Ok'
              })
            </script>
            @endif 
            <div class="col-lg-8 col-xl-9">
                <div class="d-block d-md-flex flex-center-between align-items-start mb-3">
                    <div class="mb-1 tabelfull">
                        <div class="mb-2 mb-md-0">
                            <h4 class="font-size-23 font-weight-bold mb-1 mr-3">Cek Status Order Anda</h4>
                        </div>
                        <div class="d-block d-md-flex flex-horizontal-center">
                            <div class="d-block d-md-flex flex-horizontal-center font-size-14 text-gray-1 mb-2 mb-md-0">
                                <i class="icon flaticon-placeholder mr-2 font-size-20"></i> Gili Ketapang Trip
                            </div>
                        </div>
                        <div class="py-4 border-top border-bottom mb-4">
                            <div class="pt-4 pb-5 px-5">
                                <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark mb-4">
                                    Masukkan nomor booking dan nomor handphone anda
                                </h5>
                                <!-- Contacts Form -->
                                <form action="{{ route('result_order') }}" method="POST" class="js-validate">
                                    @csrf
                                    <div class="row">
                                        <!-- Input -->
                                        <div class="col-sm-12 mb-4">
                                            <div class="js-form-message">
                                                <label class="form-label">
                                                    Nomor Booking
                                                </label>
                                                <input type="number" class="form-control" name="nobooking" placeholder="Masukkan Nomor Booking" aria-label="Masukkan Nomor Booking" required
                                                data-msg="Masukkan Nomor Booking"
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success">
                                            </div>
                                        </div>
                                        <!-- End Input -->
        
                                        <!-- Input -->
                                        <div class="col-sm-12 mb-4">
                                            <div class="js-form-message">
                                                <label class="form-label">
                                                    Nomor Handphone
                                                </label>
                                                <input type="number" class="form-control" name="nohp" placeholder="Masukkan Nomor Handphone" aria-label="Masukkan Nomor Handphone" required
                                                data-msg="Masukkan Nomor Handphone"
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success">
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                <!-- End Input -->

                                <div class="col-sm-12 align-self-end">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary btn-wide rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3">Cek Order</button>
                                    </div>
                                </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-bottom py-4">
                    <h5 class="font-size-21 font-weight-bold text-dark mb-4">
                        Anda juga mungkin bertanya
                    </h5>
                    <div id="basicsAccordion">
                        <!-- Card -->
                        <div class="card border-0 mb-4 pb-1">
                            <div class="card-header border-bottom-0 p-0" id="basicsHeadingOne">
                                <h5 class="mb-0">
                                    <button type="button" class="collapse-link btn btn-link btn-block d-flex align-items-md-center p-0"
                                    data-toggle="collapse" data-target="#basicsCollapseOne" aria-expanded="true" aria-controls="basicsCollapseOne">
                                        <div class="border border-color-8 rounded-xs border-width-2 p-2 mb-3 mb-md-0 mr-4">
                                            <figure id="rectangle" class="minus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/rectangle.svg" alt="SVG" data-parent="#rectangle">
                                            </figure>
                                            <figure id="plus1" class="plus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/plus.svg" alt="SVG" data-parent="#plus1">
                                            </figure>
                                        </div>

                                        <h6 class="font-weight-bold text-gray-3 mb-0">Bagaimana cara booking trip?</h6>
                                    </button>
                                </h5>
                            </div>
                            <div id="basicsCollapseOne" class="collapse show"
                                 aria-labelledby="basicsHeadingOne"
                                 data-parent="#basicsAccordion">
                                <div class="card-body pl-10 pl-md-10 pr-md-12 pt-0">
                                    <p class="mb-0 text-gray-1 text-lh-lg">Mudah, cukup hubungi Kami melalui WA, bisa juga dengan tekan Tombol <b>Pesan Sekarang</b> atau <b>Booking Sekarang</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End Card -->

                        <!-- Card -->
                        <div class="card border-0 mb-4 pb-1">
                            <div class="card-header border-bottom-0 p-0" id="basicsHeadingTwo">
                                <h5 class="mb-0">
                                    <button type="button" class="collapse-link btn btn-link btn-block d-flex align-items-md-center p-0"
                                    data-toggle="collapse" data-target="#basicsCollapseTwo" aria-expanded="false" aria-controls="basicsCollapseTwo">
                                        <div class="border border-color-8 rounded-xs border-width-2 p-2 mb-3 mb-md-0 mr-4">
                                            <figure id="rectangle1" class="minus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/rectangle.svg" alt="SVG" data-parent="#rectangle1">
                                            </figure>
                                            <figure id="plus2" class="plus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/plus.svg" alt="SVG" data-parent="#plus2">
                                            </figure>
                                        </div>
                                        <h6 class="font-weight-bold text-gray-3 mb-0">Berapa minimal jumlah orang/ rombongan ke sana?</h6>
                                    </button>
                                </h5>
                            </div>
                            <div id="basicsCollapseTwo" class="collapse"
                                 aria-labelledby="basicsHeadingTwo"
                                 data-parent="#basicsAccordion">
                                <div class="card-body pl-10 pl-md-10 pr-md-12 pt-0">
                                    <p class="mb-0 text-gray-1 text-lh-lg">Tidak ada jumlah minimal rombongan untuk memesan layanan Wisata kami. Jika Anda sendiri atau berdua, Anda juga bisa memesan layanan wisata kami.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End Card -->

                        <!-- Card -->
                        <div class="card border-0 mb-4 pb-1">
                            <div class="card-header border-bottom-0 p-0" id="basicsHeadingThree">
                                <h5 class="mb-0">
                                    <button type="button" class="collapse-link btn btn-link btn-block d-flex align-items-md-center p-0"
                                    data-toggle="collapse" data-target="#basicsCollapseThree" aria-expanded="false" aria-controls="basicsCollapseThree">
                                        <div class="border border-color-8 rounded-xs border-width-2 p-2 mb-3 mb-md-0 mr-4">
                                            <figure id="rectangle2" class="minus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/rectangle.svg" alt="SVG" data-parent="#rectangle2">
                                            </figure>
                                            <figure id="plus3" class="plus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/plus.svg" alt="SVG" data-parent="#plus3">
                                            </figure>
                                        </div>
                                        <h6 class="font-weight-bold text-gray-3 mb-0">Apakah ada harga khusus untuk rombongan?</h6>
                                    </button>
                                </h5>
                            </div>
                            <div id="basicsCollapseThree" class="collapse"
                                 aria-labelledby="basicsHeadingThree"
                                 data-parent="#basicsAccordion">
                                <div class="card-body pl-10 pl-md-10 pr-md-12 pt-0">
                                    <p class="mb-0 text-gray-1 text-lh-lg">Jangan khawatir, jika Anda datang bersama rombongan, akan mendapatkan potongan harga dari kami. 
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End Card -->

                        <!-- Card -->
                        <div class="card border-0 mb-4 pb-1">
                            <div class="card-header border-bottom-0 p-0" id="basicsHeadingFour">
                                <h5 class="mb-0">
                                    <button type="button" class="collapse-link btn btn-link btn-block d-flex align-items-md-center p-0"
                                    data-toggle="collapse" data-target="#basicsCollapseFour" aria-expanded="false" aria-controls="basicsCollapseFour">
                                        <div class="border border-color-8 rounded-xs border-width-2 p-2 mb-3 mb-md-0 mr-4">
                                            <figure id="plus4" class="plus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/plus.svg" alt="SVG" data-parent="#plus4">
                                            </figure>
                                            <figure id="rectangle3" class="minus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/rectangle.svg" alt="SVG" data-parent="#rectangle3">
                                            </figure>
                                        </div>
                                        <h6 class="font-weight-bold text-gray-3 mb-0">Bagaimana jika saya sampai di sana malam hari?</h6>
                                    </button>
                                </h5>
                            </div>
                            <div id="basicsCollapseFour" class="collapse"
                                 aria-labelledby="basicsHeadingFour"
                                 data-parent="#basicsAccordion">
                                <div class="card-body pl-10 pl-md-10 pr-md-12 pt-0">
                                    <p class="mb-0 text-gray-1 text-lh-lg">Kami menyediakan tempat untuk bermalam di Probolinggo jika Anda datang saat malam hari. Biayanya <b>Gratis</b>, dan paginya akan langsung kami antar menuju Pulau Gili Ketapang.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3">
                                <div class="mb-4">
                                
                                    <div class="border border-color-7 rounded p-4 mb-5">
                                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Bisa Booking Kapanpun</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Pembayaran Mudah</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Tour Guide </a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Basecamp</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Admin Online 24 Jam</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                                            </h6>
                                        </div>
                                    </>
                                </div>
                            </div>
                            <div class="mb-4">
                                <div class="border border-color-7 rounded p-4 mb-5">
                                <div>
                                    <small class="d-block font-size-18 font-weight-normal text-primary">Booking / Info: <span class="text-primary font-weight-semi-bold"><br>0812 1663 2836</span></small>
                                    <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang
                    " target="_blank"><img src="https://gosocial.co.id/assets/landing/image/wa.png" width="200px" height="47px" alt="Whatsapp"></a>
                                </div>
                            </div>
                            </div>
        </div>
        <!-- Product Cards carousel -->
    
        <!-- End Product Cards carousel -->
    </div>
</main>
<!-- ========== END MAIN CONTENT ========== -->

@endsection