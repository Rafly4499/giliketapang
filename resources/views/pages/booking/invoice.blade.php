@extends('layouts.landing')

@section('content')
<!-- ========== MAIN CONTENT ========== -->
<main id="content" class="bg-gray">
    <!-- Hero Section -->
    <div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url({{ asset ('img/1920x600/img88.jpg')}});">
        <div class="container space-top-xl-3 py-6 py-xl-0">
            <div class="row justify-content-center py-xl-4">
                <!-- Info -->
                <div class="py-xl-10 py-5">
                    <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">Detail Booking</h1>
                </div>
                <!-- End Info -->
            </div>
        </div>
    </div>
    <div class="container">
        @if (session()->has('success'))
            <script>
            Swal.fire({
                title: 'Success!',
                text:  'Anda berhasil melakukan booking. Silahkan melakukan pembayaran',
                icon: 'success',
                confirmButtonText: 'Ok'
              })
            </script>
            @endif 
        <div class="row">
            
            <div class="col-lg-8 col-xl-9">
                <div class="mb-5 mt-5 shadow-soft bg-white rounded-sm">
                    <div class="py-6 px-5 border-bottom">
                        <div class="flex-horizontal-center">
                            <div class="height-50 width-50 flex-shrink-0 flex-content-center bg-primary rounded-circle">
                                <i class="flaticon-shopping-basket text-white font-size-24"></i>
                            </div>
                            <div class="ml-3">
                                <h3 class="font-size-18 font-weight-bold text-dark mb-0 text-lh-sm">
                                    Detail Booking Gili Ketapang Trip
                                </h3>
                                <p class="mb-0">Booking anda telah dikonfirmasi.</p>
                            </div>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5 border-bottom">
                        <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark mb-2">
                            Booking Information
                        </h5>
                        <ul class="list-unstyled font-size-1 mb-0 font-size-16">
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Aktivitas</span>
                                <span class="text-secondary text-right">{{$booking->activities['name_activities']}}</span>
                            </li>
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Nama Pemesan</span>
                                <span class="text-secondary text-right">{{$booking->name_order}}</span>
                            </li>

                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Asal Kota</span>
                                <span class="text-secondary text-right">{{$booking->city}}</span>
                            </li>

                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Tanggal Booking</span>
                                <span class="text-secondary text-right">{{ date('d M Y', strtotime($booking->date_booking)) }}</span>
                            </li>
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Jumlah Peserta</span>
                                <span class="text-secondary text-right">{{$booking->number_participant}} Peserta</span>
                            </li>
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Harga paket per peserta</span>
                                <span class="text-secondary text-right">Rp. {{ number_format($booking->payment, 0)}},-</span>
                            </li>
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Total Pembayaran</span>
                                <span class="text-secondary text-right">Rp. {{ number_format($booking->total_payment, 0) }},-</span>
                            </li>
                            @if($booking->type_payment_bank != null)
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Tipe Pembayaran</span>
                                @if($booking->type_payment_bank == null)
                                <span class="text-secondary text-right">COD</span>
                                @else
                                <span class="text-secondary text-right">{{ $booking->type_payment_bank }}</span>
                                @endif
                            </li>
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Biaya Pembayaran</span>
                                @if($booking->type_payment_bank == "DP30%")
                                <?php
                                $DP = (30 / 100) * (int)$booking->total_payment;
                                ?>
                                <span class="text-secondary text-right">Rp. {{ number_format($DP, 0) }},-</span>
                                @else
                                <span class="text-secondary text-right">Rp. {{ number_format($booking->total_payment, 0) }},-</span>
                                @endif
                            </li>
                            @endif
                        </ul>
                        <div class="w-100"></div>
                        <div class="pt-4 border-bottom">
                            <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark mb-3">
                                Payment
                            </h5>
                            <p class="">
                                Silahkan melakukan pembayaran ke Rekening Bank BRI 007301026338537 An.ABDUL MUIS jika memilih metode pembayaran melalui Bank.
                            </p>
                        </div>
                        <div class="pt-4 border-bottom">
                            <div class="row">
                            <div class="col-sm-6 mb-4">
                                <a href="/check-order"><button class="btn btn-primary w-100 rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3">Cek Status Order</button></a>
                            </div>
                            @if($booking->status != 'Sudah Bayar')
                            <div class="col-sm-6 mb-4">
                                <a href="{{ url('/confirmation-payment/'.$booking->id) }}"><button type="submit" class="btn btn-primary w-100 rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3">Confirmation Payment</button>
                            </div>
                            @endif
                        </div>
                        </div>
                        <!-- End Fact List -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3">
                    <div class="mb-4">
                        
                        <div class="border border-color-7 shadow-soft bg-white rounded p-4 mb-5">
                            <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                            <div class="d-flex align-items-center mb-3">
                                <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                                <h6 class="mb-0 font-size-14 text-gray-1">
                                    <a href="#">Bisa Booking Kapanpun</a>
                                </h6>
                            </div>
                            <div class="d-flex align-items-center mb-3">
                                <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                                <h6 class="mb-0 font-size-14 text-gray-1">
                                    <a href="#">Pembayaran Mudah</a>
                                </h6>
                            </div>
                            <div class="d-flex align-items-center mb-3">
                                <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                                <h6 class="mb-0 font-size-14 text-gray-1">
                                    <a href="#">Tersedia Tour Guide </a>
                                </h6>
                            </div>
                            <div class="d-flex align-items-center mb-0">
                                <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                                <h6 class="mb-0 font-size-14 text-gray-1">
                                    <a href="#">Tersedia Basecamp</a>
                                </h6>
                            </div>
                            <div class="d-flex align-items-center mb-0">
                                <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                                <h6 class="mb-0 font-size-14 text-gray-1">
                                    <a href="#">Admin Online 24 Jam</a>
                                </h6>
                            </div>
                            <div class="d-flex align-items-center mb-0">
                                <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                                <h6 class="mb-0 font-size-14 text-gray-1">
                                    <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                                </h6>
                            </div>
                        </>
                    </div>
                </div>
                <div class="mb-4">
                    <div class="border border-color-7 shadow-soft bg-white rounded p-4 mb-5">
                    <div>
                        <small class="d-block font-size-18 font-weight-normal text-primary">Booking / Info: <span class="text-primary font-weight-semi-bold"><br>0812 1663 2836</span></small>
                        <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang
        " target="_blank"><img src="https://gosocial.co.id/assets/landing/image/wa.png" width="200px" height="47px" alt="Whatsapp"></a>
                    </div>
                </div>
                </div>
        </div>
    </div>
</main>
    @endsection