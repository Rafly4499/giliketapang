@extends('layouts.landing')

@section('content')
<!-- ========== MAIN CONTENT ========== -->
<main id="content" class="bg-gray">
    <!-- Hero Section -->
    <div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url({{ asset ('img/1920x600/img88.jpg')}});">
        <div class="container space-top-xl-3 py-6 py-xl-0">
            <div class="row justify-content-center py-xl-4">
                <!-- Info -->
                <div class="py-xl-10 py-5">
                    <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">Booking</h1>
                </div>
                <!-- End Info -->
            </div>
        </div>
    </div>
    <!-- End Hero Section -->
    <!-- Breadcrumb -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-9">
                <div class="mb-5 mt-5 shadow-soft bg-white rounded-sm">
                    <div class="py-3 px-4 px-xl-12 border-bottom">
                        <ul class="list-group flex-nowrap overflow-auto overflow-md-visble list-group-horizontal list-group-borderless flex-center-between pt-1">
                            <li class="list-group-item text-center flex-shrink-0 flex-xl-shrink-1">
                                <div class="flex-content-center mb-3 width-40 height-40 bg-primary border-width-2 border border-primary text-white mx-auto rounded-circle">
                                    1
                                </div>
                                <div class="text-primary">Customer information</div>
                            </li>
                            <li class="list-group-item text-center flex-shrink-0 flex-xl-shrink-1">
                                <div class="flex-content-center mb-3 width-40 height-40 border  border-width-2 border-gray mx-auto rounded-circle">
                                    2
                                </div>
                                <div class="text-gray-1">Payment information</div>
                            </li>
                            <li class="list-group-item text-center flex-shrink-0 flex-md-shrink-1">
                                <div class="flex-content-center mb-3 width-40 height-40 border  border-width-2 border-gray mx-auto rounded-circle">
                                    3
                                </div>
                                <div class="text-gray-1">Review Booking!</div>
                            </li>
                        </ul>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark mb-4">
                            Let us know who you are
                        </h5>
                        <!-- Contacts Form -->
                        <form action="{{ route('booking.step.one.post') }}" method="POST" class="js-validate">
                            @csrf
                            <div class="row">
                                <!-- Input -->
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Aktivitas
                                        </label>
                                        <select name="activities_id" class="form-control js-select selectpicker dropdown-select" required="" data-msg="Pilih paket aktivitas." data-error-class="u-has-error" data-success-class="u-has-success"
                                            data-live-search="true"
                                            data-style="form-control font-size-16 border-width-2 border-gray font-weight-normal">
                                            @if(isset($booking->activities_id))
                                            <option value="{{ $booking->activities_id }}">{{$booking->activities['name_activities']}}</option>
                                            @endif
                                            <option value="{{ $booking->activities_id ?? '' }}">Pilih paket aktivitas</option>
                                            @foreach($activities as $data)
                                                <option value="{{ $data->id }}">{{ $data->name_activities }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- End Input -->

                                <!-- Input -->
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Jumlah Peserta
                                        </label>
                                        <input type="number_participant" value="{{ $booking->number_participant ?? '' }}" class="form-control" name="number_participant" placeholder="Masukkan jumlah peserta" aria-label="Masukkan jumlah peserta" required
                                        data-msg="Masukkan jumlah peserta"
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Nama Pemesan
                                        </label>

                                        <input type="text" class="form-control" value="{{ $booking->name_order ?? '' }}" name="name_order" placeholder="Masukkan nama pemesan" aria-label="Ali" required
                                        data-msg="Masukkan nama pemesan."
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <!-- End Input -->

                                <!-- Input -->
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Asal Kota
                                        </label>

                                        <input type="text" class="form-control" value="{{ $booking->city ?? '' }}" name="city" placeholder="Masukkan asal kota" aria-label="TUFAN" required
                                        data-msg="Masukkan asal kota."
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <!-- End Input -->
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Email Pemesan
                                        </label>

                                        <input type="email" class="form-control" value="{{ $booking->email ?? '' }}" name="email" placeholder="Masukkan email" aria-label="email" required
                                        data-msg="Masukkan email"
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Nomor Handphone
                                        </label>

                                        <input type="number" class="form-control" value="{{ $booking->no_hp ?? '' }}" name="no_hp" placeholder="Masukkan nomor handphone" aria-label="no_hp" required
                                        data-msg="Masukkan nomor handphone"
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <!-- Input -->
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Tanggal Booking
                                        </label>

                                        <input type="date" class="form-control" value="{{ $booking->date_booking ?? '' }}" name="date_booking" aria-label="date_booking" required
                                        data-msg="Masukkan tanggal yang ingin dibooking"
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <!-- End Input -->

                                <div class="w-100"></div>

                                <!-- Input -->
                                
                                <!-- End Input -->

                                <div class="w-100"></div>
                                <!-- End Input -->

                                <div class="col-sm-12 align-self-end">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary btn-wide rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3">NEXT PAGE</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- End Contacts Form -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3">
                <div class="mb-4">
                    
                    <div class="border border-color-7 shadow-soft bg-white rounded p-4 mb-5">
                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Bisa Booking Kapanpun</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Pembayaran Mudah</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Tersedia Tour Guide </a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Tersedia Basecamp</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Admin Online 24 Jam</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                            </h6>
                        </div>
                    </>
                </div>
            </div>
            <div class="mb-4">
                <div class="border border-color-7 shadow-soft bg-white rounded p-4 mb-5">
                <div>
                    <small class="d-block font-size-18 font-weight-normal text-primary">Booking / Info: <span class="text-primary font-weight-semi-bold"><br>0812 1663 2836</span></small>
                    <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang
    " target="_blank"><img src="https://gosocial.co.id/assets/landing/image/wa.png" width="200px" height="47px" alt="Whatsapp"></a>
                </div>
            </div>
            </div>
    </div>
    </div>
</main>
@endsection
<!-- ========== END MAIN CONTENT ========== -->