@extends('layouts.landing')

@section('content')
<!-- ========== MAIN CONTENT ========== -->
<main id="content" class="bg-gray">
    <!-- Hero Section -->
    <div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url({{ asset ('img/1920x600/img88.jpg')}});">
        <div class="container space-top-xl-3 py-6 py-xl-0">
            <div class="row justify-content-center py-xl-4">
                <!-- Info -->
                <div class="py-xl-10 py-5">
                    <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">Konfirmasi Pembayaran</h1>
                </div>
                <!-- End Info -->
            </div>
        </div>
    </div>
<div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-9">
                <div class="mb-5 mt-5 shadow-soft bg-white rounded-sm">
                    <div class="pt-4 pb-5 px-5">
                        <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark mb-4">
                            Confirmation Payment
                        </h5>
                        <!-- Contacts Form -->
                        <form action="/confirmation-payment" method="POST" class="js-validate" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <!-- Input -->
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            ID Booking
                                        </label>
                                        <input type="text" class="form-control" name="booking_id" value="{{ $booking->bookingid }}" disabled>
                                        <input type="hidden" class="form-control" name="booking_id" value="{{ $booking->bookingid }}">
                                    </div>
                                </div>
                                <!-- End Input -->

                                <!-- Input -->
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Tujuan Transfer
                                        </label>
                                        <input type="text" class="form-control" name="transfer_destination" placeholder="Masukkan tujuan tranfer" aria-label="Masukkan tujuan tranfer" required
                                        data-msg="Masukkan tujuan tranfer"
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Pemilik Rekening
                                        </label>

                                        <input type="text" class="form-control" name="account_owner" placeholder="Masukkan pemilik rekening" aria-label="Ali" required
                                        data-msg="Masukkan pemilik rekening."
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <!-- End Input -->
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Tanggal Pembayaran
                                        </label>

                                        <input type="date" class="form-control" name="date_payment" aria-label="date_payment" required
                                        data-msg="Masukkan tanggal pembayaran"
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <!-- Input -->
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            Total Pembayaran
                                        </label>

                                        <input type="number" class="form-control" name="total_payment" placeholder="Masukkan total pembayaran" aria-label="TUFAN" required
                                        data-msg="Masukkan total pembayaran."
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <div class="col-sm-6 mb-4">
                                    <div class="js-form-message">
                                        <label class="form-label">
                                            No Handphone
                                        </label>

                                        <input type="number" class="form-control" name="no_hp" placeholder="Masukkan no handphone yang bisa dihubungi" aria-label="TUFAN" required
                                        data-msg="Masukkan no handphone yang bisa dihubungi."
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success">
                                    </div>
                                </div>
                                <div class="col-sm-6 mb-4">
                                   
                                        <label class="form-label">
                                            Bukti Pembayaran
                                        </label>

                                        <input type="file" name="image" id="preview" class="image" required>

                                </div>
                                <div class="col-sm-12 mb-4">
                                    {!! NoCaptcha::renderJs() !!}
                                    {!! NoCaptcha::display() !!}
                                </div>
                                <!-- End Input -->

                                <!-- Input -->
                                
                                <!-- End Input -->

                                <div class="w-100"></div>

                                <!-- Input -->
                                
                                <!-- End Input -->

                                <div class="w-100"></div>
                                <!-- End Input -->

                                <div class="col-sm-12 align-self-end">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary btn-wide rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3">SUBMIT</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- End Contacts Form -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3">
                <div class="mb-4">
                    <div class="border border-color-7 shadow-soft bg-white rounded p-4 mb-5">
                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Bisa Booking Kapanpun</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Pembayaran Mudah</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Tersedia Tour Guide </a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Tersedia Basecamp</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Admin Online 24 Jam</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                            </h6>
                        </div>
                    </div>
                </div>
                <div class="mb-4">
                    <div class="border border-color-7 shadow-soft bg-white rounded p-4 mb-5">
                    <div>
                        <small class="d-block font-size-18 font-weight-normal text-primary">Booking / Info: <span class="text-primary font-weight-semi-bold"><br>0812 1663 2836</span></small>
                        <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang
        " target="_blank"><img src="https://gosocial.co.id/assets/landing/image/wa.png" width="200px" height="47px" alt="Whatsapp"></a>
                    </div>
                </div>
                </div>
            </div>
    </div>
</div>
</main>
@endsection
<!-- ========== END MAIN CONTENT ========== -->