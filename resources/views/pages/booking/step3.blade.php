@extends('layouts.landing')

@section('content')
<!-- ========== MAIN CONTENT ========== -->
<main id="content" class="bg-gray">
    <!-- Hero Section -->
    <div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url({{ asset ('img/1920x600/img88.jpg')}});">
        <div class="container space-top-xl-3 py-6 py-xl-0">
            <div class="row justify-content-center py-xl-4">
                <!-- Info -->
                <div class="py-xl-10 py-5">
                    <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">Booking</h1>
                </div>
                <!-- End Info -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-9">
                <div class="mb-5 mt-5 shadow-soft bg-white rounded-sm">
                    <div class="py-3 px-4 px-xl-12 border-bottom">
                        <ul class="list-group flex-nowrap overflow-auto overflow-md-visble list-group-horizontal list-group-borderless flex-center-between pt-1">
                            <li class="list-group-item text-center flex-shrink-0 flex-xl-shrink-1">
                                <div class="flex-content-center mb-3 width-40 height-40 border border-width-2 border-gray mx-auto rounded-circle">
                                    1
                                </div>
                                <div class="text-gray-1">Customer information</div>
                            </li>
                            <li class="list-group-item text-center flex-shrink-0 flex-xl-shrink-1">
                                <div class="flex-content-center mb-3 width-40 height-40 border  border-width-2 border-gray mx-auto rounded-circle">
                                    2
                                </div>
                                <div class="text-gray-1">Payment information</div>
                            </li>
                            <li class="list-group-item text-center flex-shrink-0 flex-md-shrink-1">
                                <div class="flex-content-center mb-3 width-40 height-40 bg-primary border-width-2 border border-primary text-white mx-auto rounded-circle">
                                    3
                                </div>
                                <div class="text-primary">Review Booking!</div>
                            </li>
                        </ul>
                    </div>
                    <div class="py-6 px-5 border-bottom">
                        <div class="flex-horizontal-center">
                            <div class="ml-3">
                                <h3 class="font-size-18 font-weight-bold text-dark mb-0 text-lh-sm">
                                    Review Booking.
                                </h3>
                                <p class="mb-0">Cek data yang anda masukkan sebelum mengonfirmasi booking</p>
                            </div>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5 border-bottom">
                        <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark mb-2">
                            Review Booking
                        </h5>
                        <!-- Fact List -->
                        <form action="{{ route('booking.step.three.post') }}" method="post" >
                            {{ csrf_field() }}
                            
                        <ul class="list-unstyled font-size-1 mb-0 font-size-16">
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Aktivitas</span>
                                <span class="text-secondary text-right">{{$booking->activities['name_activities']}}</span>
                            </li>

                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Jumlah Peserta</span>
                                <span class="text-secondary text-right">{{ $booking->number_participant}} Peserta</span>
                            </li>

                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Nama Pemesan</span>
                                <span class="text-secondary text-right">{{$booking->name_order}}</span>
                            </li>

                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Asal Kota</span>
                                <span class="text-secondary text-right">{{$booking->city}}</span>
                            </li>

                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Tanggal Booking</span>
                                <span class="text-secondary text-right">{{ date('d M Y', strtotime($booking->date_booking)) }}</span>
                            </li>
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Metode Pembayaran</span>
                                <span class="text-secondary text-right">{{$booking->method_payment}}</span>
                            </li>
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Total Pembayaran</span>
                                <span class="text-secondary text-right">Rp. {{ number_format($booking->total_payment, 0) }},-</span>
                            </li>
                            @if($booking->type_payment_bank != null)
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Tipe Pembayaran</span>
                                @if($booking->type_payment_bank == null)
                                <span class="text-secondary text-right">COD</span>
                                @else
                                <span class="text-secondary text-right">{{ $booking->type_payment_bank }}</span>
                                @endif
                            </li>
                            <li class="d-flex justify-content-between py-2">
                                <span class="font-weight-medium">Biaya Pembayaran</span>
                                @if($booking->type_payment_bank == "DP30%")
                                <?php
                                $DP = (30 / 100) * (int)$booking->total_payment;
                                ?>
                                <span class="text-secondary text-right">Rp. {{ number_format($DP, 0) }},-</span>
                                @else
                                <span class="text-secondary text-right">Rp. {{ number_format($booking->total_payment, 0) }},-</span>
                                @endif
                            </li>
                            @endif

                        </ul>
                        <div class="pt-4 border-bottom">
                            <div class="row">
                                <div class="col-sm-12 mb-4">
                                    {!! NoCaptcha::renderJs() !!}
                                    {!! NoCaptcha::display() !!}
                                </div>
                            <div class="col-sm-4 mb-4">
                                <a class="btn btn-default w-100 rounded-sm transition-3d-hover border font-size-16 font-weight-bold py-3" href="{{ route('booking.step.one') }}">BACK STEP 1</a>
                            </div>
                            <div class="col-sm-4 mb-4">
                                <a class="btn btn-default w-100 rounded-sm transition-3d-hover border font-size-16 font-weight-bold py-3" href="{{ route('booking.step.two') }}">BACK STEP 2</a>
                            </div>
                            <div class="col-sm-4 mb-4">
                                <button type="submit" class="btn btn-primary w-100 rounded-sm border transition-3d-hover font-size-16 font-weight-bold py-3">CONFIRM BOOKING</button>
                            </div>
                        </div>
                        </div>
                        </form>
                        <!-- End Fact List -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3">
                <div class="mb-4">
                
                    <div class="border border-color-7 shadow-soft bg-white rounded p-4 mb-5">
                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Bisa Booking Kapanpun</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Pembayaran Mudah</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Tersedia Tour Guide </a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Tersedia Basecamp</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Admin Online 24 Jam</a>
                            </h6>
                        </div>
                        <div class="d-flex align-items-center mb-0">
                            <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                            <h6 class="mb-0 font-size-14 text-gray-1">
                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                            </h6>
                        </div>
                    </>
                </div>
            </div>
            <div class="mb-4">
                <div class="border border-color-7 shadow-soft bg-white rounded p-4 mb-5">
                <div>
                    <small class="d-block font-size-18 font-weight-normal text-primary">Booking / Info: <span class="text-primary font-weight-semi-bold"><br>0812 1663 2836</span></small>
                    <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang
    " target="_blank"><img src="https://gosocial.co.id/assets/landing/image/wa.png" width="200px" height="47px" alt="Whatsapp"></a>
                </div>
            </div>
            </div>
        </div>
    </div>
</main>
    @endsection