@extends('layouts.landing')

@section('content')

<!-- ========== MAIN CONTENT ========== -->
<main id="content">
    <!-- Hero Section -->
    <div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url({{ asset ('img/1920x600/img33.jpg')}});">
        <div class="container space-top-xl-3 py-6 py-xl-0">
            <div class="row justify-content-center py-xl-4">
                <!-- Info -->
                <div class="py-xl-10 py-5">
                    <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">{{$title}}</h1>
                </div>
                <!-- End Info -->
            </div>
        </div>
    </div>
    <!-- End Hero Section -->
    <!-- Breadcrumb -->
    <div class="container">
        <nav class="py-3" aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-no-gutter mb-0 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{url ('/')}}">Home</a></li>
                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{url ('/activities')}}">Layanan</a></li>
                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">{{$label}}</li>
            </ol>
        </nav>
    </div>
    <!-- End Breadcrumb -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-9">
                <div class="d-block d-md-flex flex-center-between align-items-start mb-3">
                    <div class="mb-1">
                        <div class="mb-2 mb-md-0">
                            <h4 class="font-size-23 font-weight-bold mb-1 mr-3">Paket Wisata Gua Kucing Gili Ketapang Probolinggo (Sudah termasuk Snorkeling)</h4>
                        </div>
                        <div class="d-block d-md-flex flex-horizontal-center">
                            <div class="mr-4 mb-2 mb-md-0">
                                <span class="badge badge-pill badge-warning text-lh-sm text-white py-1 px-2 font-size-14 border-radius-3 font-weight-normal">5/5</span>
                                <span class="font-size-14 text-gray-1 ml-2">(1000++ Pengunjung)</span>
                            </div>
                            <div class="d-block d-md-flex flex-horizontal-center font-size-14 text-gray-1 mb-2 mb-md-0">
                                <i class="icon flaticon-placeholder mr-2 font-size-20"></i> Gili Ketapang Probolinggo
                                <a href="https://goo.gl/maps/h8BfQofQyRKsGpyA9" class="ml-1 d-block d-md-inline" target="_blank"> - Buka di Map</a>
                            </div>
                            <div class="p-4">
                                <div class="text-center">
                                    <a href="/booking" target=”_blank”  class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Pesan Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="py-4 border-top border-bottom mb-4">
                    <ul class="list-group list-group-borderless list-group-horizontal row">
                        <li class="col-md-4 flex-horizontal-center list-group-item text-lh-sm mb-2">
                            <i class="flaticon-non-commercial text-primary font-size-22 mr-2 d-block"></i>
                            <div class="ml-1 text-gray-1">Hanya Rp. 135.000/Orang (Sudah termasuk Snorkeling)</div>
                        </li>
                         <li class="col-md-4 flex-horizontal-center list-group-item text-lh-sm mb-2">
                            <i class="flaticon-user text-primary font-size-22 mr-2 d-block"></i>
                            <div class="ml-1 text-gray-1">Tour Guide Ramah</div>
                        </li>
                        <li class="col-md-4 flex-horizontal-center list-group-item text-lh-sm mb-2">
                            <i class="flaticon-alarm text-primary font-size-22 mr-2 d-block"></i>
                            <div class="ml-1 text-gray-1">Puas Wisata 5 Jam</div>
                        </li>
                        <li class="col-md-4 flex-horizontal-center list-group-item text-lh-sm mb-2">
                            <i class="flaticon-event text-primary font-size-22 mr-2 d-block"></i>
                            <div class="ml-1 text-gray-1">Siap Menerima Setiap Hari</div>
                        </li>
                        <li class="col-md-4 flex-horizontal-center list-group-item text-lh-sm mb-2">
                            <i class="flaticon-tickets text-primary font-size-23 mr-2 d-block"></i>
                            <div class="ml-1 text-gray-1">Bisa Bayar Di tempat</div>
                        </li>
                    </ul>
                </div>
                <div class="py-4">
                    <h5 class="font-size-21 font-weight-bold text-dark mb-4">
                        Pengalaman yang Anda Dapatkan
                    </h5>
                    <ul class="list-group list-group-borderless list-group-horizontal list-group-flush no-gutters row">
                        <li class="col-md-2 mb-5 list-group-item pt-0 border-bottom pb-3">
                            <div class="font-weight-bold text-dark">Highlight</div>
                        </li>
                        <li class="col-md-10 mb-5 list-group-item pt-0 border-bottom pb-3">
                            <div class="flex-horizontal-center mb-3 text-gray-1"><i class="fas fa-circle mr-3 font-size-8 text-primary"></i>Bisa foto dengan ikan Nemo</div>
                            <div class="flex-horizontal-center mb-3 text-gray-1"><i class="fas fa-circle mr-3 font-size-8 text-primary"></i>Gratis file foto hasil liburan Anda</div>
                            <div class="flex-horizontal-center mb-3 text-gray-1"><i class="fas fa-circle mr-3 font-size-8 text-primary"></i>Eksplore foto di spot terbaik kami</div>
                        </li>
                        <li class="col-md-2 mb-5 mb-md-0 list-group-item pt-0 border-bottom pb-3">
                            <div class="font-weight-bold text-dark">Biaya Sudah Termasuk</div>
                        </li>
                        <li class="col-md-5 mb-5 mb-md-0 list-group-item pt-0 border-bottom pb-3">
                            <div class="flex-horizontal-center mb-3 text-gray-1"><i class="flaticon-tick mr-3 font-size-16 text-primary"></i>Transport Ke Pulau</div>
                            <div class="flex-horizontal-center mb-3 text-gray-1"><i class="flaticon-tick mr-3 font-size-16 text-primary"></i>Pelampung</div>
                            <div class="flex-horizontal-center mb-3 text-gray-1"><i class="flaticon-tick mr-3 font-size-16 text-primary"></i>Foto Under Water</div>
                            <div class="flex-horizontal-center mb-3 text-gray-1"><i class="flaticon-tick mr-3 font-size-16 text-primary"></i>Makan Siang</div>
                            <div class="flex-horizontal-center mb-3 text-gray-1"><i class="flaticon-tick mr-3 font-size-16 text-primary"></i>Gazebo</div>
                            <div class="flex-horizontal-center mb-3 text-gray-1"><i class="flaticon-tick mr-3 font-size-16 text-primary"></i>Asuransi</div>
                            <div class="flex-horizontal-center mb-3 text-gray-1"><i class="flaticon-tick mr-3 font-size-16 text-primary"></i>Snorkeling</div>
                        </li>
                    </ul>
                </div>
                <div class="border-bottom position-relative">
                    <h5 class="font-size-21 font-weight-bold text-dark mb-3">
                        Deskripsi
                    </h5>
                    <p>Dengan harga Rp135.000/orang Anda dapat menikmati keindahan Gili Ketapang dengan Drone. Keberadaan drone tentu sangat membantu traveler menghasilkan foto-foto perjalanan wisata yang menawan di Gili Ketapang. Melalui mata seekor burung, setiap bentangan alam tentu menjadi tampak begitu indah. Namun demikian, di Gili Ketapang Anda dapat menikmati indahnya pemandangan Pulai dan Pantai di Gili Ketapang dari atas menggunakan drone.</p>
                </div>
                <div class="border-bottom py-4">
                    <h5 class="font-size-21 font-weight-bold text-dark mb-4">
                        Anda juga mungkin bertanya
                    </h5>
                    <div id="basicsAccordion">
                        <!-- Card -->
                        <div class="card border-0 mb-4 pb-1">
                            <div class="card-header border-bottom-0 p-0" id="basicsHeadingOne">
                                <h5 class="mb-0">
                                    <button type="button" class="collapse-link btn btn-link btn-block d-flex align-items-md-center p-0"
                                    data-toggle="collapse" data-target="#basicsCollapseOne" aria-expanded="true" aria-controls="basicsCollapseOne">
                                        <div class="border border-color-8 rounded-xs border-width-2 p-2 mb-3 mb-md-0 mr-4">
                                            <figure id="rectangle" class="minus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/rectangle.svg" alt="SVG" data-parent="#rectangle">
                                            </figure>
                                            <figure id="plus1" class="plus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/plus.svg" alt="SVG" data-parent="#plus1">
                                            </figure>
                                        </div>

                                        <h6 class="font-weight-bold text-gray-3 mb-0">Bagaimana cara booking trip?</h6>
                                    </button>
                                </h5>
                            </div>
                            <div id="basicsCollapseOne" class="collapse show"
                                 aria-labelledby="basicsHeadingOne"
                                 data-parent="#basicsAccordion">
                                <div class="card-body pl-10 pl-md-10 pr-md-12 pt-0">
                                    <p class="mb-0 text-gray-1 text-lh-lg">Mudah, cukup hubungi Kami melalui WA, bisa juga dengan tekan Tombol <b>Pesan Sekarang</b> atau <b>Booking Sekarang</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End Card -->

                        <!-- Card -->
                        <div class="card border-0 mb-4 pb-1">
                            <div class="card-header border-bottom-0 p-0" id="basicsHeadingTwo">
                                <h5 class="mb-0">
                                    <button type="button" class="collapse-link btn btn-link btn-block d-flex align-items-md-center p-0"
                                    data-toggle="collapse" data-target="#basicsCollapseTwo" aria-expanded="false" aria-controls="basicsCollapseTwo">
                                        <div class="border border-color-8 rounded-xs border-width-2 p-2 mb-3 mb-md-0 mr-4">
                                            <figure id="rectangle1" class="minus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/rectangle.svg" alt="SVG" data-parent="#rectangle1">
                                            </figure>
                                            <figure id="plus2" class="plus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/plus.svg" alt="SVG" data-parent="#plus2">
                                            </figure>
                                        </div>
                                        <h6 class="font-weight-bold text-gray-3 mb-0">Berapa minimal jumlah orang/ rombongan ke sana?</h6>
                                    </button>
                                </h5>
                            </div>
                            <div id="basicsCollapseTwo" class="collapse"
                                 aria-labelledby="basicsHeadingTwo"
                                 data-parent="#basicsAccordion">
                                <div class="card-body pl-10 pl-md-10 pr-md-12 pt-0">
                                    <p class="mb-0 text-gray-1 text-lh-lg">Tidak ada jumlah minimal rombongan untuk memesan layanan Wisata kami. Jika Anda sendiri atau berdua, Anda juga bisa memesan layanan wisata kami.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End Card -->

                        <!-- Card -->
                        <div class="card border-0 mb-4 pb-1">
                            <div class="card-header border-bottom-0 p-0" id="basicsHeadingThree">
                                <h5 class="mb-0">
                                    <button type="button" class="collapse-link btn btn-link btn-block d-flex align-items-md-center p-0"
                                    data-toggle="collapse" data-target="#basicsCollapseThree" aria-expanded="false" aria-controls="basicsCollapseThree">
                                        <div class="border border-color-8 rounded-xs border-width-2 p-2 mb-3 mb-md-0 mr-4">
                                            <figure id="rectangle2" class="minus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/rectangle.svg" alt="SVG" data-parent="#rectangle2">
                                            </figure>
                                            <figure id="plus3" class="plus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/plus.svg" alt="SVG" data-parent="#plus3">
                                            </figure>
                                        </div>
                                        <h6 class="font-weight-bold text-gray-3 mb-0">Apakah ada harga khusus untuk rombongan?</h6>
                                    </button>
                                </h5>
                            </div>
                            <div id="basicsCollapseThree" class="collapse"
                                 aria-labelledby="basicsHeadingThree"
                                 data-parent="#basicsAccordion">
                                <div class="card-body pl-10 pl-md-10 pr-md-12 pt-0">
                                    <p class="mb-0 text-gray-1 text-lh-lg">Jangan khawatir, jika Anda datang bersama rombongan, akan mendapatkan potongan harga dari kami. 
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End Card -->

                        <!-- Card -->
                        <div class="card border-0 mb-4 pb-1">
                            <div class="card-header border-bottom-0 p-0" id="basicsHeadingFour">
                                <h5 class="mb-0">
                                    <button type="button" class="collapse-link btn btn-link btn-block d-flex align-items-md-center p-0"
                                    data-toggle="collapse" data-target="#basicsCollapseFour" aria-expanded="false" aria-controls="basicsCollapseFour">
                                        <div class="border border-color-8 rounded-xs border-width-2 p-2 mb-3 mb-md-0 mr-4">
                                            <figure id="plus4" class="plus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/plus.svg" alt="SVG" data-parent="#plus4">
                                            </figure>
                                            <figure id="rectangle3" class="minus svg-preloader">
                                                <img class="js-svg-injector mb-0" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/rectangle.svg" alt="SVG" data-parent="#rectangle3">
                                            </figure>
                                        </div>
                                        <h6 class="font-weight-bold text-gray-3 mb-0">Bagaimana jika saya sampai di sana malam hari?</h6>
                                    </button>
                                </h5>
                            </div>
                            <div id="basicsCollapseFour" class="collapse"
                                 aria-labelledby="basicsHeadingFour"
                                 data-parent="#basicsAccordion">
                                <div class="card-body pl-10 pl-md-10 pr-md-12 pt-0">
                                    <p class="mb-0 text-gray-1 text-lh-lg">Kami menyediakan tempat untuk bermalam di Probolinggo jika Anda datang saat malam hari. Biayanya <b>Gratis</b>, dan paginya akan langsung kami antar menuju Pulau Gili Ketapang.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3">
                                <div class="mb-4">
                                    <div class="border border-color-7 rounded mb-5">
                                        <div class="border-bottom">
                                            <div class="p-4">
                                                <span class="font-size-14">Harga mulai dari:</span><br>
                                                <span class="font-size-24 text-gray-6 font-weight-bold ml-1">Rp. 135.000</span>
                                            </div>
                                        </div>
                                        <div class="p-4">
                                            <div class="text-center">
                                                <a href="/booking" target=”_blank” class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Booking Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="border border-color-7 rounded p-4 mb-5">
                                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Bisa Booking Kapanpun</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Pembayaran Mudah</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Tour Guide </a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Basecamp</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Admin Online 24 Jam</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
        </div>
        <!-- Product Cards carousel -->
        <div class="product-card-carousel-block product-card-carousel-v5">
            <div class="space-1">
                <div class="w-md-80 w-lg-50 text-center mx-md-auto mt-3">
                    <h2 class="section-title text-black font-size-30 font-weight-bold mb-0">Yuk lihat paket wisata lainnya</h2>
                </div>
                <div class="js-slick-carousel u-slick u-slick--equal-height u-slick--gutters-3"
                    data-slides-show="4"
                    data-slides-scroll="1"
                    data-arrows-classes="d-none d-xl-inline-block u-slick__arrow-classic v1 u-slick__arrow-classic--v1 u-slick__arrow-centered--y rounded-circle"
                    data-arrow-left-classes="fas fa-chevron-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left shadow-5"
                    data-arrow-right-classes="fas fa-chevron-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right shadow-5"
                    data-pagi-classes="text-center d-xl-none u-slick__pagination mt-4"
                    data-responsive='[{
                    "breakpoint": 1025,
                    "settings": {
                    "slidesToShow": 3
                    }
                    }, {
                    "breakpoint": 992,
                    "settings": {
                    "slidesToShow": 2
                    }
                    }, {
                    "breakpoint": 768,
                    "settings": {
                    "slidesToShow": 1
                    }
                    }, {
                    "breakpoint": 554,
                    "settings": {
                    "slidesToShow": 1
                    }
                    }]'>
                    <div class="js-slide mt-5">
                        <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/snorkeling')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="{{asset ('img/300x230/imgsatu.jpg')}}" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Tervavorit</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.90.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/snorkeling')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Snorkeling</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="js-slide mt-5">
                        <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/snorkeling')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="{{asset('img/300x230/imgdua.jpg')}}" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Tereksklusif</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.100.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/snorkeling')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Snorkeling VIP</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="js-slide mt-5">
                        <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/pemancingan')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="{{asset ('img/300x230/mancing1.png')}}" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Terfavorit</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.800.000<small class="mr-2">&nbsp;(Maksimal 10 Orang)</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/pemancingan')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Pemancingan Gili Ketapang - <b>Rute Dekat</b></a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 1 Hari
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="js-slide mt-5">
                        <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/pemancingan')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="{{asset ('img/300x230/mancing2.png')}}" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Tereksklusif</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.1.000.000<small class="mr-2">&nbsp;(Maksimal 10 Orang)</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/pemancingan')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Pemancingan Gili Ketapang - <b>Rute Jauh</b></a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 1 Hari
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="js-slide mt-5">
                        <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/snorkeling')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="{{asset('img/300x230/imgfour.jpg')}}" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Rame-Rame</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.165.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a  class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/snorkeling')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Rombongan + Kaos Exclusive</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="js-slide mt-5">
                        <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/banana-boat')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="{{asset('img/300x230/imglima.jpg')}}" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Tervavorit</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.150.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/banana-boat')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Banana Boat (Plus Snorkeling)</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="js-slide mt-5">
                        <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/drone')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="{{asset('img/300x230/imgenam.jpg')}}" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Tervavorit</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.150.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/drone')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Drone (Plus Snorkeling)</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="js-slide mt-5">
                        <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/gua-kucing')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="{{ asset ('img/300x230/imgtiga.jpg')}}" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Tervavorit</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.135.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/gua-kucing')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Snorkeling + Goa Kucing</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Product Cards carousel -->
    </div>
</main>
<!-- ========== END MAIN CONTENT ========== -->

@endsection