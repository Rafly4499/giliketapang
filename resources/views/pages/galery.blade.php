@extends('layouts.landing')

@section('content')
<!-- Hero Section -->
<div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url(img/1920x400/img112.jpg);">
    <div class="container space-top-xl-3 py-6 py-xl-0">
        <div class="row justify-content-center py-xl-4">
            <!-- Info -->
            <div class="py-xl-10 py-5">
                <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">Yuk lihat keseruan di Gili Ketapang</h1>
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb breadcrumb-no-gutter justify-content-center mb-0">
                  <li class="breadcrumb-item font-size-14"> <a class="text-white" href="{{url ('/')}}">Home</a> </li>
                    <li class="breadcrumb-item custom-breadcrumb-item font-size-14 text-white active" aria-current="page">{{$label}}</li>
                  </ol>
                </nav>
            </div>
            <!-- End Info -->
        </div>
    </div>
</div>
<!-- End Hero Section -->

<!-- Tabs v1 -->
<div class="tabs-block tab-v1">
    <div class="container space-lg-1">
        <div class="w-md-80 w-lg-50 text-center mx-md-auto my-3">
            <h2 class="section-title text-black font-size-30 font-weight-bold mb-0">Butuh liburan? <br>Yuk eksplore Gili Ketapang</h2>
        </div>
        <!-- Nav Classic -->
        <ul class="nav tab-nav-pill flex-nowrap pb-4 pb-lg-5 tab-nav justify-content-lg-center" role="tablist">
            <li class="nav-item">
                <a class="nav-link font-weight-medium active" id="pills-one-example-t1-tab" data-toggle="pill" href="#pills-one-example-t1" role="tab" aria-controls="pills-one-example-t1" aria-selected="true">
                    <div class="d-flex flex-column flex-md-row  position-relative text-dark align-items-center">
                        <span class="tabtext font-weight-semi-bold">Snorkeling</span>
                    </div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-weight-medium" id="pills-two-example-t1-tab" data-toggle="pill" href="#pills-two-example-t1" role="tab" aria-controls="pills-two-example-t1" aria-selected="true">
                    <div class="d-flex flex-column flex-md-row  position-relative text-dark align-items-center">
                        <span class="tabtext font-weight-semi-bold">Banana Boat</span>
                    </div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-weight-medium" id="pills-three-example-t1-tab" data-toggle="pill" href="#pills-three-example-t1" role="tab" aria-controls="pills-three-example-t1" aria-selected="true">
                    <div class="d-flex flex-column flex-md-row  position-relative text-dark align-items-center">
                        <span class="tabtext font-weight-semi-bold">Drone Gili</span>
                    </div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-weight-medium" id="pills-four-example-t1-tab" data-toggle="pill" href="#pills-four-example-t1" role="tab" aria-controls="pills-four-example-t1" aria-selected="true">
                    <div class="d-flex flex-column flex-md-row  position-relative text-dark align-items-center">
                        <span class="tabtext font-weight-semi-bold">Gua Kucing</span>
                    </div>
                </a>
            </li>
        </ul>
        <!-- End Nav Classic -->
        <div class="tab-content">
            <div class="tab-pane fade active show" id="pills-one-example-t1" role="tabpanel" aria-labelledby="pills-one-example-t1-tab">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-xl-9">
                                <div class="d-block d-md-flex flex-center-between align-items-start mb-3">
                                    <div class="border-bottom position-relative">
                                        <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark">
                                            Snorkeling Gili Ketapang
                                        </h5>
                                        <p>Gili Ketapang Probolinggo adalah sebuah pulau kecil yang terletak di bagian utara Probolinggo dengan jarak sekitar 7 kilometer. Pulau ini bisa di lihat dengan mata telanjang dari pelabuhan kota Probolinggo karena jaraknya tidak terlalu jauh. Pulau ini tepatnya masuk pada Desa Ketapang, Kecamatan Sumberasih Kabupaten Probolinggo dengan luas sekitar 70 hektar persegi. </p>
            
                                        <div class="collapse" id="collapseLinkExample">
                                            <p>Aktivitas penduduk lokal adalah nelayan yang melaut di perairan sekitar pulau dan bisa di bilang sudah makmur kehidupan mereka. Pada awalnya pulai ini merupakan spot mancing bagi masyarakat penghobi dunia pancing di area Jawa timur. Karena kondisi lautnya masih bagus serta nelayan pun saat melaut masih menggunakan cara tradisional. Perkembangan kearah dunia pariwisata ini berkembang setelah masyarakat sekitar tahu bahwa potensi alam di pantainya bagus tidak kalah dengan spot snorkeling dengan wilayah indonesia lainnya. Untuk itulah dinas pariwisata probolinggo pun menggenjot potensi Paket Wisata Pulau Gili Ketapang dengan menggelar pameran di berbagai ajang promosi di berbagai wilayah Indonesia.</p>
                                        </div>
            
                                        <a class="link-collapse link-collapse-custom gradient-overlay-half mb-5 d-inline-block border-bottom border-primary" data-toggle="collapse" href="#collapseLinkExample" role="button" aria-expanded="false" aria-controls="collapseLinkExample">
                                            <span class="link-collapse__default font-size-14">Tampilkan lebih banyak <i class="flaticon-down-chevron font-size-10 ml-1"></i></span>
                                            <span class="link-collapse__active font-size-14">Tampilkan lebih sedikit <i class="flaticon-arrow font-size-10 ml-1"></i></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="py-4 border-top mb-4">
                                    <div class="position-relative">
                                        <!-- Images Carousel -->
                                        <div id="sliderSyncingNav" class="js-slick-carousel u-slick mb-2"
                                            data-infinite="true"
                                            data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
                                            data-arrow-left-classes="flaticon-back u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
                                            data-arrow-right-classes="flaticon-next u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
                                            data-nav-for="#sliderSyncingThumb">
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img67.png" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img55.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img565.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img554.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img333.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img1124.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <div class="position-absolute right-0 mr-3 mt-md-n11 mt-n9">
                                            <div class="flex-horizontal-center">
        
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-fancybox="fancyboxGallery6"
                                                data-src="img/960x490/img2.jpg"
                                                data-caption="MyTravel in frames - image #02"
                                                data-speed="700">
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-caption="MyTravel in frames - image #03"
                                                data-src="img/960x490/img3.jpg"
                                                data-fancybox="fancyboxGallery6"
                                                data-speed="700">
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-fancybox="fancyboxGallery6"
                                                data-src="img/960x490/img4.jpg"
                                                data-caption="MyTravel in frames - image #04"
                                                data-speed="700">
        
                                            </div>
                                        </div>
        
                                        <div id="sliderSyncingThumb" class="js-slick-carousel u-slick u-slick--gutters-4 u-slick--transform-off"
                                            data-infinite="true"
                                            data-slides-show="6"
                                            data-is-thumbs="true"
                                            data-nav-for="#sliderSyncingNav"
                                            data-responsive='[{
                                                "breakpoint": 992,
                                                "settings": {
                                                    "slidesToShow": 4
                                                }
                                            }, {
                                                "breakpoint": 768,
                                                "settings": {
                                                    "slidesToShow": 3
                                                }
                                            }, {
                                                "breakpoint": 554,
                                                "settings": {
                                                    "slidesToShow": 2
                                                }
                                            }]'>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img67.png" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img55.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img565.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img554.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img333.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img1124.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <!-- End Images Carousel -->
                                    </div>
                                    <div class="border-bottom py-4">
                                        <h5 id="scroll-video" class="font-size-21 font-weight-bold text-dark mb-4">
                                            Lihat lebih lengkap dengan Video
                                        </h5>
                                        <!-- Video Block -->
                                        <div id="youTubeVideoPlayerExample1" class="u-video-player rounded-sm">
                                            <!-- Cover Image -->
                                            <img class="img-fluid u-video-player__preview rounded-sm" src="img/960x450/img1.jpg" alt="Image">
                                            <!-- End Cover Image -->
                
                                            <!-- Play Button -->
                                            <a class="js-inline-video-player u-video-player__btn u-video-player__centered" href="javascript:;"
                                                data-video-id="rdQfeH8Ikso"
                                                data-parent="youTubeVideoPlayerExample1"
                                                data-is-autoplay="true"
                                                data-target="youTubeVideoIframeExample1"
                                                data-classes="u-video-player__played">
                                                <span class="u-video-player__icon u-video-player__icon--lg text-primary bg-transparent">
                                                    <span class="flaticon-multimedia text-white ml-0 font-size-60 u-video-player__icon-inner"></span>
                                                </span>
                                            </a>
                                            <!-- End Play Button -->
                
                                            <!-- Video Iframe -->
                                            <div class="embed-responsive embed-responsive-16by9 rounded-sm">
                                                <div id="youTubeVideoIframeExample1"></div>
                                            </div>
                                            <!-- End Video Iframe -->
                                        </div>
                                        <!-- End Video Block -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-3">
                                <div class="mb-4">
                                    <div class="border border-color-7 rounded mb-5">
                                        <div class="border-bottom">
                                            <div class="p-4">
                                                <span class="font-size-14">Harga mulai dari:</span><br>
                                                <span class="font-size-24 text-gray-6 font-weight-bold ml-1">Rp. 90.000</span>
                                            </div>
                                        </div>
                                        <div class="p-4">
                                            <div class="text-center">
                                                <a href="rental-booking.html" class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Booking Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="border border-color-7 rounded p-4 mb-5">
                                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Bisa Booking Kapanpun</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Pembayaran Mudah</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Tour Guide </a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Basecamp</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Admin Online 24 Jam</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-two-example-t1" role="tabpanel" aria-labelledby="pills-two-example-t1-tab">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-xl-9">
                                <div class="d-block d-md-flex flex-center-between align-items-start mb-3">
                                    <div class="border-bottom position-relative">
                                        <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark">
                                            Banana Boat Gili Ketapang
                                        </h5>
                                        <p>Banana boat adalah sebuah perahu berbentuk pisang yang berukuran besar. Perahu terbuat dari karet tebal dan ringan sehingga mudah ditarik oleh speed boat. Kemudian perahu karet ini akan ditarik oleh speed boat ke tengah laut, dan berputar-putar mengelilingi pulau. Apalagi saat menerjang ombak di Gili Ketapang, membuat permainan ini sangat seru.</p>
            
                                        <div class="collapse" id="collapseLinkExample">
                                            <p>Speed boat dikemudikan oleh satu sampai dua orang pemandu. Panjang tali pengikat banana boat dengan speed boat kurang lebih lima meter. Hal ini untuk memudahkan komunikasi antara pemandu speed boat dengan peserta banana boat.</p>
                                        </div>
            
                                        <a class="link-collapse link-collapse-custom gradient-overlay-half mb-5 d-inline-block border-bottom border-primary" data-toggle="collapse" href="#collapseLinkExample" role="button" aria-expanded="false" aria-controls="collapseLinkExample">
                                            <span class="link-collapse__default font-size-14">Tampilkan lebih banyak <i class="flaticon-down-chevron font-size-10 ml-1"></i></span>
                                            <span class="link-collapse__active font-size-14">Tampilkan lebih sedikit <i class="flaticon-arrow font-size-10 ml-1"></i></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="py-4 border-top mb-4">
                                    <div class="position-relative">
                                        <!-- Images Carousel -->
                                        <div id="sliderSyncingNav" class="js-slick-carousel u-slick mb-2"
                                            data-infinite="true"
                                            data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
                                            data-arrow-left-classes="flaticon-back u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
                                            data-arrow-right-classes="flaticon-next u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
                                            data-nav-for="#sliderSyncingThumb">
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img67.png" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img55.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img565.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img554.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img333.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img1124.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <div class="position-absolute right-0 mr-3 mt-md-n11 mt-n9">
                                            <div class="flex-horizontal-center">
        
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-fancybox="fancyboxGallery6"
                                                data-src="img/960x490/img2.jpg"
                                                data-caption="MyTravel in frames - image #02"
                                                data-speed="700">
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-caption="MyTravel in frames - image #03"
                                                data-src="img/960x490/img3.jpg"
                                                data-fancybox="fancyboxGallery6"
                                                data-speed="700">
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-fancybox="fancyboxGallery6"
                                                data-src="img/960x490/img4.jpg"
                                                data-caption="MyTravel in frames - image #04"
                                                data-speed="700">
        
                                            </div>
                                        </div>
        
                                        <div id="sliderSyncingThumb" class="js-slick-carousel u-slick u-slick--gutters-4 u-slick--transform-off"
                                            data-infinite="true"
                                            data-slides-show="6"
                                            data-is-thumbs="true"
                                            data-nav-for="#sliderSyncingNav"
                                            data-responsive='[{
                                                "breakpoint": 992,
                                                "settings": {
                                                    "slidesToShow": 4
                                                }
                                            }, {
                                                "breakpoint": 768,
                                                "settings": {
                                                    "slidesToShow": 3
                                                }
                                            }, {
                                                "breakpoint": 554,
                                                "settings": {
                                                    "slidesToShow": 2
                                                }
                                            }]'>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img67.png" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img55.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img565.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img554.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img333.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img1124.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <!-- End Images Carousel -->
                                    </div>
                                    <div class="border-bottom py-4">
                                        <h5 id="scroll-video" class="font-size-21 font-weight-bold text-dark mb-4">
                                            Lihat lebih lengkap dengan Video
                                        </h5>
                                        <!-- Video Block -->
                                        <div id="youTubeVideoPlayerExample1" class="u-video-player rounded-sm">
                                            <!-- Cover Image -->
                                            <img class="img-fluid u-video-player__preview rounded-sm" src="img/960x450/img1.jpg" alt="Image">
                                            <!-- End Cover Image -->
                
                                            <!-- Play Button -->
                                            <a class="js-inline-video-player u-video-player__btn u-video-player__centered" href="javascript:;"
                                                data-video-id="rdQfeH8Ikso"
                                                data-parent="youTubeVideoPlayerExample1"
                                                data-is-autoplay="true"
                                                data-target="youTubeVideoIframeExample1"
                                                data-classes="u-video-player__played">
                                                <span class="u-video-player__icon u-video-player__icon--lg text-primary bg-transparent">
                                                    <span class="flaticon-multimedia text-white ml-0 font-size-60 u-video-player__icon-inner"></span>
                                                </span>
                                            </a>
                                            <!-- End Play Button -->
                
                                            <!-- Video Iframe -->
                                            <div class="embed-responsive embed-responsive-16by9 rounded-sm">
                                                <div id="youTubeVideoIframeExample1"></div>
                                            </div>
                                            <!-- End Video Iframe -->
                                        </div>
                                        <!-- End Video Block -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-3">
                                <div class="mb-4">
                                    <div class="border border-color-7 rounded mb-5">
                                        <div class="border-bottom">
                                            <div class="p-4">
                                                <span class="font-size-14">Harga mulai dari:</span><br>
                                                <span class="font-size-24 text-gray-6 font-weight-bold ml-1">Rp. 150.000</span>
                                            </div>
                                        </div>
                                        <div class="p-4">
                                            <div class="text-center">
                                                <a href="rental-booking.html" class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Booking Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border border-color-7 rounded p-4 mb-5">
                                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Bisa Booking Kapanpun</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Pembayaran Mudah</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Tour Guide </a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Basecamp</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Admin Online 24 Jam</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-three-example-t1" role="tabpanel" aria-labelledby="pills-three-example-t1-tab">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-xl-9">
                                <div class="d-block d-md-flex flex-center-between align-items-start mb-3">
                                    <div class="border-bottom position-relative">
                                        <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark">
                                            Drone Gili Ketapang
                                        </h5>
                                        <p>Munculnya teknologi drone beberapa tahun belakangan menciptakan pengalaman wisata yang berbeda. Aktivitas traveling kini bukan semata menikmati keindahan alam, lebih dari itu aktivitas ini perlu menghasilkan dokumentasi yang bagus dan berkesan, sehingga punya nilai jual tersendiri. Dokumentasi travel salah satunya dihasilkan dengan foto drone.</p>
            
                                        <div class="collapse" id="collapseLinkExample">
                                            <p>Keberadaan drone tentu sangat membantu traveler menghasilkan foto-foto perjalanan wisata yang menawan di Gili Ketapang. Melalui mata seekor burung, setiap bentangan alam tentu menjadi tampak begitu indah. Namun demikian, di Gili Ketapang Anda dapat menikmati indahnya pemandangan Pulai dan Pantai di Gili Ketapang dari atas menggunakan drone.</p>
                                        </div>
            
                                        <a class="link-collapse link-collapse-custom gradient-overlay-half mb-5 d-inline-block border-bottom border-primary" data-toggle="collapse" href="#collapseLinkExample" role="button" aria-expanded="false" aria-controls="collapseLinkExample">
                                            <span class="link-collapse__default font-size-14">Tampilkan lebih banyak <i class="flaticon-down-chevron font-size-10 ml-1"></i></span>
                                            <span class="link-collapse__active font-size-14">Tampilkan lebih sedikit <i class="flaticon-arrow font-size-10 ml-1"></i></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="py-4 border-top mb-4">
                                    <div class="position-relative">
                                        <!-- Images Carousel -->
                                        <div id="sliderSyncingNav" class="js-slick-carousel u-slick mb-2"
                                            data-infinite="true"
                                            data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
                                            data-arrow-left-classes="flaticon-back u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
                                            data-arrow-right-classes="flaticon-next u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
                                            data-nav-for="#sliderSyncingThumb">
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img67.png" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img55.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img565.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img554.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img333.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img1124.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <div class="position-absolute right-0 mr-3 mt-md-n11 mt-n9">
                                            <div class="flex-horizontal-center">
        
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-fancybox="fancyboxGallery6"
                                                data-src="img/960x490/img2.jpg"
                                                data-caption="MyTravel in frames - image #02"
                                                data-speed="700">
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-caption="MyTravel in frames - image #03"
                                                data-src="img/960x490/img3.jpg"
                                                data-fancybox="fancyboxGallery6"
                                                data-speed="700">
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-fancybox="fancyboxGallery6"
                                                data-src="img/960x490/img4.jpg"
                                                data-caption="MyTravel in frames - image #04"
                                                data-speed="700">
        
                                            </div>
                                        </div>
        
                                        <div id="sliderSyncingThumb" class="js-slick-carousel u-slick u-slick--gutters-4 u-slick--transform-off"
                                            data-infinite="true"
                                            data-slides-show="6"
                                            data-is-thumbs="true"
                                            data-nav-for="#sliderSyncingNav"
                                            data-responsive='[{
                                                "breakpoint": 992,
                                                "settings": {
                                                    "slidesToShow": 4
                                                }
                                            }, {
                                                "breakpoint": 768,
                                                "settings": {
                                                    "slidesToShow": 3
                                                }
                                            }, {
                                                "breakpoint": 554,
                                                "settings": {
                                                    "slidesToShow": 2
                                                }
                                            }]'>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img67.png" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img55.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img565.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img554.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img333.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img1124.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <!-- End Images Carousel -->
                                    </div>
                                    <div class="border-bottom py-4">
                                        <h5 id="scroll-video" class="font-size-21 font-weight-bold text-dark mb-4">
                                            Lihat lebih lengkap dengan Video
                                        </h5>
                                        <!-- Video Block -->
                                        <div id="youTubeVideoPlayerExample1" class="u-video-player rounded-sm">
                                            <!-- Cover Image -->
                                            <img class="img-fluid u-video-player__preview rounded-sm" src="img/960x450/img1.jpg" alt="Image">
                                            <!-- End Cover Image -->
                
                                            <!-- Play Button -->
                                            <a class="js-inline-video-player u-video-player__btn u-video-player__centered" href="javascript:;"
                                                data-video-id="rdQfeH8Ikso"
                                                data-parent="youTubeVideoPlayerExample1"
                                                data-is-autoplay="true"
                                                data-target="youTubeVideoIframeExample1"
                                                data-classes="u-video-player__played">
                                                <span class="u-video-player__icon u-video-player__icon--lg text-primary bg-transparent">
                                                    <span class="flaticon-multimedia text-white ml-0 font-size-60 u-video-player__icon-inner"></span>
                                                </span>
                                            </a>
                                            <!-- End Play Button -->
                
                                            <!-- Video Iframe -->
                                            <div class="embed-responsive embed-responsive-16by9 rounded-sm">
                                                <div id="youTubeVideoIframeExample1"></div>
                                            </div>
                                            <!-- End Video Iframe -->
                                        </div>
                                        <!-- End Video Block -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-3">
                                <div class="mb-4">
                                    <div class="border border-color-7 rounded mb-5">
                                        <div class="border-bottom">
                                            <div class="p-4">
                                                <span class="font-size-14">Harga mulai dari:</span><br>
                                                <span class="font-size-24 text-gray-6 font-weight-bold ml-1">Rp. 150.000</span>
                                            </div>
                                        </div>
                                        <div class="p-4">
                                            <div class="text-center">
                                                <a href="rental-booking.html" class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Booking Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="border border-color-7 rounded p-4 mb-5">
                                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Bisa Booking Kapanpun</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Pembayaran Mudah</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Tour Guide </a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Basecamp</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Admin Online 24 Jam</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-four-example-t1" role="tabpanel" aria-labelledby="pills-four-example-t1-tab">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-xl-9">
                                <div class="d-block d-md-flex flex-center-between align-items-start mb-3">
                                    <div class="border-bottom position-relative">
                                        <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark">
                                            Gua Kucing Gili Ketapang
                                        </h5>
                                        <p>Pesona Gili Ketapang tidak berhenti pada terumbu karang dan pantainya saja. Nyatanya ada sekali lagi satu tempat yg unik, Goa Kucing namanya. Goa Kucing Gili Ketapang seperti namanya tempat ini memanglah banyak di huni oleh kucing liar di sekitaran goa itu, goa ini berada di tepi pantai timur gili ketapang</p>
            
                                        <div class="collapse" id="collapseLinkExample">
                                            <p>Karna berada di pantai timur yang adalah beberapa besar pantainya banyak menyebar batu-batu karang besar dan di dorong keindahan deretan pohon di ruang goa kucing ini yang memanglah bagus untuk latar belakang photo, jadi cocok buat selfie atau mengabadikan event dg berfoto2 di sini. Oleh karena itu begitu kurang komplit rasa-rasanya untuk beberapa travellers jg photografer jika gak nyoba yg satu ini.</p>
                                        </div>
            
                                        <a class="link-collapse link-collapse-custom gradient-overlay-half mb-5 d-inline-block border-bottom border-primary" data-toggle="collapse" href="#collapseLinkExample" role="button" aria-expanded="false" aria-controls="collapseLinkExample">
                                            <span class="link-collapse__default font-size-14">Tampilkan lebih banyak <i class="flaticon-down-chevron font-size-10 ml-1"></i></span>
                                            <span class="link-collapse__active font-size-14">Tampilkan lebih sedikit <i class="flaticon-arrow font-size-10 ml-1"></i></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="py-4 border-top mb-4">
                                    <div class="position-relative">
                                        <!-- Images Carousel -->
                                        <div id="sliderSyncingNav" class="js-slick-carousel u-slick mb-2"
                                            data-infinite="true"
                                            data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
                                            data-arrow-left-classes="flaticon-back u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
                                            data-arrow-right-classes="flaticon-next u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
                                            data-nav-for="#sliderSyncingThumb">
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img67.png" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img55.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img565.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img554.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img333.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide">
                                                <img class="img-fluid border-radius-3" src="img/960x490/img1124.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <div class="position-absolute right-0 mr-3 mt-md-n11 mt-n9">
                                            <div class="flex-horizontal-center">
        
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-fancybox="fancyboxGallery6"
                                                data-src="img/960x490/img2.jpg"
                                                data-caption="MyTravel in frames - image #02"
                                                data-speed="700">
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-caption="MyTravel in frames - image #03"
                                                data-src="img/960x490/img3.jpg"
                                                data-fancybox="fancyboxGallery6"
                                                data-speed="700">
                                                <img class="js-fancybox d-none" alt="Image Description"
                                                data-fancybox="fancyboxGallery6"
                                                data-src="img/960x490/img4.jpg"
                                                data-caption="MyTravel in frames - image #04"
                                                data-speed="700">
        
                                            </div>
                                        </div>
        
                                        <div id="sliderSyncingThumb" class="js-slick-carousel u-slick u-slick--gutters-4 u-slick--transform-off"
                                            data-infinite="true"
                                            data-slides-show="6"
                                            data-is-thumbs="true"
                                            data-nav-for="#sliderSyncingNav"
                                            data-responsive='[{
                                                "breakpoint": 992,
                                                "settings": {
                                                    "slidesToShow": 4
                                                }
                                            }, {
                                                "breakpoint": 768,
                                                "settings": {
                                                    "slidesToShow": 3
                                                }
                                            }, {
                                                "breakpoint": 554,
                                                "settings": {
                                                    "slidesToShow": 2
                                                }
                                            }]'>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img1.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img2.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img3.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img4.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img5.jpg" alt="Image Description">
                                            </div>
                                            <div class="js-slide" style="cursor: pointer;">
                                                <img class="img-fluid border-radius-3 height-110" src="img/960x490/img6.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <!-- End Images Carousel -->
                                    </div>
                                    <div class="border-bottom py-4">
                                        <h5 id="scroll-video" class="font-size-21 font-weight-bold text-dark mb-4">
                                            Lihat lebih lengkap dengan Video
                                        </h5>
                                        <!-- Video Block -->
                                        <div id="youTubeVideoPlayerExample1" class="u-video-player rounded-sm">
                                            <!-- Cover Image -->
                                            <img class="img-fluid u-video-player__preview rounded-sm" src="img/960x450/img1.jpg" alt="Image">
                                            <!-- End Cover Image -->
                
                                            <!-- Play Button -->
                                            <a class="js-inline-video-player u-video-player__btn u-video-player__centered" href="javascript:;"
                                                data-video-id="rdQfeH8Ikso"
                                                data-parent="youTubeVideoPlayerExample1"
                                                data-is-autoplay="true"
                                                data-target="youTubeVideoIframeExample1"
                                                data-classes="u-video-player__played">
                                                <span class="u-video-player__icon u-video-player__icon--lg text-primary bg-transparent">
                                                    <span class="flaticon-multimedia text-white ml-0 font-size-60 u-video-player__icon-inner"></span>
                                                </span>
                                            </a>
                                            <!-- End Play Button -->
                
                                            <!-- Video Iframe -->
                                            <div class="embed-responsive embed-responsive-16by9 rounded-sm">
                                                <div id="youTubeVideoIframeExample1"></div>
                                            </div>
                                            <!-- End Video Iframe -->
                                        </div>
                                        <!-- End Video Block -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-3">
                                <div class="mb-4">
                                    <div class="border border-color-7 rounded mb-5">
                                        <div class="border-bottom">
                                            <div class="p-4">
                                                <span class="font-size-14">Harga mulai dari:</span><br>
                                                <span class="font-size-24 text-gray-6 font-weight-bold ml-1">Rp. 135.000 (Plus Snorkeling)</span>
                                            </div>
                                        </div>
                                        <div class="p-4">
                                            <div class="text-center">
                                                <a href="rental-booking.html" class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Booking Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border border-color-7 rounded p-4 mb-5">
                                        <h6 class="font-size-17 font-weight-bold text-gray-3 mx-1 mb-3 pb-1">Kenapa harus menggunakan jasa kami?</h6>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-calendar font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Bisa Booking Kapanpun</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-invoice font-size-23 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Pembayaran Mudah</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <i class="flaticon-user-1 font-size-22 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Tour Guide </a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-house font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Tersedia Basecamp</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-support font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Admin Online 24 Jam</a>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center mb-0">
                                            <i class="flaticon-dish font-size-25 text-primary mr-3 pr-1"></i>
                                            <h6 class="mb-0 font-size-14 text-gray-1">
                                                <a href="#">Mendapatkan Gratis Makan Berkualitas</a>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Tabs v1 -->

@endsection