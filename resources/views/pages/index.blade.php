@extends('layouts.landing')

@section('content')
<!-- ========== MAIN CONTENT ========== -->
<main id="content">
    <!-- ========== HERO ========== -->
    <div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url(img/1920x960/giliketapang1.png);">
        <div class="container space-2 space-top-xl-10">
            <!-- Info -->
            <div class="py-wd-10 pb-5">
              <h1 class="font-size-64 font-size-xs-30 text-white font-weight-bold">Pesona Bahari Bawah Laut<br/>Gili Ketapang - Probolinggo</h1>
              <p class="font-size-20 font-weight-normal text-white">Open Trip Resmi dari Gili Ketapang</p>
            </div>
            <!-- End Info -->
            <!-- Nav Classic -->
                        <ul class="nav tab-nav-rounded flex-nowrap pb-2 pb-md-4 tab-nav tab-gili" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link font-weight-medium active pl-md-5 pl-3" id="pills-one-example2-tab" data-toggle="pill" href="#pills-one-example2" role="tab" aria-controls="pills-one-example2" aria-selected="true">
                                    <div class="d-flex flex-column flex-md-row  position-relative text-white align-items-center">
                                        <figure class="ie-height-40 d-md-block mr-md-3">
                                            <div class="icon">
                                            <img class="icon-gili" src="img/icon-white/hotel.png" alt="">
                                        </div>
                                        </figure>
                                        <span class="tabtext mt-2 mt-md-0 font-weight-semi-bold">Hotel</span>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link font-weight-medium" id="pills-two-example2-tab" data-toggle="pill" href="#pills-two-example2" role="tab" aria-controls="pills-two-example2" aria-selected="true">
                                    <div class="d-flex flex-column flex-md-row  position-relative text-white align-items-center">
                                        <figure class="ie-height-40 d-md-block mr-md-3">
                                            <div class="icon">
                                            <img class="icon-gili" src="img/icon-white/snorkeling.png" alt="">
                                            </div>
                                        </figure>
                                        <span class="tabtext mt-2 mt-md-0 font-weight-semi-bold">Snorkeling</span>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link font-weight-medium" id="pills-three-example2-tab" data-toggle="pill" href="#pills-three-example2" role="tab" aria-controls="pills-three-example2" aria-selected="true">
                                    <div class="d-flex flex-column flex-md-row  position-relative text-white align-items-center">
                                        <figure class=" ie-height-40  d-md-block mr-md-3">
                                            <div class="icon">
                                                <img class="icon-gili" src="img/icon-white/mancing.png" alt="">
                                            </div>
                                        </figure>
                                        <span class="tabtext mt-2 mt-md-0 font-weight-semi-bold">Mancing</span>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link font-weight-medium" id="pills-four-example2-tab" data-toggle="pill" href="#pills-four-example2" role="tab" aria-controls="pills-four-example2" aria-selected="true">
                                    <div class="d-flex flex-column flex-md-row  position-relative text-white align-items-center">
                                        <figure class="ie-height-40 d-md-block mr-md-3">
                                            <div class="icon">
                                            <img class="icon-gili" src="img/icon-white/jetski.png" alt="">
                                            </div>
                                        </figure>
                                        <span class="tabtext mt-2 mt-md-0 font-weight-semi-bold">Jetski</span>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link font-weight-medium" id="pills-five-example2-tab" data-toggle="pill" href="#pills-five-example2" role="tab" aria-controls="pills-five-example2" aria-selected="true">
                                    <div class="d-flex flex-column flex-md-row  position-relative text-white align-items-center">
                                        <figure class="ie-height-40 d-md-block mr-md-3">
                                            <div class="icon">
                                                
                                            <img class="icon-gili" src="img/icon-white/camping.png" alt="">
                                        
                                        </div>
                                        </figure>
                                        <span class="tabtext mt-2 mt-md-0 font-weight-semi-bold">Camping</span>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link font-weight-medium" id="pills-seven-example2-tab" data-toggle="pill" href="#pills-seven-example2" role="tab" aria-controls="pills-seven-example2" aria-selected="true">
                                    <div class="d-flex flex-column flex-md-row  position-relative  text-white align-items-center">
                                        <figure class="ie-height-40 d-md-block mr-md-3">
                                           <div class="icon">
                                            <img class="icon-gili2" src="img/icon-white/perahu.png" alt="">
                                       
                                        </div>
                                        </figure>
                                        <span class="tabtext mt-2 mt-md-0 font-weight-semi-bold">Perahu</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- End Nav Classic -->
        <div class="tab-content" >
            <div class="tab-pane fade active show" id="pills-one-example2" role="tabpanel" aria-labelledby="pills-one-example-t1-tab"> 
            <div class="tabtravel space-top-lg-2 space-top-xl-3">
                <!-- Search Jobs Form -->
                <div class="card border-0">
                    <div class="card-body">
                        <form class="js-validate">
                          <div class="row d-block nav-select d-lg-flex mb-lg-3 px-lg-3 px-2">
                            <div class="col-sm-12 col-lg-3dot7 mb-8 mb-lg-0 ">
                                <!-- Input -->
                                <img class="min-height-230 bg-img-hero card-img-top" src="img/iklan-ig.jpeg" alt="Smiley face">
                                <!-- End Input -->
                            </div>
                            
                            <div class="col-sm-12 col-lg-3dot6 mb-4 mb-lg-0 ">
                                <!-- Input -->
                                <h3 style="text-align: left;"><b>Paket Baru!<br/> Hotel</b></h3>
                                <p class="text-gray-1" style="text-align: left;">Tetap aman berlibur ditengah pandemi dengan paket wisata pemancingan dengan private boat di sekitar Gili Ketapang, Probolinggo. <br><br>Tersedia berbagai spot pemancingan di area pesisir Gili Ketapang Probolinggo</p>
                                <!-- End Input -->
                            </div>

                            <div class="col-sm-12 col-lg-2dot8 mb-4 mb-lg-0">
                                <!-- Input -->
                                <span class="d-block text-gray-1 font-weight-normal mb-0 text-left">Pilihan Paket</span>
                                <div class="js-focus-state">
                                    <div class="d-flex border-bottom border-width-2 border-color-1">
                                        <i class="flaticon-portfolio d-flex align-items-center mr-2 text-primary font-weight-semi-bold"></i>
                                        <select class="js-select selectpicker dropdown-select">
                                            <option value="2 Rooms - 3 Guests" selected>Mancing Jarak Dekat</option>
                                            <option value="2 Rooms - 3 Guests">Mancing Jarak Jauh</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- End Input -->
                            </div>

                            <div class="col-sm-12 col-lg-1dot8 align-self-lg-end text-md-right">
                                <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang"  target=”_blank”  class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Pesan Sekarang</a>
                            </div>
                          </div>
                          <!-- End Checkbox -->
                        </form>
                    </div>
                </div>
                <!-- End Search Jobs Form -->
            </div>
            </div>
            <div class="tab-pane fade" id="pills-two-example2" role="tabpanel" aria-labelledby="pills-one-example-t1-tab"> 
                <div class="tabtravel space-top-lg-2 space-top-xl-3">
                    <!-- Search Jobs Form -->
                    <div class="card border-0">
                        <div class="card-body">
                            <form class="js-validate">
                              <div class="row d-block nav-select d-lg-flex mb-lg-3 px-lg-3 px-2">
                                <div class="col-sm-12 col-lg-3dot7 mb-8 mb-lg-0 ">
                                    <!-- Input -->
                                    <img class="min-height-230 bg-img-hero card-img-top" src="img/iklan-ig.jpeg" alt="Smiley face">
                                    <!-- End Input -->
                                </div>
                                
                                <div class="col-sm-12 col-lg-3dot6 mb-4 mb-lg-0 ">
                                    <!-- Input -->
                                    <h3 style="text-align: left;"><b>Paket Baru!<br/> Snorkeling</b></h3>
                                    <p class="text-gray-1" style="text-align: left;">Tetap aman berlibur ditengah pandemi dengan paket wisata pemancingan dengan private boat di sekitar Gili Ketapang, Probolinggo. <br><br>Tersedia berbagai spot pemancingan di area pesisir Gili Ketapang Probolinggo</p>
                                    <!-- End Input -->
                                </div>
    
                                <div class="col-sm-12 col-lg-2dot8 mb-4 mb-lg-0">
                                    <!-- Input -->
                                    <span class="d-block text-gray-1 font-weight-normal mb-0 text-left">Pilihan Paket</span>
                                    <div class="js-focus-state">
                                        <div class="d-flex border-bottom border-width-2 border-color-1">
                                            <i class="flaticon-portfolio d-flex align-items-center mr-2 text-primary font-weight-semi-bold"></i>
                                            <select class="js-select selectpicker dropdown-select">
                                                <option value="2 Rooms - 3 Guests" selected>Mancing Jarak Dekat</option>
                                                <option value="2 Rooms - 3 Guests">Mancing Jarak Jauh</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- End Input -->
                                </div>
    
                                <div class="col-sm-12 col-lg-1dot8 align-self-lg-end text-md-right">
                                    <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang"  target=”_blank”  class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Pesan Sekarang</a>
                                </div>
                              </div>
                              <!-- End Checkbox -->
                            </form>
                        </div>
                    </div>
                    <!-- End Search Jobs Form -->
                </div>
                </div>
                <div class="tab-pane fade" id="pills-three-example2" role="tabpanel" aria-labelledby="pills-one-example-t1-tab"> 
                    <div class="tabtravel space-top-lg-2 space-top-xl-3">
                        <!-- Search Jobs Form -->
                        <div class="card border-0">
                            <div class="card-body">
                                <form class="js-validate">
                                  <div class="row d-block nav-select d-lg-flex mb-lg-3 px-lg-3 px-2">
                                    <div class="col-sm-12 col-lg-3dot7 mb-8 mb-lg-0 ">
                                        <!-- Input -->
                                        <img class="min-height-230 bg-img-hero card-img-top" src="img/iklan-ig.jpeg" alt="Smiley face">
                                        <!-- End Input -->
                                    </div>
                                    
                                    <div class="col-sm-12 col-lg-3dot6 mb-4 mb-lg-0 ">
                                        <!-- Input -->
                                        <h3 style="text-align: left;"><b>Paket Baru!<br/> Mancing Private Boat</b></h3>
                                        <p class="text-gray-1" style="text-align: left;">Tetap aman berlibur ditengah pandemi dengan paket wisata pemancingan dengan private boat di sekitar Gili Ketapang, Probolinggo. <br><br>Tersedia berbagai spot pemancingan di area pesisir Gili Ketapang Probolinggo</p>
                                        <!-- End Input -->
                                    </div>
        
                                    <div class="col-sm-12 col-lg-2dot8 mb-4 mb-lg-0">
                                        <!-- Input -->
                                        <span class="d-block text-gray-1 font-weight-normal mb-0 text-left">Pilihan Paket</span>
                                        <div class="js-focus-state">
                                            <div class="d-flex border-bottom border-width-2 border-color-1">
                                                <i class="flaticon-portfolio d-flex align-items-center mr-2 text-primary font-weight-semi-bold"></i>
                                                <select class="js-select selectpicker dropdown-select">
                                                    <option value="2 Rooms - 3 Guests" selected>Mancing Jarak Dekat</option>
                                                    <option value="2 Rooms - 3 Guests">Mancing Jarak Jauh</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- End Input -->
                                    </div>
        
                                    <div class="col-sm-12 col-lg-1dot8 align-self-lg-end text-md-right">
                                        <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang"  target=”_blank”  class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Pesan Sekarang</a>
                                    </div>
                                  </div>
                                  <!-- End Checkbox -->
                                </form>
                            </div>
                        </div>
                        <!-- End Search Jobs Form -->
                    </div>
                    </div>
                    <div class="tab-pane fade" id="pills-four-example2" role="tabpanel" aria-labelledby="pills-one-example-t1-tab"> 
                        <div class="tabtravel space-top-lg-2 space-top-xl-3">
                            <!-- Search Jobs Form -->
                            <div class="card border-0">
                                <div class="card-body">
                                    <form class="js-validate">
                                      <div class="row d-block nav-select d-lg-flex mb-lg-3 px-lg-3 px-2">
                                        <div class="col-sm-12 col-lg-3dot7 mb-8 mb-lg-0 ">
                                            <!-- Input -->
                                            <img class="min-height-230 bg-img-hero card-img-top" src="img/iklan-ig.jpeg" alt="Smiley face">
                                            <!-- End Input -->
                                        </div>
                                        
                                        <div class="col-sm-12 col-lg-3dot6 mb-4 mb-lg-0 ">
                                            <!-- Input -->
                                            <h3 style="text-align: left;"><b>Paket Baru!<br/> Jetski</b></h3>
                                            <p class="text-gray-1" style="text-align: left;">Tetap aman berlibur ditengah pandemi dengan paket wisata pemancingan dengan private boat di sekitar Gili Ketapang, Probolinggo. <br><br>Tersedia berbagai spot pemancingan di area pesisir Gili Ketapang Probolinggo</p>
                                            <!-- End Input -->
                                        </div>
            
                                        <div class="col-sm-12 col-lg-2dot8 mb-4 mb-lg-0">
                                            <!-- Input -->
                                            <span class="d-block text-gray-1 font-weight-normal mb-0 text-left">Pilihan Paket</span>
                                            <div class="js-focus-state">
                                                <div class="d-flex border-bottom border-width-2 border-color-1">
                                                    <i class="flaticon-portfolio d-flex align-items-center mr-2 text-primary font-weight-semi-bold"></i>
                                                    <select class="js-select selectpicker dropdown-select">
                                                        <option value="2 Rooms - 3 Guests" selected>Mancing Jarak Dekat</option>
                                                        <option value="2 Rooms - 3 Guests">Mancing Jarak Jauh</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- End Input -->
                                        </div>
            
                                        <div class="col-sm-12 col-lg-1dot8 align-self-lg-end text-md-right">
                                            <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang"  target=”_blank”  class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Pesan Sekarang</a>
                                        </div>
                                      </div>
                                      <!-- End Checkbox -->
                                    </form>
                                </div>
                            </div>
                            <!-- End Search Jobs Form -->
                        </div>
                        </div>
                        <div class="tab-pane fade" id="pills-five-example2" role="tabpanel" aria-labelledby="pills-one-example-t1-tab"> 
                            <div class="tabtravel space-top-lg-2 space-top-xl-3">
                                <!-- Search Jobs Form -->
                                <div class="card border-0">
                                    <div class="card-body">
                                        <form class="js-validate">
                                          <div class="row d-block nav-select d-lg-flex mb-lg-3 px-lg-3 px-2">
                                            <div class="col-sm-12 col-lg-3dot7 mb-8 mb-lg-0 ">
                                                <!-- Input -->
                                                <img class="min-height-230 bg-img-hero card-img-top" src="img/iklan-ig.jpeg" alt="Smiley face">
                                                <!-- End Input -->
                                            </div>
                                            
                                            <div class="col-sm-12 col-lg-3dot6 mb-4 mb-lg-0 ">
                                                <!-- Input -->
                                                <h3 style="text-align: left;"><b>Paket Baru!<br/> Camping</b></h3>
                                                <p class="text-gray-1" style="text-align: left;">Tetap aman berlibur ditengah pandemi dengan paket wisata pemancingan dengan private boat di sekitar Gili Ketapang, Probolinggo. <br><br>Tersedia berbagai spot pemancingan di area pesisir Gili Ketapang Probolinggo</p>
                                                <!-- End Input -->
                                            </div>
                
                                            <div class="col-sm-12 col-lg-2dot8 mb-4 mb-lg-0">
                                                <!-- Input -->
                                                <span class="d-block text-gray-1 font-weight-normal mb-0 text-left">Pilihan Paket</span>
                                                <div class="js-focus-state">
                                                    <div class="d-flex border-bottom border-width-2 border-color-1">
                                                        <i class="flaticon-portfolio d-flex align-items-center mr-2 text-primary font-weight-semi-bold"></i>
                                                        <select class="js-select selectpicker dropdown-select">
                                                            <option value="2 Rooms - 3 Guests" selected>Mancing Jarak Dekat</option>
                                                            <option value="2 Rooms - 3 Guests">Mancing Jarak Jauh</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- End Input -->
                                            </div>
                
                                            <div class="col-sm-12 col-lg-1dot8 align-self-lg-end text-md-right">
                                                <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang"  target=”_blank”  class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Pesan Sekarang</a>
                                            </div>
                                          </div>
                                          <!-- End Checkbox -->
                                        </form>
                                    </div>
                                </div>
                                <!-- End Search Jobs Form -->
                            </div>
                            </div>
                            <div class="tab-pane fade" id="pills-seven-example2" role="tabpanel" aria-labelledby="pills-one-example-t1-tab"> 
                                <div class="tabtravel space-top-lg-2 space-top-xl-3">
                                    <!-- Search Jobs Form -->
                                    <div class="card border-0">
                                        <div class="card-body">
                                            <form class="js-validate">
                                              <div class="row d-block nav-select d-lg-flex mb-lg-3 px-lg-3 px-2">
                                                <div class="col-sm-12 col-lg-3dot7 mb-8 mb-lg-0 ">
                                                    <!-- Input -->
                                                    <img class="min-height-230 bg-img-hero card-img-top" src="img/iklan-ig.jpeg" alt="Smiley face">
                                                    <!-- End Input -->
                                                </div>
                                                
                                                <div class="col-sm-12 col-lg-3dot6 mb-4 mb-lg-0 ">
                                                    <!-- Input -->
                                                    <h3 style="text-align: left;"><b>Paket Baru!<br/> Perahu</b></h3>
                                                    <p class="text-gray-1" style="text-align: left;">Tetap aman berlibur ditengah pandemi dengan paket wisata pemancingan dengan private boat di sekitar Gili Ketapang, Probolinggo. <br><br>Tersedia berbagai spot pemancingan di area pesisir Gili Ketapang Probolinggo</p>
                                                    <!-- End Input -->
                                                </div>
                    
                                                <div class="col-sm-12 col-lg-2dot8 mb-4 mb-lg-0">
                                                    <!-- Input -->
                                                    <span class="d-block text-gray-1 font-weight-normal mb-0 text-left">Pilihan Paket</span>
                                                    <div class="js-focus-state">
                                                        <div class="d-flex border-bottom border-width-2 border-color-1">
                                                            <i class="flaticon-portfolio d-flex align-items-center mr-2 text-primary font-weight-semi-bold"></i>
                                                            <select class="js-select selectpicker dropdown-select">
                                                                <option value="2 Rooms - 3 Guests" selected>Mancing Jarak Dekat</option>
                                                                <option value="2 Rooms - 3 Guests">Mancing Jarak Jauh</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!-- End Input -->
                                                </div>
                    
                                                <div class="col-sm-12 col-lg-1dot8 align-self-lg-end text-md-right">
                                                    <a href="https://wa.me/6281216632836?text=Saya%20Tertarik%20untuk%20Berwisata%20di%20Gili%20Ketapang"  target=”_blank”  class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Pesan Sekarang</a>
                                                </div>
                                              </div>
                                              <!-- End Checkbox -->
                                            </form>
                                        </div>
                                    </div>
                                    <!-- End Search Jobs Form -->
                                </div>
                                </div>
        </div>
        </div>
    </div>
    <!-- ========== END HERO ========== -->

    <!-- Destinantion  -->
            <div class="border-bottom border-color-8">
                <div class="container space-bottom-1 space-top-lg-3">
                    <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-4 mb-xl-7 pb-xl-1">
                        <h2 class="section-title text-black font-size-30 font-weight-bold mb-0">Wisata di Gili Ketapang</h2>
                    </div>
                    <div class="w-lg-80 w-xl-60 mx-auto collapse_custom position-relative mb-4 pb-xl-1">
                        <p>Pulau Gili Ketapang terletak di sebelah utara Kota Probolinggo, Jawa Timur menawarkan keindahan wisata bahari laut dengan terumbu karang dan berbagai jenis ikan yang indah.
</p>
                        </div>
                        <div class="w-lg-80 w-xl-60 mx-auto collapse_custom position-relative mb-4 pb-xl-1">
                        <h4 class="font-size-21 font-weight-semi-bold text-gray-6 pb-1">Perjalanan</h4>
                        <p>Untuk menuju ke Gili Ketapang sendiri diperlukan waktu sekitar 2 jam dari Surabaya, atau sekitar 1,5 jam dari Kota Malang untuk menuju ke Pelabuhan Tanjung Tembaga - Probolinggo. Setelah sampai di pelabuhan Guide akan menjemput wisatawan dengan perahu untuk menuju ke pulau Gili Ketapang yang menempuh waktu sekitar 20 menit.<br/><br/>
</p>

                        <div class="collapse" id="collapseLinkExample">
                                
                        <h4 class="font-size-21 font-weight-semi-bold text-gray-6 pb-1">Wahana Wisata</h4>
                            <p>Disini wisatawan dapat melakukan 5 kegiatan sekaligus diantaranya; Snorkeling, Camping, Banana Boat, Jetski, Private Boat Memancing, dan mengelilingi berbagai spot foto menarik yang Instagramable dengan perahu nelayan lokal.</p>
                            <p>Tak hanya itu, wisatawan juga dimudahkan dengan berbagai fasilitas yang memanjakan dan memudahkan ketika berwisata ke Gili Ketapang dengan menggunakan bantuan dari Tim Gili Ketapang Trip.</p>
                        </div>
                        

                        <a class="link-collapse link-collapse-custom gradient-overlay-half mb-5 d-inline-block border-bottom border-primary" data-toggle="collapse" href="#collapseLinkExample" role="button" aria-expanded="false" aria-controls="collapseLinkExample">
                            <span class="link-collapse__default font-size-14">View More <i class="flaticon-down-chevron font-size-10 ml-1"></i></span>
                            <span class="link-collapse__active font-size-14">View Less <i class="flaticon-arrow font-size-10 ml-1"></i></span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- End Destinantion -->
            <!-- Features Section -->
            <div class="container text-center space-1">
                <!-- Title -->
                <div class="w-md-80 w-lg-50 text-center mx-md-auto pb-1">
                    <h2 class="section-title text-black font-size-30 font-weight-bold">Fasilitas oleh Gili Ketapang Trip</h2>
                </div>
                <!-- End Title -->
                <!-- Features -->
                <div class="mb-8">
                    <div class="row">
                        <!-- Icon Block -->
                        <div class="col-md-4">
                           <img class="img-fluid pb-5" src="img/icons/fasilitas1.svg" width="128px" height="128px">
                            <h5 class="font-size-17 text-dark font-weight-bold mb-2"><a href="#">Open Trip Tiap Hari</a></h5>
                            <p class="text-gray-1 px-xl-2 px-uw-7">Kamu bisa booking kapanpun untuk private trip atau berapapun jumlah rombonganmu. <b>Dengan Protokol #NewNormal</b></p>
                        </div>
                        <!-- End Icon Block -->

                        <!-- Icon Block -->
                        <div class="col-md-4">
                           <img class="img-fluid pb-5" src="img/icons/fasilitas2.svg" width="128px" height="128px">
                            <h5 class="font-size-17 text-dark font-weight-bold mb-2"><a href="#">Perlengkapan Snorkeling</a></h5>
                            <p class="text-gray-1 px-xl-2 px-uw-7">Paket sudah termasuk biaya sewa perlengkapan snorkeling lengkap dan pelampung, dijamin kebersihannya!</p>
                        </div>
                        <!-- End Icon Block -->

                        <!-- Icon Block -->
                        <div class="col-md-4">
                           <img class="img-fluid pb-5" src="img/icons/fasilitas3.svg" width="128px" height="128px">
                            <h5 class="font-size-17 text-dark font-weight-bold mb-2"><a href="#">Kapal Pulang Pergi</a></h5>
                            <p class="text-gray-1 px-xl-2 px-uw-7">Wisatawan akan dijemput dan diantar oleh kapal tim kami di meeting point (Pelabuhan Probolinggo).</p>
                        </div>
                        <!-- End Icon Block -->
                    </div>
                </div>
                <!-- End Features -->
                <!-- Features -->
                <div class="mb-8">
                    <div class="row">
                        <!-- Icon Block -->
                        <div class="col-md-4">
                           <img class="img-fluid pb-5" src="img/icons/fasilitas4.svg" width="128px" height="128px">
                            <h5 class="font-size-17 text-dark font-weight-bold mb-2"><a href="#">Guide Profesional</a></h5>
                            <p class="text-gray-1 px-xl-2 px-uw-7">Sudah termasuk Tour Guide dan Snorkeling yang akan menemani selama berwisata di Gili Ketapang.</p>
                        </div>
                        <!-- End Icon Block -->

                        <!-- Icon Block -->
                        <div class="col-md-4">
                           <img class="img-fluid pb-5" src="img/icons/fasilitas5.svg" width="128px" height="128px">
                            <h5 class="font-size-17 text-dark font-weight-bold mb-2"><a href="#">Jamuan Menu Lokal</a></h5>
                            <p class="text-gray-1 px-xl-2 px-uw-7">Setelah beraktivitas yang melelahkan wisatawan juga disuguhkan hidangan makan siang lokal seperti Ikan Bakar.</p>
                        </div>
                        <!-- End Icon Block -->

                        <!-- Icon Block -->
                        <div class="col-md-4">
                           <img class="img-fluid pb-5" src="img/icons/fasilitas6.svg" width="128px" height="128px">
                            <h5 class="font-size-17 text-dark font-weight-bold mb-2"><a href="#">Dokumentasi</a></h5>
                            <p class="text-gray-1 px-xl-2 px-uw-7">Tidak perlu ribet membawa kamera sendiri karena kami menyediakan fasilitas dokumentasi seperti Action Cam & Drone.</p>
                        </div>
                        <!-- End Icon Block -->
                    </div>
                </div>
                <!-- End Features -->
            </div>
            <!-- End Features Section -->

    <!-- Icon Block Center -->
    <div class="icon-block-center icon-center-v2 bg-primary">
        <div class="container text-center space-1">
            <!-- Features -->
            <div class="row">
                <!-- Icon Block -->
                <div class="col-md-3">
                    <i class="flaticon-user-1 text-white font-size-80 mb-3"></i>
                    <h5 class="font-size-30 text-white font-weight-bold mb-2 js-counter">2302</h5>
                    <p class="text-white px-xl-2 text-lh-inherit px-uw-3">Wisatawan Puas</p>
                </div>
                <!-- End Icon Block -->

                <!-- Icon Block -->
                <div class="col-md-3">
                    <i class="flaticon-placeholder text-white font-size-80 mb-3"></i>
                    <h5 class="font-size-30 text-white font-weight-bold mb-2">2016</h5>
                    <p class="text-white px-xl-2 text-lh-inherit px-uw-3">Berpengalaman Sejak Awal</p>
                </div>
                <!-- End Icon Block -->

                <!-- Icon Block -->
                <div class="col-md-3">
                    <i class="flaticon-favorites text-white font-size-80 mb-3"></i>
                    <h5 class="font-size-30 text-white font-weight-bold mb-2">10+</h5>
                    <p class="text-white px-xl-2 text-lh-inherit px-uw-3">Spot Foto Menarik</p>
                </div>
                <!-- End Icon Block -->

                <!-- Icon Block -->
                <div class="col-md-3">
                    <i class="flaticon-jetski-facing-right text-white font-size-80 mb-3"></i>
                    <h5 class="font-size-30 text-white font-weight-bold mb-2">5 Jenis</h5>
                    <p class="text-white px-xl-2 text-lh-inherit px-uw-3">Wahana Wisata</p>
                </div>
                <!-- End Icon Block -->
            </div>
            <!-- End Features -->
        </div>
    </div>
    <!-- Icon Block Center -->


    <div class="testimonial-block testimonial-v2 border-bottom border-color-8" id="testi">
        <div class="container">
            <div class="pt-7 pb-8">
                <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-5">
                    <h2 class="section-title text-black font-size-30 font-weight-bold mb-0">Apa kata mereka?</h2>
                </div>
                
                    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-63b7d431-acc7-4308-9f3a-21b31f5723e1"></div>

            </div>
        </div>
    </div>
    
    
    <!-- Tabs v1 -->
     <div class="tabs-block tab-v1">
        <div class="container space-lg-1">
            <div class="w-md-80 w-lg-50 text-center mx-md-auto my-3">
                <h2 class="section-title text-black font-size-30 font-weight-bold mb-0">Aktivitas di Gili Ketapang</h2>
            </div>
            <!-- Nav Classic -->
            <ul class="nav tab-nav-pill flex-nowrap pb-4 pb-lg-5 tab-nav justify-content-lg-center" role="tablist">
                <li class="nav-item">
                    <a class="nav-link font-weight-medium active" id="pills-one-example-t1-tab" data-toggle="pill" href="#pills-one-example-t1" role="tab" aria-controls="pills-one-example-t1" aria-selected="true">
                        <div class="d-flex flex-column flex-md-row  position-relative text-dark align-items-center">
                            <span class="tabtext font-weight-semi-bold">Snorkeling</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-medium" id="pills-dua-example-t1-tab" data-toggle="pill" href="#pills-dua-example-t1" role="tab" aria-controls="pills-one-example-t1" aria-selected="true">
                        <div class="d-flex flex-column flex-md-row  position-relative text-dark align-items-center">
                            <span class="tabtext font-weight-semi-bold">Pemancingan</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-medium" id="pills-two-example-t1-tab" data-toggle="pill" href="#pills-two-example-t1" role="tab" aria-controls="pills-two-example-t1" aria-selected="true">
                        <div class="d-flex flex-column flex-md-row  position-relative text-dark align-items-center">
                            <span class="tabtext font-weight-semi-bold">Banana Boat</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-medium" id="pills-three-example-t1-tab" data-toggle="pill" href="#pills-three-example-t1" role="tab" aria-controls="pills-three-example-t1" aria-selected="true">
                        <div class="d-flex flex-column flex-md-row  position-relative text-dark align-items-center">
                            <span class="tabtext font-weight-semi-bold">Drone Gili</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-medium" id="pills-four-example-t1-tab" data-toggle="pill" href="#pills-four-example-t1" role="tab" aria-controls="pills-four-example-t1" aria-selected="true">
                        <div class="d-flex flex-column flex-md-row  position-relative text-dark align-items-center">
                            <span class="tabtext font-weight-semi-bold">Gua Kucing</span>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- End Nav Classic -->
            <div class="tab-content" >
                <div class="tab-pane fade active show" id="pills-one-example-t1" role="tabpanel" aria-labelledby="pills-one-example-t1-tab"> 
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3 mb-md-4 pb-1">
                            <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/snorkeling')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="img/300x230/imgsatu.jpg" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Terfavorit</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.90.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/snorkeling')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Snorkeling</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3 mb-md-4 pb-1">
                            <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/snorkeling')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="img/300x230/imgdua.jpg" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Tereksklusif</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.100.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/snorkeling')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Snorkeling VIP</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3 mb-md-4 pb-1">
                            <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/snorkeling')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="img/300x230/imgtiga.jpg" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Terhemat</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.135.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/snorkeling')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Snorkeling + Goa Kucing</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3 mb-md-4 pb-1">
                            <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/snorkeling')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="img/300x230/imgfour.jpg" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Rame-Rame</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.165.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/snorkeling')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Rombongan + Kaos Exclusive</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-dua-example-t1" role="tabpanel" aria-labelledby="pills-dua-example-t1-tab">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3 mb-md-4 pb-1">
                            <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/pemancingan')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="img/300x230/mancing1.png" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Terfavorit</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.800.000<small class="mr-2">&nbsp;(Maksimal 10 Orang)</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/pemancingan')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Pemancingan Gili Ketapang - <b>Rute Dekat</b></a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 1 Hari
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3 mb-md-4 pb-1">
                            <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/pemancingan')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="img/300x230/mancing2.png" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Tereksklusif</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.1.000.000<small class="mr-2">&nbsp;(Maksimal 10 Orang)</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="https://madrasthemes.github.io/mytravel-html/html/tour/tour-single-v1.html" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Pemancingan Gili Ketapang - <b>Rute Jauh</b></a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 1 Hari
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-two-example-t1" role="tabpanel" aria-labelledby="pills-two-example-t1-tab">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3 mb-md-4 pb-1">
                            <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/banana-boat')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="img/300x230/imglima.jpg" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Terfavorit</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.150.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/banana-boat')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Banana Boat (Plus Snorkeling)</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-three-example-t1" role="tabpanel" aria-labelledby="pills-three-example-t1-tab">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3 mb-md-4 pb-1">
                            <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/drone')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="img/300x230/imgenam.jpg" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Terfavorit</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.150.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/drone')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Drone (Plus Snorkeling)</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-four-example-t1" role="tabpanel" aria-labelledby="pills-four-example-t1-tab">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3 mb-md-4 pb-1">
                            <div class="card mb-1 transition-3d-hover shadow-hover-2 tab-card h-100">
                                <div class="position-relative mb-2">
                                    <a href="{{url ('/activities/gua-kucing')}}" class="d-block gradient-overlay-half-bg-gradient-v5">
                                        <img class="min-height-230 bg-img-hero card-img-top" src="img/300x230/imgtiga.jpg" alt="img">
                                    </a>
                                    <div class="position-absolute top-0 left-0 pt-5 pl-3">
                                        <span class="badge badge-pill bg-white text-primary px-4 py-2 font-size-14 font-weight-normal">Terfavorit</span>
                                    </div>
                                    <div class="position-absolute bottom-0 left-0 right-0">
                                        <div class="px-3 pb-2">
                                            <h2 class="h5 text-white mb-0 font-weight-bold">Rp.135.000<small class="mr-2">&nbsp;Per Orang</small></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-4 py-2">
                                    <a class="d-block">
                                        <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                            <i class="icon flaticon-pin-1 mr-2 font-size-15"></i> Gili Ketapang
                                        </div>
                                    </a>
                                    <a href="{{url ('/activities/gua-kucing')}}" class="card-title font-size-17 font-weight-bold mb-0 text-dark">Paket Snorkeling + Goa Kucing</a>
                                    <div class="my-2">
                                        <div class="d-inline-flex align-items-center font-size-17 text-lh-1 text-primary">
                                            <div class="green-lighter mr-2">
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                                <small class="fas fa-star"></small>
                                            </div>
                                            <span class="text-secondary font-size-14 mt-1">Rating</span>
                                        </div>
                                    </div>
                                    <div class="mb-1 d-flex align-items-center font-size-14 text-gray-1">
                                        <i class="icon flaticon-clock-circular-outline mr-2 font-size-14"></i> 5 Jam
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Tabs v1 -->
    


     <!-- Recent Articles -->
            <div class="border-bottom border-color-8">
                <div class="container space-lg-1">
                    <!-- Title -->
                    <div class="w-md-80 w-lg-50 text-center mx-md-auto mt-3 mb-5 mb-lg-8 pb-lg-2">
                        <h2 class="section-title text-black font-size-30 font-weight-bold">Info Terbaru Gili Ketapang</h2>
                    </div>
                    <!-- End Title -->
                    <script src="https://giliketapangtrip.co.id/js/page/elfsight.js" defer></script>
<div class="elfsight-app-instagram-feed"></div>
                </div>
            </div>
            
            
    <!-- End Clients v1 -->
    <div class="clients-block clients-v1 border-bottom border-color-8">
        <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-5 my-3">
            <h2 class="section-title text-black font-size-30 font-weight-bold mb-0">Our Partner</h2>
        </div>
        <div class="container space-1">
             <div class="row justify-content-between align-items-center pb-lg-1 text-center text-md-left">
            <div class="col-12 col-md mb-5">
                <img class="img-fluid" src="img/icons/clear.png" alt="Trip Gili Ketapang">
            </div>
            <div class="col-12 col-md mb-5">
                <img class="img-fluid" src="img/200x200/img13.png" alt="Trip Advisor">
            </div>
            <div class="col-12 col-md mb-5">
                <img class="img-fluid" src="img/icons/gosocial.png" alt="GoSocial.co.id">
            </div>
            <div class="col-12 col-md mb-5">
                <img class="img-fluid" src="img/200x200/img15.png" alt="Trip Advisor">
            </div>
            <div class="col-12 col-md mb-md-5">
                <img class="img-fluid" src="img/icons/clear.png" alt="Wisata Gili">
            </div>
        </div>
            </div>
        </div>
    </div>
    <!-- End Clients v1 -->
</main>
<!-- ========== END MAIN CONTENT ========== -->
@endsection