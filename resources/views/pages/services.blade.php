@extends('layouts.landing')

@section('content')
<!-- Hero Section -->
<div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url(img/1920x400/img3.jpg);">
    <div class="container space-top-xl-3 py-6 py-xl-0">
        <div class="row justify-content-center py-xl-4">
            <!-- Info -->
            <div class="py-xl-10 py-5">
                <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">Butuh Liburan?</h1>
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb breadcrumb-no-gutter justify-content-center mb-0">
                  <li class="breadcrumb-item font-size-14"> <a class="text-white" href="{{url ('/')}}">Home</a> </li>
                    <li class="breadcrumb-item custom-breadcrumb-item font-size-14 text-white active" aria-current="page">{{$label}}</li>
                  </ol>
                </nav>
            </div>
            <!-- End Info -->
        </div>
    </div>
</div>
<!-- End Hero Section -->

<!-- Features Section -->
<div class="container text-center space-top-lg-2">
    <!-- Title -->
    <div class="w-md-80 w-lg-50 text-center mx-md-auto pb-1 pt-5 pb-md-6">
        <h2 class="section-title text-black font-size-30 font-weight-bold">How it Works</h2>
    </div>
    <!-- End Title -->
    <!-- Features -->
    <div class="mb-6">
        <div class="row">
            <!-- Icon Block -->
            <div class="col-lg-4 pb-4 pb-lg-0">
                <img class="img-fluid pb-5" src="img/icons/img8.jpg">
                <div class="text-lg-left  w-lg-80 mx-auto">
                    <h5 class="font-size-21 text-dark font-weight-bold mb-2"><a href="#">Sign Up</a></h5>
                    <p class="text-gray-1">Bringing you a modern, comfortable, and connected travel anything with us.</p>
                </div>
            </div>
            <!-- End Icon Block -->

            <!-- Icon Block -->
            <div class="col-lg-4 pb-4 pb-lg-0">
                <img class="img-fluid pb-5" src="img/icons/img9.jpg">
                <div class="text-lg-left w-lg-80 ml-auto">
                    <h5 class="font-size-21 text-dark font-weight-bold mb-2"><a href="#">Add Your Services</a></h5>
                    <p class="text-gray-1">Bringing you a modern, comfortable, and connected travel anything with us.</p>
                </div>
            </div>
            <!-- End Icon Block -->

            <!-- Icon Block -->
            <div class="col-lg-4 pb-4 pb-lg-0">
                <img class="img-fluid pb-5" src="img/icons/img10.jpg">
                    <div class="text-lg-left w-lg-80 mx-auto">
                        <h5 class="font-size-21 text-dark font-weight-bold mb-2"><a href="#">Get Bookings</a></h5>
                        <p class="text-gray-1">Bringing you a modern, comfortable, and connected travel anything with us.</p>
                    </div>
            </div>
            <!-- End Icon Block -->
        </div>
    </div>
    <!-- End Features -->
</div>
<!-- End Features Section -->
@endsection