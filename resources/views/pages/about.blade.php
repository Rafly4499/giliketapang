@extends('layouts.landing')

@section('content')
<!-- Hero Section -->
<div class="hero-block hero-v7 bg-img-hero-bottom gradient-overlay-half-sapphire-gradient text-center z-index-2" style="background-image: url(img/1920x400/img111.jpg);">
    <div class="container space-top-xl-3 py-6 py-xl-0">
        <div class="row justify-content-center py-xl-4">
            <!-- Info -->
            <div class="py-xl-10 py-5">
                <h1 class="font-size-40 font-size-xs-30 text-white font-weight-bold mb-0">Wisata Gili Ketapang</h1>
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb breadcrumb-no-gutter justify-content-center mb-0">
                  <li class="breadcrumb-item font-size-14"> <a class="text-white" href="{{url ('/')}}">Home</a> </li>
                    <li class="breadcrumb-item custom-breadcrumb-item font-size-14 text-white active" aria-current="page">{{$label}}</li>
                  </ol>
                </nav>
            </div>
            <!-- End Info -->
        </div>
    </div>
</div>
<!-- End Hero Section -->

<!-- Icon Block v1 -->
<div class="icon-block-center icon-center-v1 border-bottom border-color-8">
    <div class="container text-center space-1">
        <!-- Title -->
        <div class="w-md-80 w-lg-50 text-center mx-md-auto pb-1 mt-3">
            <h2 class="section-title text-black font-size-30 font-weight-bold">Nikmati liburan Anda bersama kami</h2>
        </div>
        <!-- End Title -->

        <!-- Features -->
        <div class="mb-2">
            <div class="row">
                <!-- Icon Block -->
                <div class="col-md-4">
                    <i class="flaticon-medal text-primary font-size-80 mb-3"></i>
                    <h5 class="font-size-17 text-dark font-weight-bold mb-2"><a href="#">Terpercaya Sejak 2016</a></h5>
                    <p class="text-gray-1 px-xl-2 px-uw-7">Kami termasuk salah satu pioner dalam wisata Gili Ketapang Probolinggo yang sudah dipercaya sejak 20016</p>
                </div>
                <!-- End Icon Block -->

                <!-- Icon Block -->
                <div class="col-md-4">
                    <i class="flaticon-browser-1 text-primary font-size-80 mb-3"></i>
                    <h5 class="font-size-17 text-dark font-weight-bold mb-2"><a href="#">Sistem Pemesanan Online</a></h5>
                    <p class="text-gray-1 px-xl-2 px-uw-7">Di tahun 2020 kami meluncurkan website resmi kami untuk memudahkan Anda memesan paket wisata gili ketapang dan melihat berbagai destinasi disana</p>
                </div>
                <!-- End Icon Block -->

                <!-- Icon Block -->
                <div class="col-md-4">
                    <i class="flaticon-favorites text-primary font-size-80 mb-3"></i>
                    <h5 class="font-size-17 text-dark font-weight-bold mb-2"><a href="#">Pelayanan yang Memuaskan</a></h5>
                    <p class="text-gray-1 px-xl-2 px-uw-7">Kami telah berpengalaman dalam melayani ribuan wisawan hingga saat ini. Percayakan liburan Anda bersama kami di Gili Ketapang Probolinggo</p>
                </div>
                <!-- End Icon Block -->
            </div>
        </div>
        <!-- End Features -->
    </div>
</div>
<!-- End Icon Block v1 -->

<!-- Banner v1-->
<div class="banner-block banner-v1 bg-img-hero space-3" style="background-image: url(img/1920x500/img1111.jpg);">
    <div class="max-width-650 mx-auto text-center mt-xl-5 mb-xl-2 px-3 px-md-0">
        <h6 class="text-white font-size-40 font-weight-bold mb-1">Wisata Gili Ketapang Probolinggo</h6>
        <p class="text-white font-size-18 font-weight-normal mb-4 pb-1 px-md-3 px-lg-0">Gili ketapang merupakan destinasi wisata snorkeling yang berada di jawa timur, pulau kecil ini berada di probolinggo. Di pulau ini anda dapat bersnorkeling menikmati surga bawah laut di barengi terumbu karang dan anemon laut menjadi rumah tinggal bagi ikan-ikan hias di pulau ini, khususnya jenis clownfish atau ikan nemo yang menjadi incaran para pengunjung untuk berfoto eksotis di bawah laut.</p>
        <a class="btn btn-outline-white border-width-2 rounded-xs min-width-200 font-weight-normal transition-3d-hover" href="https://madrasthemes.github.io/mytravel-html/html/blog/blog-list.html">Yuk Liburan</a>
    </div>
</div>
<!-- End Banner v1-->


<!-- Testimonials v2 -->
    <div class="testimonial-block testimonial-v2 border-bottom border-color-8">
        <div class="container">
            <div class="pt-7 pb-8">
                <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-5">
                    <h2 class="section-title text-black font-size-30 font-weight-bold mb-0">Apa kata mereka?</h2>
                </div>
                <!-- Slick Carousel -->
                <div class="js-slick-carousel u-slick u-slick--gutters-3" data-infinite="    true" data-autoplay="true" data-speed="3000" data-fade="true"
                data-pagi-classes="text-center u-slick__pagination mb-0 mt-4"
                    data-responsive='[{
                    "breakpoint": 992,
                       "settings": {
                         "slidesToShow": 1
                       }
                    }]'>

                    <div class="js-slide">
                        <!-- Testimonials -->
                        <div class="d-flex justify-content-center mb-6">
                            <div class="position-relative">
                                <img class="img-fluid mx-auto" src="img/137x137/1.png" alt="Image-Descrition">
                                <div class="btn-position btn btn-icon btn-dark rounded-circle d-flex align-items-center justify-content-center height-60 width-60">
                                    <figure id="quote7" class="svg-preloader">
                                        <img class="js-svg-injector" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/quote2.svg" alt="SVG" data-parent="#quote7">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="text-gray-1 font-italic text-lh-inherit px-xl-20 mx-xl-15 px-xl-20 mx-xl-18">Waw. Pemandangan bawah laut sangat bagus sekali..</p>
                            <h6 class="font-size-17 font-weight-bold text-gray-6 mb-0">D Novando</h6>
                            <span class="text-blue-lighter-1 font-size-normal">Wisatawan</span>
                        </div>
                        <!-- End Testimonials -->
                    </div>

                    <div class="js-slide">
                        <!-- Testimonials -->
                        <div class="d-flex justify-content-center mb-6">
                            <div class="position-relative">
                                <img class="img-fluid mx-auto" src="img/137x137/2.png" alt="Image-Descrition">
                                <div class="btn-position btn btn-icon btn-dark rounded-circle d-flex align-items-center justify-content-center height-60 width-60">
                                    <figure id="quote8" class="svg-preloader">
                                        <img class="js-svg-injector" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/quote2.svg" alt="SVG" data-parent="#quote8">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="text-gray-1 font-italic text-lh-inherit px-xl-20 mx-xl-15 px-xl-20 mx-xl-18">Harganya murah, namun kualitas Top..</p>
                            <h6 class="font-size-17 font-weight-bold text-gray-6 mb-0">Y Aditya</h6>
                            <span class="text-blue-lighter-1 font-size-normal">Wisatawan</span>
                        </div>
                        <!-- End Testimonials -->
                    </div>

                    <div class="js-slide">
                        <!-- Testimonials -->
                        <div class="d-flex justify-content-center mb-6">
                            <div class="position-relative">
                                <img class="img-fluid mx-auto" src="img/137x137/3.png" alt="Image-Descrition">
                                <div class="btn-position btn btn-icon btn-dark rounded-circle d-flex align-items-center justify-content-center height-60 width-60">
                                    <figure id="quote9" class="svg-preloader">
                                        <img class="js-svg-injector" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/quote2.svg" alt="SVG" data-parent="#quote9">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="text-gray-1 font-italic text-lh-inherit px-xl-20 mx-xl-15 px-xl-20 mx-xl-18">Pengalaman yang menyenangkan, dapat makan gratis juga. Makanannya enak..</p>
                            <h6 class="font-size-17 font-weight-bold text-gray-6 mb-0">Rizky R</h6>
                            <span class="text-blue-lighter-1 font-size-normal">Wisatawan</span>
                        </div>
                        <!-- End Testimonials -->
                    </div>

                    <div class="js-slide">
                        <!-- Testimonials -->
                        <div class="d-flex justify-content-center mb-6">
                            <div class="position-relative">
                                <img class="img-fluid mx-auto" src="img/137x137/4.png" alt="Image-Descrition">
                                <div class="btn-position btn btn-icon btn-dark rounded-circle d-flex align-items-center justify-content-center height-60 width-60">
                                    <figure id="quote10" class="svg-preloader">
                                        <img class="js-svg-injector" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/quote2.svg" alt="SVG" data-parent="#quote10">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="text-gray-1 font-italic text-lh-inherit px-xl-20 mx-xl-15 px-xl-20 mx-xl-18">Banyak ikan Nemo.., bisa foto dengan mereka juga. Seru..</p>
                            <h6 class="font-size-17 font-weight-bold text-gray-6 mb-0">Taufiq A</h6>
                            <span class="text-blue-lighter-1 font-size-normal">Wisatawan</span>
                        </div>
                        <!-- End Testimonials -->
                    </div>

                    <div class="js-slide">
                        <!-- Testimonials -->
                        <div class="d-flex justify-content-center mb-6">
                            <div class="position-relative">
                                <img class="img-fluid mx-auto" src="img/137x137/5.png" alt="Image-Descrition">
                                <div class="btn-position btn btn-icon btn-dark rounded-circle d-flex align-items-center justify-content-center height-60 width-60">
                                    <figure id="quote11" class="svg-preloader">
                                        <img class="js-svg-injector" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/quote2.svg" alt="SVG" data-parent="#quote11">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="text-gray-1 font-italic text-lh-inherit px-xl-20 mx-xl-15 px-xl-20 mx-xl-18">Pemandangannya dari atas juga bagus, saya pas menikmati pemandangan menggunakan drone..</p>
                            <h6 class="font-size-17 font-weight-bold text-gray-6 mb-0">Rentio A</h6>
                            <span class="text-blue-lighter-1 font-size-normal">Wisatawan</span>
                        </div>
                        <!-- End Testimonials -->
                    </div>

                    <div class="js-slide">
                        <!-- Testimonials -->
                        <div class="d-flex justify-content-center mb-6">
                            <div class="position-relative">
                                <img class="img-fluid mx-auto" src="img/137x137/6.png" alt="Image-Descrition">
                                <div class="btn-position btn btn-icon btn-dark rounded-circle d-flex align-items-center justify-content-center height-60 width-60">
                                    <figure id="quote12" class="svg-preloader">
                                        <img class="js-svg-injector" src="https://madrasthemes.github.io/mytravel-html/assets/svg/illustrations/quote2.svg" alt="SVG" data-parent="#quote12">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="text-gray-1 font-italic text-lh-inherit px-xl-20 mx-xl-15 px-xl-20 mx-xl-18">Pemandunya ramah, dan tempatnya bagus..</p>
                            <h6 class="font-size-17 font-weight-bold text-gray-6 mb-0">Noviko Y</h6>
                            <span class="text-blue-lighter-1 font-size-normal">Wisatawan</span>
                        </div>
                        <!-- End Testimonials -->
                    </div>
                </div>
                <!-- End Slick Carousel -->
            </div>
        </div>
    </div>
    <!-- End Testimonials v2 -->

@endsection