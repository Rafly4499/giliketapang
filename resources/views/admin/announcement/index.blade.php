@extends('layouts.admin') 
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Pengumuman</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Pengumuman</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Pengumuman</h4>
                    
                    <?php
                    if($announcement == NULL){
                    ?>
                        <a href="{{ url('/panel/announcement/create') }}"><button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i>  Tambah Data</button></a><br><br>
                        <h3 class="center">Data Belum Ada</h3>
                    <?php
                    }else{
                    ?>
                    <div class="table-responsive">
                       
                        <table id="zero_config" class="table table-striped table-bordered display no-wrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Pengumuman</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ Str::limit($announcement->name, 30) }}</td>
                                    
                                        <?php
                                        if($announcement->status == 1){
                                        ?>
                                        <td>Aktif</td>
                                    <?php    
                                    }else{
                                        ?>
                                        <td>Tidak Aktif</td>
                                       
                                    <?php
                                    }
                                    ?>
                                    <td>
                                            <a href="{{ url('/panel/announcement/'.$announcement->id.'/edit') }}" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
                                    </td>
                                </tr>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Pengumuman</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
</div>
@endsection
