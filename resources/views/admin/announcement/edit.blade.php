@extends('layouts.admin') 
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Pengumuman</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/panel/announcement') }}" class="text-muted">Pengumuman</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Data Pengumuman</h4>
                    <a href="{{ url('/panel/announcement') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                    <form action="{{ url('/panel/announcement/'.$announcement->id.'/update') }}" id="formevents" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Judul Pengumuman</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="name" value="{{ $announcement->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Deskripsi</label>
                        <div class="col-md-12">
                            <textarea class="form-control" id="summary-ckeditor" rows="3" name="description">{{ $announcement->description }}</textarea>
                        </div>
                    </div>
                    <script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
                    <script>
                        CKEDITOR.replace('summary-ckeditor');

                    </script>

                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Url Pengumuman</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="url" value="{{ $announcement->url }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fullname">Status</label>
                        <select class="form-control select2" name="status" value="{{ $announcement->status }}" id="" required>
                            @if($announcement->status == 1)
                                <option value="1">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            @else
                                <option value="0">Tidak Aktif</option>
                                <option value="1">Aktif</option>
                            @endif
                            </select>
                    </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Cover Pengumuman</label>
                            
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Upload</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input image" id="preview" data-default-file="{{ $announcement->cover_announcement }}">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                </div>
                                
                            </div>
                        </div>
                            <button type="submit" class="btn btn-info">SAVE</button>
                            <a href="{{ url('/panel/announcement') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>

                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection