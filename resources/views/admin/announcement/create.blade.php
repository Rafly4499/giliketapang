@extends('layouts.admin') 
@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Pengumuman</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/panel/announcement') }}" class="text-muted">Pengumuman</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Data</li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tambah Data Pengumuman</h4>
                    <a href="{{ url('/panel/announcement') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                    <form class="form-body" method="post" id="formevents" action="{{ url('/panel/announcement/store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Judul Pengumuman</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Deskripsi Pengumuman</label>
                            <div class="col-md-12">
                                <textarea class="form-control" id="summary-ckeditor" rows="3" name="description" required></textarea>
                            </div>
                        </div>
                        <script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
                        <script>
                            CKEDITOR.replace('summary-ckeditor');

                        </script>               
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Url Pengumuman</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="url">
                            </div>
                        </div>
                            <div class="form-group">
                                <label for="fullname">Status</label>
                                <select class="form-control select2" name="status" id="" required>
                                        <option value="1">Aktif</option>
                                        <option value="0">Tidak Aktif</option>
                                    
                                    </select>
                            </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Cover Pengumuman</label>
                            <div class="col-md-12" style="margin-top: 10px">
                            <div class="form-group">
                                <label for="exampleInputImage">Image:</label>
                                <h6>Gambar tidak boleh lebih dari ukuran 1000×1000 piksel</h6>
                                <input type="file" name="image" id="preview" class="image" required>
                            </div>
                        </div>
                        </div>
                        <button type="submit" class="btn btn-info">SUBMIT</button>
                        <button type="reset" class="btn btn-dark">RESET</button></a>
                        <input type="hidden" name="_method" value="post">
                    </form> 
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    
   
</script>
@endsection