@extends('layouts.admin') 
@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Blog</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.html" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/panel/blog') }}" class="text-muted">Blog</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">View Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">View Data Blog</h4>
                                <a href="{{ url('/panel/blog') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Judul Blog</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$blog->title_blog}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Isi Blog    :</label>
                                                        
                                                        <div class="col-md-12"><p>{!! ($blog->content_blog) !!}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Kategori Blog</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$blog->categoryblog['name_category_blog']}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Link Url(Slug Blog)</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$blog->slug}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Tag</label>
                                                        
                                                        <div class="col-md-8"><p>:
                                                        @foreach($blog->tags as $tag)
                                                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info">{{ $tag->name }}</button>
                                                        @endforeach</p> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Meta Title</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$blog->meta_title}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <label class="col-md-4">Meta Deskripsi    :</label>
                                                        
                                                        <div class="col-md-12"><p>{!! ($blog->meta_description) !!}</p> </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Published On</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$blog->published_at}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Status Draft</label>
                                                        
                                                        <div class="col-md-8"><p>:
                                                            <?php
                                                            if($blog->status_draft == true){
                                                            
                                                            ?>
                                                            Tidak Aktif
                                                            <?php
                                                            }else{
                                                            ?>
                                                            Aktif
                                                            <?php
                                                            }
                                                            ?></p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Postingan</label>
                                                        <div class="col-md-8"><p>:{{$blog->user['name']}}</p> </div>
                                                    </div>
                                                </div>
                                                
                                    
                                   <a href="{{ url('/panel/blog/'.$blog->id.'/edit') }}"><button type="submit" name="submit" class="btn btn-info">UPDATE</button></a> 
                                     
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                            <div class="form-group">
                                        <label for="" class="col-md-6 control-label">Gambar Blog</label>
                                    </div>
                                <div class="col-md-12">
                                <?php
                                    if($blog->photo_blog){
                                ?>
                                <img style="border-radius: 5px; width:100%" src="{{ url('admin/img/'.$blog->photo_blog) }}" alt="">
                                <?php
                                    }else{
                                ?>
                                <h6>Belum Ada Data Gambar</h6>
                                <?php
                                    }
                                ?>
                                </div>
                            
                                    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection