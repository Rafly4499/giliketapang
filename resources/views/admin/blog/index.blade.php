@extends('layouts.admin') 
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Blog</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">List Blog</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">List Blog</h4>
                    <a href="{{ url('/panel/blog/create') }}"><button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i>  Tambah Data</button></a>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered display no-wrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th style="width:100px !important;">Judul Blog</th>
                                    <th>Kategori Blog</th>
                                    <th>Tag Blog</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;?> @foreach($blog as $item)
                                <?php $no++;?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td style="width:100px !important;">{{ Str::limit($item->title_blog, 30) }}</td>
                                    <td>{{ $item->categoryblog['name_category_blog'] }}</td>
                                    <td>
                                        
                                    @foreach($item->tags->take(1) as $tag)
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-info">{{ $tag->name }}</button>
                                    @endforeach
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info btn-sm">+ {{count($item->tags)}}</button>
                                    </td>
                                    <td>
                                        <form action="{{ url('/panel/blog/'.$item->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a href="{{ url('/panel/blog/'.$item->id.'/edit') }}" class="btn btn-success btn-rounded"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="{{ url('/panel/blog/'.$item->id.'/view') }}" class="btn btn-info btn-rounded"><i class="fas fa-eye"></i></a>
                                            <button type="submit" class="btn btn-danger btn-rounded" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>

                                        </form>
                                    </td>
                                </tr>

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                <th>No</th>
                                <th>Judul Blog</th>
                                <th>Kategori Blog</th>
                                <th>Tag Blog</th>
                                <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
</div>
@endsection
