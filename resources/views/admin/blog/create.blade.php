@extends('layouts.admin') 
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Blog</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/panel/blog') }}" class="text-muted">Blog</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Data</li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tambah Data Blog</h4>
                    <a href="{{ url('/panel/blog') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                    <form class="form-body" method="post" id="formblog" action="{{ url('/panel/blog/') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Judul Blog</label>
                            
                                <input type="text" class="form-control" name="title_blog" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Isi Blog</label>
                            
                                <textarea class="form-control" id="summary-ckeditor" rows="3" name="content_blog" required></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fullname">Kategori Blog</label>
                                <select class="form-control select2" name="blog_category_id" id="" required>
                                    @foreach($category as $data)
                                        <option value="{{ $data->id }}">{{ $data->name_category_blog }}</option>
                                    @endforeach
                                    </select>
                            </div>
                        </div>                 
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Tags</label>
                            
                            <input class="form-control" type="text" name="tags" id="tags" data-role="tagsinput" required>
                            
                            <!-- <input class="form-control" data-role="tagsinput" type="text" name="tags" > -->
                            
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Meta Title</label>
                            
                                <input type="text" class="form-control metatitle" name="meta_title" required>
                                <small id="infometatitle" class="badge badge-default badge-warning form-text text-white float-left">Meta Title melebihi 60 karakter</small>
                                <br>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Meta Description</label>
                            
                                <textarea class="form-control metadescription" rows="3" name="meta_description" required></textarea>
                                <small id="infometadesc" class="badge badge-default badge-warning form-text text-white float-left">Meta Description melebihi 160 karakter</small>
                                <br>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Setting Url(Slug Blog)</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio1" name="seturl" class="custom-control-input" value="automaticurl" checked="">
                                <label class="custom-control-label" for="customRadio1">Automatic</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio2" name="seturl" class="custom-control-input" value="settingurl">
                                <label class="custom-control-label" for="customRadio2">Custom</label>
                            </div>
                                <input type="text" class="form-control urlblog" id='urlblog' name="slug">
                        </div>
                    </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fullname">Published On</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio3" name="setpublish" value="automatic" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio3">Automatic</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio4" name="setpublish" value="settingpublish" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio4">Set date and time</label>
                                </div>
                                <div class='input-group date'>
                                    <input type="datetime-local" name="published_at" class="form-control datepicker">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                        <div class="form-group">
                            <label for="" class="control-label">Gambar Blog</label>
                            <div class="col-md-12" style="margin-top: 10px">
                            <div class="form-group">
                                <label for="exampleInputImage">Image:</label>
                                <h6>Gambar tidak boleh lebih dari ukuran 1000×1000 piksel</h6>
                                <input type="file" name="image" id="preview" class="image" required>
                                <input type="hidden" name="x1" value="" />
                                <input type="hidden" name="y1" value="" />
                                <input type="hidden" name="x2" value="" />
                                <input type="hidden" name="y2" value="" />
                                <input type="hidden" name="w" value="" />
                                <input type="hidden" name="h" value="" />
                            </div>
                        </div>
                        </div>
                        <script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
                        <script>
                            CKEDITOR.replace('summary-ckeditor');
                        </script>
                        <div class="col-md-12">
                            <p><img style="max-width:100% !important;" id="previewimage"/></p>
                            <h6>Select Gambar jika ingin memotong gambar</h6>
                            @if(session('path'))
                            
                                <img src="{{ session('path') }}" />
                            @endif
                        </div>
                        <button type="submit" class="btn btn-info">SUBMIT</button>
                        <button type="reset" class="btn btn-dark">RESET</button></a>
                        <input type="hidden" name="_method" value="post">
                    </form> 
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function () {
        $("#infometatitle").css("display", "none");
        $("#infometadesc").css("display", "none");
        $(".datepicker").css("display", "none");
        $(".urlblog").css("display", "none");
        $('.metatitle').bind('input propertychange', function() {
            if (this.value.length > 60) {
                $("#infometatitle").css("display", "block");
            }else{
                $("#infometatitle").css("display", "none");
            }
        });
        $('.metadescription').bind('input propertychange', function() {
            if (this.value.length > 160) {
                $("#infometadesc").css("display", "block");
            }else{
                $("#infometadesc").css("display", "none");
            }
        });
        $("#customRadio4").click(function () {
            $("#customRadio3").prop("checked");
            $(".datepicker").css("display", "inline-block");
        });

        $("#customRadio3").click(function () {
            $("#customRadio4").prop("checked");
            $(".datepicker").css("display", "none");
        });

        $("#customRadio2").click(function () {
            $("#customRadio1").prop("checked");
            $(".urlblog").css("display", "inline-block");
        });

        $("#customRadio1").click(function () {
            $("#customRadio2").prop("checked");
            $(".urlblog").css("display", "none");
        });
    });
  </script>
@endsection