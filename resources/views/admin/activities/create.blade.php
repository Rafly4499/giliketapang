@extends('layouts.admin') 
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Aktivitas</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/panel/activities') }}" class="text-muted">Aktivitas</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Data</li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tambah Data Aktivitas</h4>
                    <a href="{{ url('/panel/activities') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                    <form class="form-body" method="post" action="{{ url('/panel/activities/') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Aktivitas</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="name_activities">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Deskripsi</label>
                            <div class="col-md-12">
                                <textarea class="form-control" id="summary-ckeditor" rows="3" name="description"></textarea>
                            </div>
                        </div>
                        <script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
                        <script>
                            CKEDITOR.replace('summary-ckeditor');
    
                        </script>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Harga</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="harga">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Satuan</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="satuan">
                            </div>
                        </div>
                        <button type="submit" name="submit" class="btn btn-info">SUBMIT</button>
                        <button type="reset" class="btn btn-dark">RESET</button></a>
                        <input type="hidden" name="_method" value="post">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
