@extends('layouts.admin') 
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Blog</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/panel/categoryblog') }}" class="text-muted">Kategori Blog</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Data</li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tambah Data Kategori Blog</h4>
                    <a href="{{ url('/panel/categoryblog') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                    <form class="form-body" method="post" action="{{ url('/panel/categoryblog/') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Kategori Blog</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="name_category_blog">
                            </div>
                        </div>
                        
                        <!-- <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Slug Blog</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="slug">
                            </div>
                        </div> -->
                        <button type="submit" name="submit" class="btn btn-info">SUBMIT</button>
                        <button type="reset" class="btn btn-dark">RESET</button></a>
                        <input type="hidden" name="_method" value="post">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
