@extends('layouts.admin') 
@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Detail Payment</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/panel/booking') }}" class="text-muted">Detail Payment</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">No Booking : {{ $booking->bookingid }}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Detail Konfirmasi Pembayaran</h4>
                                <a href="{{ url('/panel/booking') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                                <div class="table-responsive">
                                    <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>No Rekening</th>
                                                <th>Pemilik Rekening</th>
                                                <th>No Handphone</th>
                                                <th>Tanggal Pembayaran</th>
                                                <th>Total Pembayaran</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 0;?>
                                        @foreach($confirmation as $item)
                                        <?php $no++;?>
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $item->transfer_destination }}</td>
                                            <td>{{ $item->account_owner }}</td>
                                            <td>{{ $item->no_hp }}</td>
                                            <td>{{ $item->date_payment }}</td>
                                            <td>@currency($item->total_payment)</td>
                                        </tr>
                                        @endforeach
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>No Rekening</th>
                                                <th>Pemilik Rekening</th>
                                                <th>No Handphone</th>
                                                <th>Tanggal Pembayaran</th>
                                                <th>Total Pembayaran</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            @endsection