@extends('layouts.admin') 
@section('content')

<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Booking</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">List Booking</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">List Booking</h4>
                                <div class="table-responsive">
                                    <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>BookingID</th>
                                                <th>Nama Pemesan</th>
                                                <th>Status</th>
                                                <th>Tanggal Booking</th>
                                                <th>Total Pembayaran</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 0;?>
                                        @foreach($booking as $item)
                                        <?php $no++;?>
                                        <tr>
                                            <td>{{ $item->bookingid }}</td>
                                            <td>{{ $item->name_order }}</td>
                                            <td>
                                                @if($item->status == 'Sudah Bayar')
                                                    <button class="btn btn-rounded btn-sm btn-success">Sudah Bayar</button>
                                                @elseif($item->status == 'Dibatalkan')
                                                <button class="btn btn-rounded btn-sm btn-danger">Dibatalkan</button>
                                                @elseif($item->status == 'Belum Bayar')
                                                <button class="btn btn-rounded btn-sm btn-warning">Belum Bayar</button>
                                                @elseif($item->status == 'Proses')
                                                <button class="btn btn-rounded btn-sm btn-info">Proses</button>
                                                
                                                @endif
                                            </td>
                                            <td>{{ date('d M Y', strtotime($item->date_booking)) }}</td>
                                            <td>Rp. {{ number_format($item->total_payment, 0) }},-</td>
                                            <td>
                                                    <a href="{{ url('/panel/booking/'.$item->id.'/view') }}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                                    @if($item->status == 'Proses' || $item->status == 'Sudah Bayar')
                                                    <a href="{{ url('/panel/booking/'.$item->id.'/detailpayment') }}" class="btn btn-success btn-sm"><i class="fas fa-shopping-cart"></i></a>
                                                    @endif
                                                    
                                            </td>
                                        </tr>
                                        @endforeach
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>BookingID</th>
                                                <th>Nama Pemesan</th>
                                                <th>Status</th>
                                                <th>Tanggal Booking</th>
                                                <th>Total Pembayaran</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
@endsection