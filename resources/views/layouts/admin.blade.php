<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('admin/assets/images/favicon.png')}}">
    <title>Giliketapang Admin Panel</title>
    <!-- Custom CSS -->
    <link href="{{ asset('admin/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <link href="{{ asset('admin/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{ asset('admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="{{ asset('admin/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <link href="{{ asset('admin/dist/css/style.min.css')}}" rel="stylesheet">
    <link href="{{ asset('admin/css/main.css')}}" rel="stylesheet">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/css/imgareaselect-default.css">
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        @include('inc.admin.header')
        @include('inc.admin.sidebar')
        <div class="page-wrapper">
            @include('inc.admin.flash-message')
            @yield('content')
        </div>
        <footer class="footer text-center text-muted">
            All Rights Reserved by GoSocial.
        </footer>
    </div>
    </div>
    <!-- ============================================================== -->
    <script src="{{ asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('admin/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{ asset('admin/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#js-example-basic-multiple').select2();
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/js/jquery.imgareaselect.min.js"></script>
    <script>
        window.URL = window.URL || window.webkitURL;
        $("#formblog").submit( function( e ) {
        var form = this;
        e.preventDefault(); //Stop the submit for now
                                    //Replace with your selector to find the file input in your form
        var fileInput = $(this).find("#preview")[0],
            file = fileInput.files && fileInput.files[0];

        if( file ) {
            var img = new Image();

            img.src = window.URL.createObjectURL( file );

            img.onload = function() {
                var width = img.naturalWidth,
                    height = img.naturalHeight;

                window.URL.revokeObjectURL( img.src );

                if( width > 1000 && height > 1000 ) {
                    alert('Ukuran Gambar terlalu besar');
                }
                else {
                    form.submit();
                    
                }
            };
        }
        else { //No file was input or browser doesn't support client side reading
            form.submit();
        }

    });
        jQuery(function($) {
            var image = $("#previewimage");
            var originalHeight = image.naturalHeight;
            var originalWidth = image.naturalWidth;

            $("body").on("change", ".image", function(){
 
                var imageReader = new FileReader();
                imageReader.readAsDataURL(document.querySelector(".image").files[0]);
 
                imageReader.onload = function (oFREvent) {
                    image.attr('src', oFREvent.target.result).fadeIn();
                };
            });

            console.log('IMG width: ' + originalWidth + ', height: ' + originalHeight);
            $('#previewimage').imgAreaSelect({
                aspectRatio: '16:9',
                handles: true,
                fadeSpeed: 200,
                imageHeight: originalHeight, 
                imageWidth: originalWidth,
                onSelectChange: getCoordinates
                });
            function getCoordinates(img, selection) {

                if (!selection.width || !selection.height){
                return;
                }
                var porcX = img.naturalWidth / img.width;
                var porcY = img.naturalHeight / img.height;
                $('input[name="x1"]').val(Math.round(selection.x1 * porcX));
                $('input[name="y1"]').val(Math.round(selection.y1 * porcY));
                $('input[name="x2"]').val(Math.round(selection.x2 * porcX));
                $('input[name="y2"]').val(Math.round(selection.y2 * porcY));
                $('input[name="w"]').val(Math.round(selection.width * porcX));
                $('input[name="h"]').val(Math.round(selection.height * porcY));
            }
        });
    </script>
    
    <script src="{{ asset('admin/dist/js/app-style-switcher.js')}}"></script>
    <script src="{{ asset('admin/dist/js/feather.min.js')}}"></script>
    <script src="{{ asset('admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{ asset('admin/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('admin/dist/js/custom.min.js')}}"></script>
    <!--This page JavaScript -->
    <script src="{{ asset('admin/assets/extra-libs/c3/d3.min.js')}}"></script>
    <script src="{{ asset('admin/assets/extra-libs/c3/c3.min.js')}}"></script>
    <script src="{{ asset('admin/assets/libs/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{ asset('admin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <script src="{{ asset('admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{ asset('admin/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{ asset('admin/dist/js/pages/dashboards/dashboard1.min.js')}}"></script>
    <script src="{{ asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
</body>

</html>