<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Title -->
    <!-- Primary Meta Tags -->
    <title>Gili Ketapang Trip - Paket Wisata Ke Gili Ketapang, Probolinggo</title>
    <meta name="title" content="Gili Ketapang Trip - Paket Wisata Ke Gili Ketapang, Probolinggo">
    <meta name="description" content="Gili Ketapang dikenal sebagai pulau dengan pesona bawah lautnya. Wisatawan dapat melakukan Snorkeling, Mancing, dan mengelilingi banyak spot foto menarik dengan perahu">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset ('/img/logo/logo-white.png')}}">

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900&amp;display=swap" rel="stylesheet">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/font-mytravel.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/hs-megamenu/src/hs.megamenu.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/flatpickr/dist/flatpickr.min.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/slick-carousel/slick/slick.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/dzsparallaxer/dzsparallaxer.css')}}">

    <!-- CSS MyTravel Template -->
    <link rel="stylesheet" href="{{ asset('css/theme.css')}}">

    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">


    <!-- JS Global Compulsory -->
    <script src="{{asset ('vendor/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset ('vendor/jquery-migrate/dist/jquery-migrate.min.js')}}"></script>
    <script src="{{asset ('vendor/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset ('vendor/bootstrap/bootstrap.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-84936033-6"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-84936033-6');
    </script>


    <!-- JS Implementing Plugins -->
    <script src="https://kit.fontawesome.com/bd4b393834.js" crossorigin="anonymous"></script>
    <script src="{{asset ('vendor/hs-megamenu/src/hs.megamenu.js')}}"></script>
    <script src="{{asset ('vendor/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset ('vendor/flatpickr/dist/flatpickr.min.js')}}"></script>
    <script src="{{asset ('vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset ('vendor/slick-carousel/slick/slick.js')}}"></script>
    <script src="{{asset ('vendor/svg-injector/dist/svg-injector.min.js')}}"></script>
    <script src="{{asset ('vendor/appear.js')}}"></script>
    <script src="{{asset ('vendor/gmaps/gmaps.min.js')}}"></script>
    <script src="{{asset ('vendor/fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{asset ('vendor/appear.js')}}"></script>
    <script src="{{asset ('vendor/player.js/dist/player.min.js')}}"></script>

    <!-- JS MyTravel -->
    <script src="{{asset ('js/hs.core.js')}}"></script>
    <script src="{{asset ('js/components/hs.header.js')}}"></script>
    <script src="{{asset ('js/components/hs.unfold.js')}}"></script>
    <script src="{{asset ('js/components/hs.validation.js')}}"></script>
    <script src="{{asset ('js/components/hs.show-animation.js')}}"></script>
    <script src="{{asset ('js/components/hs.range-datepicker.js')}}"></script>
    <script src="{{asset ('js/components/hs.selectpicker.js')}}"></script>
    <script src="{{asset ('js/components/hs.slick-carousel.js')}}"></script>
    <script src="{{asset ('vendor/dzsparallaxer/dzsparallaxer.js')}}"></script>
    <script src="{{asset ('js/components/hs.svg-injector.js')}}"></script>
    <script src="{{asset ('js/components/hs.quantity-counter.js')}}"></script>
    <script src="{{asset ('js/components/hs.counter.js')}}"></script>
    <script src="{{asset ('js/components/hs.g-map.js')}}"></script>
    <script src="{{asset ('js/components/hs.fancybox.js')}}"></script>
    <script src="{{asset ('js/components/hs.scroll-nav.js')}}"></script>
    <script src="{{asset ('js/components/hs.sticky-block.js')}}"></script>
    <script src="{{asset ('js/components/hs.video-player.js')}}"></script>
    
    <!-- JS Booking WA Elfsight
    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-424c1b8d-8b03-4122-a86a-e1f86c6da848"></div>
-->

    <!-- JS Plugins Init. -->
    <script>
        $(window).on('load', function () {
            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 1199.98,
                hideTimeOut: 0
            });

            // initialization of svg injector module
            $.HSCore.components.HSSVGIngector.init('.js-svg-injector');

            // Page preloader
            setTimeout(function() {
              $('#jsPreloader').fadeOut(500)
            }, 800);
        });

        $(document).on('ready', function () {
            // initialization of header
            $.HSCore.components.HSHeader.init($('#header'));

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

            // initialization of show animations
            $.HSCore.components.HSShowAnimation.init('.js-animation-link');

            // initialization of datepicker
            $.HSCore.components.HSRangeDatepicker.init('.js-range-datepicker');

            // initialization of select
            $.HSCore.components.HSSelectPicker.init('.js-select');

            // initialization of quantity counter
            $.HSCore.components.HSQantityCounter.init('.js-quantity');

            // initialization of slick carousel
            $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

            // initialization of counters
            var counters = $.HSCore.components.HSCounter.init('[class*="js-counter"]');
            // button scroll up
            buttonScroll = document.getElementById("btnScrollup");

            
            window.onscroll = function() {scrollFunction()};

            function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                buttonScroll.style.display = "block";
            } else {
                buttonScroll.style.display = "none";
            }
            }
            
            $.HSCore.components.HSVideoPlayer.init('.js-inline-video-player');
        });
    </script>
</head>
<body>
<div>
  @include('inc.navbar')
  @yield('content')
  @include('inc.footer')
</div>
</body>
</html>
