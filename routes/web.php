<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/activities', 'PagesController@services');
Route::get('/about', 'PagesController@about');
Route::get('/galery', 'PagesController@galery');
Route::get('/blog', 'PagesController@blog')->name('blog');
Route::get( '/blog/{slug}', 'PagesController@detailBlog');
Route::get( '/blog/category/{slug}', 'PagesController@categoryBlog' )->name('categoryblog');
Route::get( '/blog/tag/{slug}', 'PagesController@tagBlog' )->name('tagblog');
Route::get( '/searchblog', 'PagesController@searchBlog' )->name('searchblog');
Route::get('/activities/snorkeling', 'ServicesController@snorkeling');
Route::get('/activities/drone', 'ServicesController@drone');
Route::get('/activities/banana-boat', 'ServicesController@bananaboat');
Route::get('/activities/gua-kucing', 'ServicesController@guakucing');
Route::get('/activities/pemancingan', 'ServicesController@pemancingan');
Route::get('/customer/login', 'CustomerController@showLoginForm')->name('customer.loginform');
Route::get('/customer/register', 'CustomerController@showRegisterForm')->name('customer.registerform');
Route::post('/customer/login', 'CustomerController@login')->name('customer.login');
Route::post('/customer/register', 'CustomerController@register')->name('customer.register');
Route::get('/check-order', 'BookingController@checkOrder')->name('check_order');
Route::post('/result-order', 'BookingController@resultOrder')->name('result_order');
Route::get('/booking', 'BookingController@createStep1')->name('booking.step.one');
    Route::post('/booking-step1', 'BookingController@PostcreateStep1')->name('booking.step.one.post');
    Route::get('/booking-step2', 'BookingController@createStep2')->name('booking.step.two');
    Route::post('/booking-step2', 'BookingController@PostcreateStep2')->name('booking.step.two.post');
    Route::get('/booking-step3', 'BookingController@createStep3')->name('booking.step.three');
    Route::post('/booking-store', 'BookingController@store')->name('booking.step.three.post');
    Route::get('/booking-review', 'BookingController@index');
    Route::get('/confirmation-payment/{id}', 'BookingController@viewconfirmationPayment');
    Route::post('/confirmation-payment', 'BookingController@PostConfirmationPayment');
    Route::get('/booking/invoice/{id}', 'BookingController@viewinvoice');
Route::group(['prefix' => '/customer', 'middleware' => ['auth:customer']], function() {
    Route::get('/', 'CustomerController@index')->middleware('auth:customer');
    Route::get('/logout', 'CustomerController@logout')->name('customer.logout');
    
});
Auth::routes();
Route::group(['prefix' => '/panel', 'middleware' => ['auth']], function() {
    Route::get('/', array('as' => 'admin.dashboard.index', 'uses' => 'Admin\DashboardController@index'));
    Route::get('/announcement', 'Admin\AnnouncementController@index')->name('announcement');
    Route::get('/announcement/{id}/edit', 'Admin\AnnouncementController@edit_announcement');
    Route::put('/announcement/{id}/update', 'Admin\AnnouncementController@update_announcement');
    Route::get('/announcement/create', 'Admin\AnnouncementController@create_announcement');
    Route::post('/announcement/store', 'Admin\AnnouncementController@store_announcement');
    Route::post('/announcement/store', 'Admin\AnnouncementController@store_announcement');
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');
    Route::get('/booking', 'Admin\BookingController@index')->name('booking');
    Route::get('/booking/{id}/view', 'Admin\BookingController@view');
    Route::get('/booking/{id}/detailpayment', 'Admin\BookingController@detailpayment');
    Route::get('/booking/viewpayment/{id}', 'Admin\BookingController@viewpayment');
    Route::get('/booking/approval/{id}', 'Admin\BookingController@approval');
    Route::get('/booking/rejected/{id}', 'Admin\BookingController@rejected');
    Route::get('/customer', 'Admin\CustomerController@index')->name('customer');
    Route::get('/contact', 'Admin\ContactController@index')->name('contact');
    //Blog
    Route::resource('/activities', 'Admin\ActivitiesController');
    Route::get('/activities', 'Admin\ActivitiesController@index');
    Route::get('/activities/{id}/view', 'Admin\ActivitiesController@view');
    //Blog
    Route::resource('/blog', 'Admin\BlogController');
    Route::get('/blog', 'Admin\BlogController@index');
    Route::get('/blog/{id}/view', 'Admin\BlogController@view');
    //Category Blog
    Route::resource('/categoryblog', 'Admin\BlogCategoryController');
    Route::get('/categoryblog', 'Admin\BlogCategoryController@index');
});
Route::get('/home', 'HomeController@index')->name('home');
