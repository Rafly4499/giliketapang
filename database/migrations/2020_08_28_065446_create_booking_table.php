<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->id();
            $table->string('bookingid');
            $table->string('name_order');
            $table->string('city')->nullable();
            $table->string('method_payment')->nullable();
            $table->string('type_payment_bank')->nullable();
            $table->string('status')->nullable(); 
            $table->integer('number_participant')->nullable();
            $table->integer('payment')->nullable();
            $table->string('unit')->nullable();
            $table->integer('total_payment')->nullable();
            $table->dateTime('date_booking');
            $table->unsignedBigInteger('activities_id')->nullable();
            $table->foreign('activities_id')->references('id')->on('activities')->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
