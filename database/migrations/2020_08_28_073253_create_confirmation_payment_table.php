<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfirmationPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirmation_payment', function (Blueprint $table) {
            $table->id();
            $table->string('invoiceid');
            $table->string('transfer_destination');
            $table->string('name_order');
            $table->string('account_owner')->nullable();
            $table->dateTime('date_payment')->nullable();
            $table->string('total_payment')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('image_confirmation')->nullable();
            $table->unsignedBigInteger('booking_id')->nullable();
            $table->foreign('booking_id')->references('id')->on('booking');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confirmation_payment');
    }
}
