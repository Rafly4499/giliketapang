<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Announcement;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('inc.navbar', function($view) {
            $view->with('announcement', Announcement::first());
         });
    }
}
