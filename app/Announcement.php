<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $table = "announcement";
    protected $guarded = [];
    protected $fillable= [
        'name', 'description', 'cover_announcement', 'status', 'url', 'slug' 
    ];
}
