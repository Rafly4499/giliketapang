<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfirmationPayment extends Model
{
    protected $table = "confirmation_payment";
    protected $guarded = [];
    protected $fillable= [
        'transfer_destination', 'name_order', 'account_owner', 'date_payment', 'total_payment', 'np_hp', 'image_confirmation', 'booking_id'
    ];
    public function booking(){
        return $this->belongsTo(Booking::class,'booking_id');
    }
}
