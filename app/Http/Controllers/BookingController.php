<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activities;
use App\Booking;
use App\ConfirmationPayment;
use DateTime;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Image;
use File;
use App\Mail\BookingMail;
use Illuminate\Support\Facades\Mail;
class BookingController extends Controller
{
    protected $guarded = [];

    public function index(Request $request)
    {
        $request->session()->forget('booking');

        $booking = \App\Booking::all();

        return view('pages.booking.index',compact('booking'));
    }

    public function createStep1(Request $request)
    {
        $booking = $request->session()->get('booking');
        $activities = Activities::all();
        return view('pages.booking.step1',compact('booking','activities'));
    }

    public function PostcreateStep1(Request $request)
    {
        $priceActivities = Activities::where('id',$request->activities_id)->first();
        $total_payment = $request->number_participant * $priceActivities->harga;
        if(empty($request->session()->get('booking'))){
            $booking = new \App\Booking();
            $booking->activities_id = $request->activities_id;
            $booking->name_order = $request->name_order;
            $booking->email = $request->email;
            $booking->city = $request->city;
            $booking->number_participant = $request->number_participant;
            $booking->date_booking = $request->date_booking;
            $booking->payment = $priceActivities->harga;
            $booking->no_hp = $request->no_hp;
            $booking->total_payment = $total_payment;
            session()->put('booking', $booking);
        }else{
            $booking = $request->session()->get('booking');
            $booking->activities_id = $request->activities_id;
            $booking->name_order = $request->name_order;
            $booking->email = $request->email;
            $booking->city = $request->city;
            $booking->number_participant = $request->number_participant;
            $booking->date_booking = $request->date_booking;
            $booking->payment = $priceActivities->harga;
            $booking->no_hp = $request->no_hp;
            $booking->total_payment = $total_payment;
            session()->put('booking', $booking);
        }
        return redirect()->route('booking.step.two');
    }

    public function createStep2(Request $request)
    {
        $booking = $request->session()->get('booking');

        return view('pages.booking.step2',compact('booking'));
    }

    public function PostcreateStep2(Request $request)
    {
        $booking = $request->session()->get('booking');
        $booking->method_payment = $request->method_payment;
        $booking->type_payment_bank = $request->type_payment_bank;
        session()->put('booking', $booking);
        return redirect()->route('booking.step.three');
    }
    
    public function createStep3(Request $request)
    {  
        $booking = $request->session()->get('booking');
        return view('pages.booking.step3',compact('booking'));
    }

    public function store(Request $request)
    {
        // $this->validate($request,[
        //     'g-recaptcha-response' => 'required|captcha'
        // ]);
        $lastdata = Booking::latest('id')->first();
        if($lastdata == NULL){
            $nextNoUrut = 1;    
        }else{
            $nextNoUrut = $lastdata->id + 1;
        }
        $today = date("Ymd");
        $specificToday = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
        $nextNoTransaksi = $today.sprintf('%04s', $nextNoUrut);
        $booking = $request->session()->get('booking');
        $booking->bookingid = $nextNoTransaksi;
        $booking->status = 'Belum Bayar';
        $booking->unit = 'orang';
        $booking->save();
        try{
            Mail::to($booking->email)->send(new BookingMail( $booking ));
            return redirect('/booking/invoice/'.$booking->id)->with('success', 'Anda berhasil melakukan booking. Silahkan cek email anda dan segera melakukan pembayaran');
            $request->session()->forget('booking');
        }catch (Exception $e){
            return response (['status' => false,'errors' => $e->getMessage()]);
            $request->session()->forget('booking');
        }
        $request->session()->forget('booking');
        return redirect('/booking/invoice/'.$booking->id)->with('success', 'Anda berhasil melakukan booking. Silahkan cek email anda dan segera melakukan pembayaran');
    }
    public function viewconfirmationPayment($id)
    {
        $booking = Booking::where('id',$id)->first();
        return view('pages.booking.confirmationpayment',compact('booking'));
    }
    public function viewinvoice($id)
    {
        $booking = Booking::where('id',$id)->first();
        $confirmation = ConfirmationPayment::where('booking_id',$id)->first();
        return view('pages.booking.invoice',compact('confirmation','booking'));
    }
    public function PostConfirmationPayment(Request $request)
    {
        // $this->validate($request,[
        //     'g-recaptcha-response' => 'required|captcha'
        //     ]);
        
        $booking = Booking::where('bookingid','LIKE','%'.request('booking_id').'%')->first();
        
        $data = new ConfirmationPayment;
        $data->transfer_destination = $request->transfer_destination;
        $data->account_owner = $request->account_owner;
        $data->date_payment = $request->date_payment;
        $data->total_payment = $request->total_payment;
        $data->no_hp = $request->no_hp;
        $data->booking_id = $booking->id;
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'admin/img/konfirmasi_pembayaran/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
                })->save($destinationPath.$fileName);
                $data['image_confirmation'] = $fileName;
            }
          }
        $confirm = $data->save();
        if ($confirm) {
            $result = Booking::where('id', $booking->id)->update([
            'status' => "Proses",
            ]);
        }
        return redirect('/check-order')->with('success', 'Anda berhasil mengonfirmasi pembayaran. Silahkan menunggu konfirmasi dari admin');
    }
    public function checkOrder()
    {
        $title = 'Cek Order Booking';
        $label = 'Cek Order Booking';
        return view('pages.booking.checkorder', compact('title', 'label'));
    }
    
    public function resultOrder(Request $request)
    {
        $title = 'Cek Order Booking';
        $label = 'Cek Order Booking';
        $nobooking = $request->nobooking;
        $nohp = $request->nohp;
        $result = Booking::where('bookingid', 'like', "%".$nobooking."%")->where('no_hp', 'like', "%".$nohp."%")->first();
        return view('pages.booking.resultorder', compact('result','title', 'label'));
    }
}
