<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use App\Blog;
use App\BlogCategory;
use App\User;
class PagesController extends Controller
{
    public function index()
    {
        $title = 'Open Trip No 1 - Wisata Gili Ketapang';
        $label = 'Index';
        $announcement = Announcement::first();
        return view('pages.index',compact('label','title','announcement'));
    }

    public function services()
    {
        $title = 'Layanan Kami - Wisata Gili Ketapang';
        $label = 'Layanan Kami';
        return view('pages.services')->with('title', $title)->with('label', $label);
    }

    public function about()
    {
        $title = 'Tentang Kami - Wisata Gili Ketapang';
        $label = 'Tentang Kami';
        return view('pages.about')->with('title', $title)->with('label', $label);
    }

    public function galery()
    {
        $title = 'Galeri Kami - Wisata Gili Ketapang';
        $label = 'Galeri';
        return view('pages.galery')->with('title', $title)->with('label', $label);
    }

        // Blog -------------------------------------------------------------------------------------------//

        public function blog(){
            $title = 'Blog - Wisata Gili Ketapang';
            $label = 'Blog';
            $blogtag = Blog::all();
            $blog = Blog::where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->paginate(4);
            $lastblog = Blog::where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
            $category = BlogCategory::all();
            return view('pages.blog.index',compact('blog','category','lastblog','label','title', 'blogtag'));        
        }
        public function categoryBlog($slug){
            if($slug){
                $category = BlogCategory::where('slug', $slug)->first();
                $title = 'Kategori Blog - Gili Ketapang Trip';
                $label = 'Kategori Blog';
                if(!$category){
                    abort(404);
                }
            }
            $lastblog = Blog::where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
           
            $allcategory = BlogCategory::all();
            $blog = Blog::where('blog_category_id','=',$category->id)->where('published_at','<=',date('Y-m-d H:m',time()))->paginate(4);
            return view('pages.blog.categoryblog',compact('category','blog','lastblog','allcategory','label','title'));
            
        }
        public function detailBlog($slug){
            if($slug){
                $blog = Blog::where('slug', $slug)->first();
                $title = $blog->title_blog.'- Gili Ketapang Trip';
                $label = $blog->title_blog;
                if(!$blog){
                    abort(404);
                }
            }
            
            $category = BlogCategory::all();
            $lastblog = Blog::where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
            $blogTerkait = Blog::where('blog_category_id', $blog->blog_category_id)->orWhere('tags','like',"%".$blog->tags."%")->orderBy('id','desc')->take(3)->get();
            return view('pages.blog.detailblog',compact('blog','category','lastblog','blogTerkait','label','title'));
                            
        }
        public function searchBlog(Request $request)
        {
            $title = 'Pencarian Blog - Gili Ketapang Trip';
            $label = 'Pencarian Blog';
            $lastblog = Blog::where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
            $category = BlogCategory::all();
            $search = $request->search;
            $blog = Blog::where('title_blog','like',"%".$search."%")->where('published_at','<=',date('Y-m-d H:m',time()))->paginate(4);
            return view('pages.blog.searchblog',compact('blog','category','lastblog','search','label','title'));
        }
        public function tagBlog($slug){
            if($slug){
                $tag = Blog::Where('tags', 'like', '%' . $slug . '%')->first();
                $blog = Blog::Where('tags', 'like', '%' . $slug . '%')->paginate(4);
                $title = 'Tag Blog - Gili Ketapang Trip';
                $label = 'Tag Blog';
                if(!$blog){
                    abort(404);
                }
            }
            $lastblog = Blog::where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
            $allcategory = BlogCategory::all();
            
            return view('pages.blog.tagblog',compact('tag','blog','lastblog','allcategory','label','title'));
            
        }
}
