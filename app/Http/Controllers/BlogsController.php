<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogsController extends Controller
{
    public function tips_liburan_seru()
    {
        $title = 'Tips Liburan Seru - Gili Ketapang Trip';
        $label = 'Tips Liburan Seru';
        return view('pages.blog.blog1')->with('title', $title)->with('label', $label);
    }

    public function cara_snorkeling_aman()
    {
        $title = 'Cara Snorkeling Aman - Gili Ketapang Trip';
        $label = 'Cara Snorkeling Aman';
        return view('pages.blog.blog2')->with('title', $title)->with('label', $label);
    }

    public function tips_liburan_murah()
    {
        $title = 'Tips Liburan Murah - Gili Ketapang Trip';
        $label = 'Tips Liburan Murah';
        return view('pages.blog.blog3')->with('title', $title)->with('label', $label);
    }

    public function tips_menabung_untuk_liburan()
    {
        $title = 'Tips Menabung Untuk Liburan - Gili Ketapang Trip';
        $label = 'Tips Menabung Untuk Liburan';
        return view('pages.blog.blog4')->with('title', $title)->with('label', $label);
    }
}
