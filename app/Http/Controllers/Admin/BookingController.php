<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Booking;
use App\ConfirmationPayment;

class BookingController extends Controller
{
    public function index() {
        $booking = Booking::all();
		return view('admin.booking.index')->with('booking',$booking);
    }
    public function view($id)
    {
        $booking = Booking::find($id);
		return view('admin.booking.view')->with('booking',$booking);
    }
    public function approval($id)
    {
        $booking = Booking::where('id', $id)->update([
        'status' => "Sudah Bayar"
        ]);
        if ($booking) {
            Session::flash('info', 'Booking updated successfully');
            return redirect('panel/booking');
        }
    }
    public function rejected($id)
    {
        $booking = Booking::where('id', $id)->update([
        'status' => "Konfirmasi ulang"
        ]);
        if ($booking) {
            Session::flash('info', 'Booking updated successfully');
            return redirect('panel/booking');
        }
    }   
    public function detailpayment($id)
    {
        $booking = Booking::find($id);
        $confirmation = ConfirmationPayment::where('booking_id',$id)->first();
		return view('admin.booking.detailpayment', compact('booking','confirmation'));
    }
    public function viewpayment($id)
    {
        $confirmation = ConfirmationPayment::find($id);
		return view('admin.booking.viewpayment')->with('confirmation',$confirmation);
    }
    
}
