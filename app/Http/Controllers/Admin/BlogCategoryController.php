<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Blog;
use App\BlogCategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = BlogCategory::all();
        return view('admin.categoryblog.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = BlogCategory::orderBy('id')->get();
        $category = BlogCategory::all();
        return view('admin.categoryblog.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->get('name_category_blog'));
	    BlogCategory::create($data);
	    Session::flash('message', $data['name_category_blog'] . ' added successfully');
	    return redirect('/panel/categoryblog');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BlogCategory $blogCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = BlogCategory::find($id);
        return view('admin.categoryblog.edit', ['category' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = BlogCategory::find($id);
        $category = $request->all();
        $category['slug'] = Str::slug($category['name_category_blog']);
        $data->update($category);

	    Session::flash('success', $data['name_category_blog'] . ' updated successfully');
        return redirect('/panel/categoryblog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = BlogCategory::find($id);
	    $data->destroy($id);

	    Session::flash('success', $data['name_category_blog'] . ' deleted successfully');
	    return redirect('/panel/categoryblog');
    }
}
