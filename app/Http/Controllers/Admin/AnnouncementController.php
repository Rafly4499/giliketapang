<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Announcement;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use File;
class AnnouncementController extends Controller
{
    public function index() {
		$announcement = Announcement::first();
		return view('admin.announcement.index', compact('announcement'));
	}
	public function edit_announcement($id)
    {
        $announcement = Announcement::find($id);
        return view('admin.announcement.edit', compact('announcement'));
    }
    public function update_announcement(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit', 'x1', 'y1', 'x2', 'y2', 'w', 'h');
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'admin/img/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
                })->save($destinationPath.$fileName);
            $req['cover_announcement'] = $fileName;
            unset($req['image']);
    
              $result = Announcement::find($id);
              if (!empty($result->image)) {
                File::delete('admin/img/'.$result->image);
              }
            }else {
              unset($req['image']);
            }
          }else {
            unset($req['image']);
          }
          $req['slug'] = Str::slug($req['name']);
          $data = Announcement::where('id', $id)->update($req);
          
	    Session::flash('success', $data['name'] . ' updated successfully');
        return redirect('panel/announcement');
    }
    public function create_announcement()
    {
        return view('admin.announcement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_announcement(Request $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->get('name'));
      
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'admin/img/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
                })->save($destinationPath.$fileName);
                $data['cover_announcement'] = $fileName;
                unset($data['image']);
            }
          }

    	$announcement = Announcement::create($data);
        Session::flash('success', $data['name'] . ' added successfully');
        return redirect('/panel/announcement');
    }
}
