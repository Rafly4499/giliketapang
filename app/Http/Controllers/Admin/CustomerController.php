<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;
use App\User;
class CustomerController extends Controller
{
	public function index()
    {
        $customer = Customer::all();
        $user = User::all();
        return view('admin.customer.index', compact('customer','user'));
    }
    public function view($id)
    {
        $customer = Customer::find($id);
        return view('admin.customer.view', compact('customer'));
    }
}
