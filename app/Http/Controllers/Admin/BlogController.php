<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Blog;
use App\BlogCategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Image;
use File;
use Auth;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::all();
        return view('admin.blog.index', compact('blog'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Blog::orderBy('id')->get();
        $blog = Blog::all();
        $category = BlogCategory::all();
        return view('admin.blog.create', compact('blog','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // var_dump($data);die();
        $data['slug'] = Str::slug($request->get('slug'));
        $tags = explode(",", $request->tags);
        
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'admin/img/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                $img = Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                      $constraint->aspectRatio();
                      $constraint->upsize();
                  });
                if ($request->input('w') || $request->input('h')) {
                    $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'), $request->input('x2'), $request->input('y2'));
                    $img->save($destinationPath.$fileName);
                }else{
                    $img->save($destinationPath.$fileName);
                }
                $data['photo_blog'] = $fileName;
                unset($data['image']);
            }
          }
          if($request->seturl == 'automaticurl'){
            $data['slug'] = Str::slug($request->get('title_blog'));
          }else{
            $data['slug'] = Str::slug($request->get('slug'));
          }
        if($request->setpublish == 'automatic'){
            $data['published_at'] = date('Y-m-d H:i:s');
        }else{
          $data['published_at'] = $request->published_at;
        }
        if ($request->input('status_draft') == 'true') {
          $data['status_draft'] = 'true';
        }else{
          $data['status_draft'] = 'false';
        }
    	  $blog = Blog::create($data);
        $blog->tag($tags);
        Session::flash('success', $data['title_blog'] . ' added successfully');
        return redirect('/panel/blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        $category = BlogCategory::all();
        return view('admin.blog.edit', compact('blog','category'));
    }
    public function view($id)
    {
        $blog = Blog::find($id);
        $category = BlogCategory::all();
        return view('admin.blog.view', compact('blog','category'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit', 'x1', 'y1', 'x2', 'y2', 'w', 'h', 'seturl', 'setpublish');
        $blog = Blog::where('id', $id)->firstorfail();
        $blog->title_blog = $request->title_blog;
        $blog->content_blog = $request->content_blog;
        $blog->blog_category_id = $request->blog_category_id;
        $blog->user_id = $request->user_id;
        $blog->meta_title = $request->meta_title;
        $blog->meta_description = $request->meta_description;
      
        $tags = explode(",", $req['tags']);

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'admin/img/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            $img = Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              });
              if ($request->input('w') || $request->input('h')) {
                $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'), $request->input('x2'), $request->input('y2'));
                $img->save($destinationPath.$fileName);
            }else{
                $img->save($destinationPath.$fileName);
            }
            $blog->photo_blog = $fileName;
            unset($req['image']);
    
              $result = Blog::find($id);
              if (!empty($result->photo_blog)) {
                File::delete('admin/img/'.$result->photo_blog);
              }
            }else {
              unset($req['image']);
            }
          }else {
            unset($req['image']);
          }
          if($request->seturl == 'automaticurl'){
            $blog->slug = Str::slug($request->get('title_blog'));
          }else{
            $blog->slug = Str::slug($request->get('slug'));
          }
          if($request->setpublish == 'automatic'){
            $blog->published_at = date('Y-m-d H:i:s');
          }elseif($request->setpublish == null){
            $blog->published_at = date('Y-m-d H:i:s');
          }else{
            $blog->published_at = $request->published_at;
          }
          if ($request->input('status_draft') == 'true') {
            $blog->status_draft = 'true';
          }else{
            $blog->status_draft = 'false';
          }
          $blog->tags = $request->tags;
          $blog->save();
          $blog->retag($tags);
	    Session::flash('success', $blog['title_blog'] . ' updated successfully');
        return redirect('panel/blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Blog::find($id);
	    $data->destroy($id);

	    Session::flash('success', $data['title_blog'] . ' deleted successfully');
	    return redirect('panel/blog');
    }
}