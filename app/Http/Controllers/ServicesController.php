<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function snorkeling()
    {
        $title = 'Snorkeling - Gili Ketapang Trip';
        $label = 'Snorkeling';
        return view('pages.layanan.snorkeling')->with('title', $title)->with('label', $label);
    }

    public function drone()
    {
        $title = 'Drone Video - Gili Ketapang Trip';
        $label = 'Drone Video';
        return view('pages.layanan.drone')->with('title', $title)->with('label', $label);
    }

    public function guakucing()
    {
        $title = 'Gua Kucing - Gili Ketapang Trip';
        $label = 'Gua Kucing';
        return view('pages.layanan.guakucing')->with('title', $title)->with('label', $label);
    }

    public function bananaboat()
    {
        $title = 'Banana Boat - Gili Ketapang Trip';
        $label = 'Banana Boat';
        return view('pages.layanan.bananaboat')->with('title', $title)->with('label', $label);
    }
    
    public function pemancingan()
    {
        $title = 'Wisata Mancing - Gili Ketapang Trip';
        $label = 'Wisata Mancing';
        return view('pages.layanan.pemancingan')->with('title', $title)->with('label', $label);
    }
}
