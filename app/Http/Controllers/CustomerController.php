<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Booking;
class CustomerController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/customer';

public function __construct()
{
    $this->middleware('guest:customer')->except('logout')->except('index');
}

public function index(){
    $title = 'Account Detail';
    $label = 'Account Detail';
    $id_user = Auth::guard('customer')->user()->id;
    $booking = Booking::where('customer_id',$id_user)->get();
    return view('customer.account',compact('label','title','booking'));
}

public function showLoginForm()
{
      return view('customer.auth.login');
}

public function showRegisterForm()
{
      return view('customer.auth.register');
}

public function username()
{
        return 'username';
}

protected function guard()
{
      return Auth::guard('customer');
}

public function register(Request $request)
{
      $request->validate([
          'username'      => 'required|string|unique:customers',
          'fullname'      => 'required|string',
          'email'         => 'required|string|email|unique:customers',
          'no_hp'         => 'required',
          'password'      => 'required|string|min:6|confirmed'
      ]);
      \App\Customer::create($request->all());
      return redirect('/customer/login')->with('success', 'Successfully register!');
}
public function login(Request $request)
{   
    $input = $request->all();
    $this->validate($request, [
        'email' => 'required|email',
        'password' => 'required',
    ]);

    if(Auth::guard('customer')->attempt(array('email' => $input['email'], 'password' => $input['password'])))
    {
        return redirect('/customer');
    }else{
        return redirect()->route('customer.loginform')
            ->with('error','Email dan password anda salah');
    }
      
}
}
