<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Blog extends Model
{
    protected $table = "blog";
    protected $guarded = [];
    protected $fillable= [
        'title_blog', 'content_blog', 'photo_blog', 'slug', 'tags', 'blog_category_id', 'user_id', 'meta_title', 'meta_description', 'published_at', 'status_draft'
    ];
    use \Conner\Tagging\Taggable;

    public function categoryblog(){
        return $this->belongsTo(BlogCategory::class,'blog_category_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
