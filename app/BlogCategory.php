<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{

    protected $table = "blog_category";

    protected $guarded = [];
    protected $fillable= [
        'name_category_blog', 'slug',
    ];
    public function blog(){
        return $this->hasMany(Blog::class, 'blog_category_id');
    }
}
