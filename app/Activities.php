<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activities extends Model
{
    protected $table = "activities";

    protected $guarded = [];
    protected $fillable= [
        'name_activities', 'description','harga', 'satuan',
    ];
    public function booking(){
        return $this->hasMany(Booking::class, 'booking_id');
    }
}
