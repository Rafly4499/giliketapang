<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = "booking";
    protected $guarded = [];
    protected $fillable= [
        'bookingid', 'name_order', 'email', 'city', 'method_payment', 'type_payment_bank', 'status', 'number_participant', 'payment', 'unit', 'no_hp', 'total_payment', 'date_booking', 'activities_id'
    ];
    public function activities(){
        return $this->belongsTo(Activities::class,'activities_id');
    }
}
