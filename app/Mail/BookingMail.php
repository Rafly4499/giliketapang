<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingMail extends Mailable
{
    use Queueable, SerializesModels;

   /**
     * The user instance.
     *
     * @var user
     */
    public $info;

    /**
     * Create a new message instance.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function __construct($user)
    {
        $this->info = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this
                 ->subject('Booking GiliKetapangTrip')
                 ->from('giliketapang1@gmail.com')
                 ->view( 'pages.mails.bookingmail', ['info' => $this->info] );
    }
}
